[ ![Download](https://api.bintray.com/packages/librade/public/librade-spi/images/download.svg) ](https://bintray.com/librade/public/librade-spi/_latestVersion)

# Librade

This is an SPI package for [Librade algorithmic trading platform](http://librade.com).
Librade platform is implementing this SPI and is injected to trading algorithm, which can access all the platforms features. 
## Getting started


The main object which is injected to algorithm is of class ILibradeAPI. You can use Librade Java API from any of the other languages that Librade supports.
But here'a a cool bit - we don't force you to use the Java API directly from, say, Python, R or Ruby - after all, different languages have different conventions and idioms,
and it would be odd to force Java idioms on Ruby developers (for example). Instead, we automatically generate an idiomatic equivalent of the Java API for each language.
Let's discuss the different concepts and features in Librade API.

#####Example:
    :::java
    /**
    * Method starts algorithm execution.
    * @param pAPI implementation of SPI, which you can use to access all the features
    * @param pInputParameters input parameters, that are inserted through the web based GUI
    */
    public void runAlgo(ILibradeAPI pAPI, String[] pInputParameters) {
        // algorithm logic initializes here
    }

## Main API parts


Librade API consists from these main parts:

*    **cep** - provides access to complex event processor functionality([Esper](http://www.espertech.com/esper/) or [Siddhi](https://docs.wso2.com/display/CEP400/WSO2+Complex+Event+Processor+Documentation))
*    **event** - provides access to events that are arriving to algorithm through CEP
*    **orderExecution** - provides access to order execution (sending orders, requesting account state, ...)
*    **computerHumanInteraction** - provides access to computer human interaction methods (e.g. text notifications or requests like sms or emails, voice notifications or requests, ...)
*    **log** - provides access to logging functionality
*    **historicalData** - provides access to raw (not filtered through CEP) historical data
*    **async** - provides access to asynchronous task execution functionality
*    **dataStorage** - provides access to data storage functionality (e.g. variables can be stored/retrieved from/to data storage through this interface)
*    **session** - provides access to trading session related functionality (algorithm can stop its trading session)
*    **time** - provides access to time related functionality (e.g. current time, timers, custom timed events)
*    **utils** - provides access to various utility methods


### CEP


Provides access to complex event processor functionality. In [event-driven](https://en.wikipedia.org/wiki/Event-driven_programming)
[reactive systems](http://www.reactivemanifesto.org) like the librade platform,
[CEP](https://en.wikipedia.org/wiki/Complex_event_processing) is the main part, through which your algorithm
can "tap in" to streams (or channels, pipelines,... - call it as you like) of data that are flowing throughout the system. The basic streams which your algorithm will require for sure
(unless your strategy is a very "exotic" one) are the streams of MarketData. To receive that, your algo just need to start a respective CEP query.
Very nice tool to try the EPL queries: [Esper EPL Online](http://esper-epl-tryout.appspot.com/epltryout/mainform.html).
In following example, you see how easy it is to receive realtime tick data from LMAX EUR/USD. After the query is started, algorithm starts receiving events that matches the query.


#####Example:
    :::java
    // Every event that matches this query is marked by the queryUniqueIdentifier, so you know to which query event belongs to.
    api.cep().createAndStartEPLQuery("queryUniqueIdentifier", "SELECT * FROM MarketData(id=4006)");
    // same result, little longer representation
    api.cep().createAndStartEPLQuery("queryUniqueIdentifier", "SELECT * FROM MarketData(broker='LMAX',symbol='EUR',currency='USD')");


If you need CEP filtered historical data, you can use methods, which takes also start, end time and timeZone
[(all available time zones)](https://bitbucket.org/librade/librade-spi/src/master/doc/availableTimeZones.txt) as input parameters. Some algorithms need historical data before the realtime, so they can learn (or "train") themselves. You can use feature, which we call
**Continuous query**. This query starts with historical data, than it sends the marker, which marks the last historical data item, and than continues with realtime data.


#####Example:
    :::java
    // [HISTORICAL CEP QUERY] method starts query, that will stream historical tick data (4006 is id of EUR/USD on LMAX) through the CEP engine
    api.cep().createAndStartEPLQuery("historicalCEPQuery", "SELECT * FROM MarketData(broker='LMAX',symbol='EUR',currency='USD')", "01.01.2014 00:00:00.000", "01.01.2015 00:00:00.000", "America/New_York", new int[]{4006});
    // [CONTINUOUS QUERY] method starts with historical data, after which marker of last historical data is sent, which is followed by realtime data
    api.cep().createAndStartEPLQuery("continuousQuery", "SELECT * FROM MarketData(id=4006)", "01.01.2015 00:00:00.000", "America/New_York", new int[]{4006});
    // ...
    // last event
    if (event.isLast()) {
            // event marking last historical data was sent
    }
    // ...


Some of you would probably ask: "Ok, but I'm used to OHLC bar data, how can I get that? And I want my OHLC time period to be something different than 1, 5, 10... min.
I want it to be 1hour, 15min, 24sec". Well, here is the answer for you:

#####Example:
    :::java
    // you can set the period in milliseconds (3600000 + 900000 + 24000 = 4524000)
    api.cep().createAndStartEPLQuery("ohlc", "SELECT * FROM MarketData(broker='LMAX',symbol='EUR',currency='USD').librade:ohlc(4524000)");
    // to set start of the period to specific point in time, use second parameter (time is in UTC time zone)
    api.cep().createAndStartEPLQuery("ohlc", "SELECT * FROM MarketData(broker='LMAX',symbol='EUR',currency='USD').librade:ohlc(4524000,'12:00:00')");

Besides MarketData events, algorithm can receive ExecutionReport, AccountState, CustomEvent, ControlMessage, MonitoringEvent, NotificationEvent.

#### MarketData event

MarketData event is represented by IMarketData type. Here are the properties which can be used in CEP queries:

Base properties:


*    **id** - [Type:int] unique identifier of investment instrument (security) in the librade platform
*    **broker** - [Type:String] unique identifier of broker (e.g. LMAX or INTERACTIVE_BROKERS)
*    **exchange** - [Type:String] unique identifier of exchange (e.g. IDEALPRO)
*    **symbol** - [Type:String] investment instrument symbol (e.g. EUR)
*    **currency** - [Type:String] currency of investment instrument (e.g. USD)
*    **eventType** - [Type:String] type of the MarketData event (e.g. "Q" - as quotation, "D" - as deal)
*    **priceType** - [Type:String] price type (e.g. ASK, or BID) 
*    **volume** - [Type:BigDecimal] volume
*    **timestamp** - [Type:long] timestamp in **milliseconds** [(unix time, but in milliseconds)](https://en.wikipedia.org/wiki/Unix_time)
*    **priceBD** - [Type:BigDecimal]
*    **price** - [Type:double] this is a convenience method, which creates double value from priceBD BigDecimal

[More details here --->>>](http://librade.com)
#### ExecutionReport event

ExecutionReport event is represented by IExecutionReport type. Here are the properties which can be used in CEP queries:

Base properties:



*    **customAccountId** - [Type:String] custom account identifier. As algorithm can be trading on more than one account in one trading session, this uniquely identifies to which account, this execution belongs to. 
*    **clOrdId** - [Type:String] unique identifier of Order, to which this execution belongs (it's assigned to order by Librade platform and it's unique accross all trading sessions)
*    **execType** - [Type:String] execution type (e.g. PartialFill, or Fill, ...)
*    **sessionId** - [Type:String] librade trading session unique identifier
*    **orderId** - [Type:String] id of order at the broker
*    **orderType** - [Type:String] order type (e.g. MARKET, or LIMIT, ...)
*    **ordStatus** - [Type:String] order status (e.g. PartiallyFilled, Filled, ...)
*    **lastPrice** - [Type:BigDecimal] price of the last fill
*    **avgPrice** - [Type:BigDecimal] average price of execution
*    **stopPrice** - [Type:BigDecimal] stop price, if it's an execution report of STOP order
*    **priceBD** - [Type:BigDecimal] limit price, if it's an execution report of LIMIT order
*    **lastShares** - [Type:BigDecimal] quantity (e.g. shares) bought/sold on this (last) fill
*    **cumulativeQuantity** - [Type:BigDecimal] currently executed quantity for chain of executions
*    **quantity** - [Type:BigDecimal] order quantity
*    **leavesQuantity** - [Type:BigDecimal] Quantity open for further execution. If the OrdStatus is Canceled, DoneForTheDay, Expired, Calculated, or Rejected (in which case the order is no longer active) then LeavesQty could be 0, otherwise LeavesQty = OrderQty - CumQty.
*    **securityId** - [Type:int] security (investment instrument) of execution
*    **symbol** - [Type:String] security (investment instrument) symbol
*    **side** - [Type:String] side (e.g. BUY, SELL)
*    **timeInForce** - [Type:String] time in force (e.g. IOC, GTC, ...)
*    **expire** - [Type:long] expire of order in unix timestamp in **milliseconds** [(unix time, but in milliseconds)](https://en.wikipedia.org/wiki/Unix_time)
*    **timestamp** - [Type:long] timestamp of execution report creation in Librade in **milliseconds** [(unix time, but in milliseconds)](https://en.wikipedia.org/wiki/Unix_time)

[More details here ->>>](http://librade.com)
#### AccountState event

AccountState event is represented by IAccountState type. Here are the properties which can be used in CEP queries:

Base properties:


*    **timestamp** - [Type:long] timestamp of event creation in Librade in **milliseconds** [(unix time, but in milliseconds)](https://en.wikipedia.org/wiki/Unix_time)
*    **sessionId** - [Type:String] librade trading session unique identifier
*    **customAccountId** - [Type:String] custom account identifier. As algorithm can be trading on more than one account in one trading session, this uniquely identifies to which account, this execution belongs to. 
*    **accountBalance** - [Type:BigDecimal] account balance
*    **availableFunds** - [Type:BigDecimal] available funds
*    **availableToWithdraw** - [Type:BigDecimal] available to withdraw
*    **unrealisedProfitAndLoss** - [Type:BigDecimal] unrealized profit and loss
*    **margin** - [Type:BigDecimal] used margin

[More details here --->>>](http://librade.com)
#### CustomEvent

CustomEvent is represented by ICustomEvent type. Here are the properties which can be used in CEP queries:

Base properties:


*    **timestamp** - [Type:long] timestamp of event creation in Librade in **milliseconds** [(unix time, but in milliseconds)](https://en.wikipedia.org/wiki/Unix_time)
*    **id** - [Type:String] id of custom event
*    **eventType** - [Type:String] custom event type (e.g. TIME, ...)
*    **content** - [Type:String] content of the custom event (it could be a value, that can user input to algo from cell phone, if algorithm calls user)

[More details here --->>>](http://librade.com)
### Events


This part is used for getting to events. You simply call your trading session event queue and poll the events out of it. Your algorithm gets the event instantly.
Main part of your algorithm should be waiting for events on this queue to arrive and react on it. If your algorithm can't process events in pace they are arriving,
librade will discard them (there are many reasons why we don't try to store them somewhere, or provide any mechanisms to get to discarded events).
Trading session event queue size is 100K, which is plenty. All your algorithm needs to do is to process events efficiently.

#####Example:
    :::java
    // poll one event from the top of the queue, return null if empty
    ICEPEvent poll = api.event().poll();
    // wait max. 5 seconds for event to arrive, if the event arrives it returns it instantly, if no event arrives in 5 seconds, return null
    ICEPEvent poll = api.event().poll(5000);

If you are asking why we haven't used design pattern like [observer](https://en.wikipedia.org/wiki/Observer_pattern),
or listener (or whatever you call it) to deliver events to your algorithm, but rather we choose simple queue buffer, here is the answer.
There are milions of events flowing through the system every second. If you decide to react to a large portion of these, your algorithm needs to be able to handle events in couple of microseconds
- which is plenty of time for even some complex computations. But if for some reason it's not, platform needs to be able to function properly in all cases.
(even if there is a clear separation of trading sessions - they are running on different physical servers, and your failed or stuck trading sessions can't influence each other).
To sum that up:

*    if we choose to deliver every event in its own thread and your algorithm will not release that thread, event thread pool will be depleted
*    if we deliver events in one thread and that thread will be blocked for longer period of time, its buffer queue will overflow
*    ...

Hence the queue. Main event processing cycle could look like this:


#####Example:
    :::java
    // process events, until algorithm is stopped
    while (!api.session().shouldStop()) {
        EventWrapper event = new EventWrapper(api.event().poll(5000));
        // if it's null, wait for next event
        if (event.isNa()) {
            continue;
        }
        processEvent(event);
    }

### Order execution


In order to open positions, algorithm need to create trade orders and send them specific account at the specific broker. After order is sent, algorithm should await the ExecutionReport.
Unless some fatal error occurs (like the datacenter where your trading session is running is hit by a meteor), ExecutionReport should always appear in your event queue.
Besides sending orders, algorithm can also request account state.

#####Example:
    :::java
    // to send order
    api.orderExecution().sendOrder(api.orderExecution().createOrderBuilder()
        .customAccountUID("account123") // account identifier, where the order will be routed by librade
        .orderType("MARKET") // or LIMIT, STOP, ...
        .quantity(100000)
        .side("BUY") // or SELL
        .timeInForce("IOC") // or DAY, GTC, ...
        .securityIdentifier(4006) // id of EUR/USD on LMAX
        .buildNewOrderSingle());

    // to request account state
    api.orderExecution().requestAccountState("account123"); // account identifier, to which account state is requested

### CHI (Computer Human Interaction)

"Fancy" name for simple, yet usefull set of features - if your algorithm needs a human input, besides sending event to it through our main browser based GUI, algotihm can e.g. call on your phone and ask for input.
After you input the value through phone keyboard, simply press #, to send it to your algorithm. Phone SMS messages are a planned feature.

#####Example:
    :::java
    // you specify the phone number and question, the algorithm needs to ask you
    String statementId = api.computerHumanInteraction().requestVoiceUserInput("+1234567890", "Hi Tomas, here is your algorithm on librade. It looks like there is a strong buy opportunity on EUR/USD. If you would you like, I can open a position, just input the volume size followed by hash.");
    // ...
    // after the call reach you and you enter input, algorithm can use that input
    if (event.containsCustomEvents() && statementId.equals(event.getStatementId())) {
        String userInput = event.getCustomEvents().get(0).getContent();
    // ...
    }


### Log


Strings logged through log functionality are easily accessible through the browser based GUI.


    **Important:** Limit for one log string length is 1024, rest is truncated. Algorithm can produce 100 logs per second, rest is discarded.
    During whole trading session (simulation, demo trading, live trading, ...), 20K of logs entries are allowed, after reaching this limit,
    session is stopped (that's a precaution for algorithms that are stuck in cycle and can deplete users resources).

#####Example:
    :::java
    // this will create trace log entry (dark grey in color in gui)
    api.log().trace("Some trace log...");
    // this will create trace log entry (grey in color in gui)
    api.log().debug("Some debug log...");
    // this will create trace log entry (white in color in gui)
    api.log().info("Some info log...");
    // this will create trace log entry (yellow in color in gui)
    api.log().warn("Some warn log...");
    // this will create trace log entry (orange in color in gui)
    api.log().error("Some error log...");


### Historical data


If getting CEP filtered data is not enough and your algorithm needs raw tick data, you can use historical data functionality.

#####Example:
    :::java
    IResultBuffer<ITimeSeriesData> dataBuffer = api.historicalData().selectMarketData("01.01.2014 00:00:00.000", "01.01.2015 00:00:00.000", "America/New_York", new int[]{4006});
    while (!api.session().shouldStop()) {
        ITimeSeriesData tick = dataBuffer.poll(5000);
        if (tick == null) {
            continue;
        } else if (tick instanceof IMarketData) {
            // process the tick data point
            processTick(tick);
        } else {
            if ("LAST".equals(((IControlMessage) tick).getControlMessageType())) {
                break; // last historical price arrived
            } else {
                // in case of error, control message is sent with error reason
                api.log().error(tick);
            }
        }
    }

### Async


Provides access to asynchronous task execution functionality. If you need to run parallel computations, or functionality that runs in background to main algorithm thread. 

#####Example:
    :::java
    // create callable containing some computations
    Callable<Double> callable = new Callable<Double>() { public Double call() throws Exception {
            double result = 0;
            // some computations...
            return result;
    }};
    // submit the callable for async execution
    Future<SomeResultType> future = api.async().submit(callable);
    // do some other computations
    SomeResultType result = future.get(); // blocking method (waits until callable is finished) to get the result counted in background thread
    // OR
    SomeResultType result = future.get(1, TimeUnit.SECONDS); // if you don't want to wait more than 1 sec. for result


### Data storage


Provides access to data storage functionality (e.g. variables can be stored/retrieved from/to data storage through this interface). Storage types:

*    NEAR_CACHE - stores data in containers RAM, either in direct memory mapped buffers, or in JVM heap
*    DISTRIBUTED_CACHE - central distributed inMemory cache. When you need to share data with multiple algorithms running on different servers, 
inMemory cache is great for sharing e.g. some partial results. The cache memory is shared among all running algorithms, but algorithm has access to only 
part of the cache, which is marked by trading session UID - so you can't interfere with another trading session data. Important is, that if the inMemory 
cache run's out of memory, it starts evicting old items. Which means you can't be sure, if the value stays in cache for couple of hours, or couple of days.
If you want to store data for longer period of time, use DATABASE or FILE_STORAGE.
*    DATABASE - central database
*    FILE_STORAGE - central file storage


    **Important:** Limits for variable size:
         NEAR_CACHE - limited only by the RAM of the container
         DISTRIBUTED_CACHE - one variable can't be bigger than 64KB
         DATABASE - one variable can't be bigger than 16MB
         FILE_STORAGE - one variable can't be bigger than 1GB;

As you can see in the following example, only Serializable types are supported as cache values.


#####Example:
    :::java
    // to set value to data storage
    api.dataStorage().set(dataStorageType, key, value)

    // to get value from data storage
    Serializable value = api.dataStorage().get(dataStorageType, key);


### Session


Provides access to trading session related functionality:


*    algorithm can stop its own trading session, e.g. if some error state occurs and user want's to save resources
*    algorithm can find out, if it should stop and e.g. commence stopping procedure
*    algorithm can find out, if its trading session is already stopped (if an error occures and session was closed by force)
*    algorithm can find out, if its trading session is restored. Restoring a trading session means starting trading session with the same session UID. All the data will be assigned to that trading session. Algorithm results, account state,... and those data from one trading session can be displayed in one chart, or downloaded in one csv...

#####Example:
    :::java
    // stops the algorithm trading session
    api.session().stopSession();
    // returns true, if the stop was requested (either from system or user)
    boolean shouldStop = api.session().shouldStop();
    // returns true, if session was already stopped (and its resources closed/cleared, like broker connectors,...)
    boolean isStopped = api.session().isStopped();
    // returns true, if session was restored
    boolean isRestored = api.session().isRestoredSession();

### Time


Provides access to time related functionality:


*    retrieve start time - when the trading session should start or already started
*    retrieve end time - when the trading session should end, or already ended
*    retrieve current time - what is the current time of trading session. Algorithm should never use the system server time, as that's different for simulation and realtime (relative time vs. absolute).
Algorithm shouldn't be aware, that's in simulation trading session, or in realtime tradings session.
*    create custom time event to remind the algorithm of some events that occur at specific times (news, special dates, ...)
*    create custom timers, that helps algorithm measure time

#####Example:
    :::java
    // trading session start time
    long startTime = api.time().startTime();
    // trading session end time
    long endTime = api.time().endTime();
    // trading session current time
    long currentTime = api.time().currentTime();

    // schedule custom time event to fire at exact date and time, with periodicity 0 - which means, event is fired only once
    api.time().createCustomTimeEvent("newYear", "America/New_York", "01.01.2016 00:00:00.000", 0);
    // destroys the scheduled event (custom event will not be fired)
    api.time().destroyCustomTimeEvent("newYear");

    // mark the computation processing time starting point by creating the timer
    api.time().startTimer("computationProcessing1");
    // returns the duration in nanoseconds, from the time, the timer was started
    long durationInNanos = api.time().getTimer("computationProcessing1");
    // returns the duration in nanoseconds, from the time, the timer was started and destroys the timer
    durationInNanos = api.time().endTimer("computationProcessing1");

### Utils


Provides access to various functionality like e.g. getting enum values out of string, ...


#####Example:
    :::java
    // enum value uniquely representing investment instrument, in this example, it's forex investment instrument EUR/USD at LMAX
    ISecurityIdentifier securityIdentifier = api.utils().getSecurityIdentifierById(4006);
    // enum value representing buy side (usefull in creating orders, or processing execution reports)
    ISide BUY = api.utils().getSideByName("BUY");



# License (See LICENSE file for full license)

#### Copyright 2015 Librade LTD

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

    [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.