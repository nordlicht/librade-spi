package com.librade.spi.open;

import com.librade.spi.open.exceptions.APIException;
import java.util.LinkedHashMap;

/**
 *
 */
public interface ICEP {

    /**
     * Method create and start EPL query.
     * @param pStatementId unique id of query for given session
     * @param pStatement string in EPL language from which EPStatement will be created
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQuery(String pStatementId, String pStatement) throws APIException;

    /**
     * Method starts Esper Pattern.
     * @param pStatementId unique id of pattern for given session
     * @param pStatement string pattern from which EPStatement will be created
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartPattern(String pStatementId, String pStatement) throws APIException;

    /**
     * Method starts EPL queries.
     * @param pStatementId unique id of statements for given session
     * @param pIsEPLQuery array of booleans representing if statements are EPL Queries,
     * if true is on [2] position, than statement on [2] position in statements EPL query, if false, than it's a pattern
     * if null is used here, all statements are considered as EPL queries
     * @param pStatement string in EPL language from which EPStatement will be created
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQueriesOrPatterns(String pStatementId, boolean[] pIsEPLQuery, String... pStatement) throws APIException;

    /**
     * Method starts statement, after stop.
     * @param pStatementId unique id of query/pattern for given session
     * @throws APIException in case of an error, message contains the cause
     */
    void startStatement(String pStatementId) throws APIException;

    /**
     * Method stops statement after it has been started.
     * @param pStatementId unique id of query/pattern for given session
     * @throws APIException in case of an error, message contains the cause
     */
    void stopStatement(String pStatementId) throws APIException;

    /**
     * Method destroys the statement - it can't be started again.
     * @param pStatementId unique id of query/pattern for given session
     * @throws APIException in case of an error, message contains the cause
     */
    void destroyStatement(String pStatementId) throws APIException;

/******************************* Historical **************************************/

    /**
     * Method start historical EPL query.
     * @param pStatementId unique id of query for given session
     * @param pStatement string in EPL language from which EPStatement will be created
     * @param pStart a date you want to get historical MD from in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pEnd a date you want to get historical MD to in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pTimeZone e.g.: "America/Los_Angeles"
     * @param pIdentifiers list of identifiers you want get MD for
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQuery(String pStatementId, String pStatement, String pStart, String pEnd, String pTimeZone, int[] pIdentifiers) throws APIException;

    /**
     * Method start historical EPL query.
     * @param pStatementId unique id of query for given session
     * @param pStatement string in EPL language from which EPStatement will be created
     * @param pStart a date in millis (GMT0)
     * @param pEnd a date in millis (GMT0)
     * @param pIdentifiers list of identifiers you want get MD for
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQuery(String pStatementId, String pStatement, long pStart, long pEnd, int[] pIdentifiers) throws APIException;
    /**
     * Method start historical EPL queries.
     * @param pStatementId unique id of statements for given session
     * @param pStart a date you want to get historical MD from in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pEnd a date you want to get historical MD to in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pTimeZone e.g.: "America/Los_Angeles"
     * @param pIdentifiers list of identifiers you want get MD for
     * @param pIsEPLQuery array of booleans representing if statements are EPL Queries,
     * if true is on [2] position, than statement on [2] position in statements EPL query, if false, than it's a pattern
     * if null is used here, all statements are considered as EPL queries
     * @param pStatement string in EPL language from which EPStatement will be created
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQueriesOrPatterns(String pStatementId, String pStart, String pEnd, String pTimeZone, int[] pIdentifiers, boolean[] pIsEPLQuery,
                                            String... pStatement) throws APIException;

    /**
     * Method start historical EPL query.
     * @param pStatementId unique id of statements for given session
     * @param pStart a date in millis (GMT0)
     * @param pEnd a date in millis (GMT0)
     * @param pIdentifiers list of identifiers you want get MD for
     * @param pIsEPLQuery array of booleans representing if statements are EPL Queries,
     * if true is on [2] position, than statement on [2] position in statements EPL query, if false, than it's a pattern
     * if null is used here, all statements are considered as EPL queries
     * @param pStatement string in EPL language from which EPStatement will be created
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQueriesOrPatterns(String pStatementId, long pStart, long pEnd, int[] pIdentifiers, boolean[] pIsEPLQuery, String... pStatement)
            throws APIException;

    /******************************************* Continous **********************************************/

    /**
     * Method start historical EPL query and continue to realTime. Method start to send historical events to buffer,
     * after all historical events, LAST ControlMessage event is sent (eventWrapper.isLast() == true). After that real time events are sent.
     * @param pStatementId unique id of query for given session
     * @param pStatement string in EPL language from which EPStatement will be created
     * @param pStart a date in millis (GMT0)
     * @param pIdentifiers list of identifiers you want get MD for
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQuery(String pStatementId, String pStatement, long pStart, int[] pIdentifiers) throws APIException;

    /**
     * Method start historical EPL query and continue to realTime. Method start to send historical events to buffer,
     * after all historical events, event with , LAST ControlMessage event is sent (eventWrapper.isLast() == true). After that real time events are sent.
     * @param pStatementIdHistorical unique id of historical query for given session
     * @param pStatementIdRealtime unique id of realtime query for given session
     * @param pStatementHistorical query in EPL language from which EPStatement will be created
     * @param pStatementRealTime query in EPL language from which EPStatement will be created
     * @param pStart a date in millis (GMT0)
     * @param pIdentifiers list of identifiers you want get MD for
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQuery(String pStatementIdHistorical, String pStatementIdRealtime, String pStatementHistorical, String pStatementRealTime,
                                long pStart, int[] pIdentifiers) throws APIException;

    /**
     * Method start historical EPL query and continue to realTime. Method start to send historical events to buffer,
     * after all historical events, event with , LAST ControlMessage event is sent (eventWrapper.isLast() == true). After that real time events are sent.
     * @param pStatementId2QueryHistorical unique id of historical query 2 EPL query
     * @param pStatementId2QueryRealTime unique id of realtime query 2 EPL query
     * @param pStart a date in millis (GMT0)
     * @param pIdentifiers list of identifiers you want get MD for
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQuery(LinkedHashMap<String, String> pStatementId2QueryHistorical, LinkedHashMap<String, String> pStatementId2QueryRealTime,
                                long pStart, int[] pIdentifiers) throws APIException;

    /**
     * Method start historical EPL query and continue to realTime. Method start to send historical events to buffer,
     * after all historical events, event with , LAST ControlMessage event is sent (eventWrapper.isLast() == true). After that real time events are sent.
     * @param pId unique id of query for given session
     * @param pStatement string in EPL language from which EPStatement will be created
     * @param pStart a date you want to get historical MD from in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pTimeZone e.g.: "America/Los_Angeles"
     * @param pIdentifiers list of identifiers you want get MD for
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQuery(String pId, String pStatement, String pStart, String pTimeZone, int[] pIdentifiers) throws APIException;

    /**
     * Method start historical EPL query and continue to realTime. Method start to send historical events to buffer,
     * after all historical events, event with, LAST ControlMessage event is sent (eventWrapper.isLast() == true). After that real time events are sent.
     * @param pStatementId unique id of statements for given session
     * @param pStart a date in millis (GMT0)
     * @param pIdentifiers list of identifiers you want get MD for
     * @param pIsEPLQuery array of booleans representing if statements are EPL Queries,
     * if true is on [2] position, than statement on [2] position in statements EPL query, if false, than it's a pattern
     * if null is used here, all statements are considered as EPL queries
     * @param pStatement string in EPL language from which EPStatement will be created
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQueriesOrPatterns(String pStatementId, long pStart, int[] pIdentifiers, boolean[] pIsEPLQuery, String... pStatement)
            throws APIException;

    /**
     * Method start historical EPL query and continue to realTime. Method start to send historical events to buffer,
     * after all historical events, event with, LAST ControlMessage event is sent (eventWrapper.isLast() == true). After that real time events are sent.
     * @param pStatementId unique id of statements for given session
     * @param pStart a date you want to get historical MD from in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pTimeZone e.g.: "America/Los_Angeles"
     * @param pIdentifiers list of identifiers you want get MD for
     * @param pIsEPLQuery array of booleans representing if statements are EPL Queries,
     * if true is on [2] position, than statement on [2] position in statements EPL query, if false, than it's a pattern
     * if null is used here, all statements are considered as EPL queries
     * @param pStatement string in EPL language from which EPStatement will be created
     * @throws APIException in case of an error, message contains the cause
     */
    void createAndStartEPLQueriesOrPatterns(String pStatementId, String pStart, String pTimeZone, int[] pIdentifiers, boolean[] pIsEPLQuery,
                                            String... pStatement) throws APIException;
}










