package com.librade.spi.open;

import com.librade.spi.open.data.definitions.IAsset;
import com.librade.spi.open.data.definitions.IBroker;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.exceptions.APIException;
import com.nordlicht.ITimeSeriesData;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingDeque;

/**
 * Librade API.
 */
public interface ILibradeAPI extends IAlgorithmAPI {

    /**
     * Method provides access to trade order related functionality.
     * @return trade order related functionality interface
     */
    IOrderExecution orderExecution();

    /**
     * Method provides access to TradeBook functionality.
     * @return tradeBook
     */
    IAccount account();

    /**
     *
     * @param pCustomAccounts custom account ids of virtual accounts
     * @param pSecurities securities
     * @param pData input data for simulation
     * @param pStartOfSimulation start of simulation time
     * @param pEndOfSimulationTime end of simulation time
     * @return api
     * @throws APIException exception
     */
    ILibradeAPI createInnerSimulation(Map<String, IBroker> pCustomAccounts, List<ISecurityIdentifier> pSecurities, BlockingDeque<ITimeSeriesData> pData,
                                      long pStartOfSimulation, long pEndOfSimulationTime, BigDecimal pStartBalance, IAsset pBaseCurrency) throws APIException;
}









