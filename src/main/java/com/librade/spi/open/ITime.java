package com.librade.spi.open;


import com.librade.spi.open.exceptions.APIException;

/**
 *
 */
public interface ITime {

    /**
     *
     * @return start time in java millisecond format
     */
    long startTime();

    /**
     *
     * @return end time in java millisecond format
     */
    long endTime();

    /**
     *
     * @return current time in millis - algorithms need to use this method all the time, as for realTime it returns real absolute time, but for
     * simulation it returns the rerlative time of simulation
     */
    long currentTime();

    /**
     * Starts the timer.
     * @param pTimerID timer ID
     * @return true, if successfull, false if not
     */
    boolean startTimer(String pTimerID);

    /**
     * Returns the duration from start.
     * @param pTimerID timer ID
     * @return time from start
     */
    long endTimer(String pTimerID);

    /**
     * Returns the duration from start.
     * @param pTimerID timer ID
     * @return timer value
     */
    long getTimer(String pTimerID);

    /**
     *
     * @param pID id of events
     * @param pTimeZone time zone CET/EST/CEST/....
     * @param pTime time format "dd.MM.yyyy HH:mm:ss.SSS"
     * @param pPeriodicity in milliseconds
     * @throws APIException any exception
     */
    void createCustomTimeEvent(String pID, String pTimeZone, String pTime, long pPeriodicity) throws APIException;

    /**
     *
     * @param pID id of custom events
     * @return true if custom event was destroyed
     */
    boolean destroyCustomTimeEvent(String pID) throws APIException;

    /**
     *
     * @param pDateTime date time "dd.MM.yyyy HH:mm:ss.SSS"
     * @param pTimeZone time zone
     * @return timestamp
     */
    long parseDateTime(String pDateTime, String pTimeZone) throws APIException;
}
