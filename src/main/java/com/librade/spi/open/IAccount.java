package com.librade.spi.open;

import com.librade.spi.open.data.IAccountState;
import com.librade.spi.open.data.IBrokerMessage;
import com.librade.spi.open.data.IExecutionReport;
import com.librade.spi.open.data.IPositionReport;
import com.librade.spi.open.data.definitions.IAsset;
import com.librade.spi.open.data.definitions.IBroker;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.exceptions.APIException;
import com.librade.spi.open.exceptions.APIRuntimeException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Set;

/**
 * @author Stanislav Fujdiar
 */
public interface IAccount {

    /**
     * Create a new virtual account on wich you can send orders.
     * @param pAccountId account id (custom identifier)
     * @param pBroker broker
     * @param pSecurities securities
     * @return true if virtual account was created
     */
    boolean createVirtualAccount(String pAccountId, IBroker pBroker, List<ISecurityIdentifier> pSecurities) throws APIException;

    /**
     * Delete created virtual account.
     * @param pAccountId account id
     */
    void deleteVirtualAccount(String pAccountId) throws APIException;

    /**
     * Method should request the broker about the account state. Account state event is created and sent to CEP.
     * @param pCustomAccountId custom account Id
     * @throws APIException exception from broker connector
     */
    OptionalInt requestAccountState(String pCustomAccountId) throws APIException;

    /**
     * Method should request the broker about trading history.
     * @param pCustomAccountId custom account Id
     * @param pSecurities securities for which you want trading history, if null or empty all trading history will be requested
     * @return
     */
    List<IBrokerMessage> requestTradingHistoryASync(String pCustomAccountId, Set<ISecurityIdentifier> pSecurities) throws APIException;

    /**
     * Method should request the broker about open positions.
     * @param pCustomAccountId
     * @return
     * @throws APIException
     */
    default List<IExecutionReport> requestOpenPositionsSync(String pCustomAccountId) throws APIException {
        throw new APIException("Not implemented yet.");
    }

    default OptionalInt requestOpenPositionsUpdate(String customAccountId) {
        throw new APIRuntimeException("Not implemented yet.");
    }

    /**
     * Return true if you can handle custom account id through thi API
     * @param customAccountId
     * @return
     */
    boolean isSuitableFor(String customAccountId);

    /**
     * Destroys broker connector and stops user data stream.
     * @param customAccountId
     * @return true if account removed
     */
    boolean removeAccount(String customAccountId);

    /**
     * Method should request the broker about the account state. Account state is created and returned. NOT SENT TO CEP
     * @param pCustomAccountId custom account Id
     * @throws APIException exception from broker connector
     */
    default IAccountState requestAccountStateSync(String pCustomAccountId) throws APIException{
        throw new APIException("Not implemented yet.");
    }

    /**
     *
     * @return account state for the first account it finds (usable for situations, when there is only 1 broker account)
     * @throws APIException from broker connector
     */
    IAccountState requestAccountStateSync() throws APIException;

    /**
     * Start balance.
     * @return start balance
     */
    BigDecimal getStartBalance();

    /**
     * Deposit currency.
     * @return deposit currency (base currency)
     */
    IAsset getDepositCurrency();

    default List<IExecutionReport> getOpenPositions(int computingSessionId) {
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     *
     * @return open positions for current trading session
     */
    List<IExecutionReport> getOpenPositions();

    void subscribeToUserDataStream(String customAccountId) throws APIException;

    default IPositionReport getPositionReport(
            String customAccountId,
            ISecurityIdentifier securityIdentifier,
            Map<String, Object> additionalParams) throws APIException {
        throw new UnsupportedOperationException("The method getPositionReport is not implemented");
    }

    default void setLeverage(
            String customAccountId,
            Double leverage,
            ISecurityIdentifier securityIdentifier,
            Map<String, Object> additionalParams) throws APIException {
        throw new UnsupportedOperationException("The method setLeverage is not implemented");
    }
}





