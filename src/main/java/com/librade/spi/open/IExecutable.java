package com.librade.spi.open;

import com.librade.spi.open.exceptions.APIException;

/**
 * Executable, that is executed by computing API.
 */
public interface IExecutable {

    /**
     * Method that needs to be implemented in order to be executed.
     * @param api librade API
     * @param inputParameters input parameters, in TOML format, or plain text
     */
    void execute(ILibradeAPI api, String inputParameters) throws APIException;

    /**
     * Method will be executed after computating session is started on server.
     * @param api librade API
     * @param inputParameters input parameters, in TOML format, or plain text
     */
    default void executeSafe(ILibradeAPI api, String inputParameters) {
        try {
            execute(api, inputParameters);
        } catch (Throwable pE) {
            api.log().error(pE.getMessage(), pE);
        } finally {
            try {
                api.session().stopSession();
            } catch (APIException pE) {
                api.log().error(pE.getMessage(), pE);
            }
        }
    }
}






