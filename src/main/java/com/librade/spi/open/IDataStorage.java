package com.librade.spi.open;

import com.librade.spi.open.data.IAggregatedMarketData;
import com.librade.spi.open.data.IAnalyticsEvent;
import com.librade.spi.open.data.definitions.IAnalyticsType;
import com.librade.spi.open.data.definitions.IDataStorageType;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ITimeInterval;
import com.librade.spi.open.data.patterns.IPattern;
import com.librade.spi.open.exceptions.APIException;
import com.nordlicht.ITimeSeriesData;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Provides access to data storage functionality.
 * @author Tomas Fecko
 */
public interface IDataStorage {

    /**
     * Retrieves the variable of the specified name from the specified storage.
     * @param pDataStorageType place, from which the variable is retrieved
     * @param pName name of the variable
     * @return value or null if there is no value associated with given session uid and name
     * @throws APIException any system exception
     */
    <T extends Serializable> T get(IDataStorageType pDataStorageType, String pName) throws APIException;

    /**
     * Retrieves the variable of the specified name from the specified storage.
     * @param pDataStorageType place, from which the variable is retrieved
     * @param pName name of the variable
     * @param pValuesToFill values to fill
     * @return value or null if there is no value associated with given session uid and name
     * @throws APIException any system exception
     */
    <T extends Serializable> int get(IDataStorageType pDataStorageType, String pName, List<T> pValuesToFill) throws APIException;

    /**
     *
     * @param pDataStorageType place, where the variable is stored
     * @param pName name of the variable
     * @param pValue value of the variable
     * @return previous value of the variable or null if there was no previous value
     * @throws APIException any system exception
     */
    <T extends Serializable> T set(IDataStorageType pDataStorageType, String pName, T pValue) throws APIException;

    /**
     *
     * @param pDataStorageType data storage type
     * @param pName name
     * @throws APIException any exception
     */
    void remove(IDataStorageType pDataStorageType, String pName) throws APIException;

    /**
     *
     * @param pDataStorageType data storage type
     * @return all used variable names
     * @throws APIException
     */
    Set<String> allNames(IDataStorageType pDataStorageType) throws APIException;

    Optional<IAggregatedMarketData> getLastAggregatedMarketData(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval);

    Optional<IAnalyticsEvent> getLastAnalyticsData(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, IAnalyticsType analyticsType);

    Optional<IAnalyticsEvent[]> getLastNAnalyticsData(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, IAnalyticsType analyticsType, int nValues);

    /**
     *
     * @param securityIdentifier
     * @param timeInterval
     * @param nValues
     * @return array with ascending timestamp, if there is not enought data than empty
     */
    Optional<IAggregatedMarketData[]> getLastNAggregatedMarketData(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int nValues);

    /**
     *
     * @param securityIdentifier
     * @param timeInterval
     * @param nValues
     * @return array with ascending timestamp
     */
    IAggregatedMarketData[] getLastNAggregatedMarketDataOrLess(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int nValues);

    Optional<BigDecimal[]> getLastCloseBigDecimalPrices(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int numberOfPeriods);

    Optional<BigDecimal[]> getLastCloseBigDecimalPricesOrLess(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int numberOfPeriods);

    Optional<List<IAggregatedMarketData>> getLastHighLowPricesOrLess(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int numberOfPeriods);

    Optional<BigDecimal[]> getLastVolumes(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int numberOfPeriods);

    Optional<double[]> getLastClosePrices(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int numberOfPeriods);

    /**
     * return field of high prices for last numberOfPeriods days
     * @return high prices for last
     */
    Optional<double[]> getLastHighPrices(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int numberOfPeriods);

    /**
     * return field of low prices for last numberOfPeriods days
     * @return low prices for last
     */
    Optional<double[]> getLastLowPrices(ISecurityIdentifier securityIdentifier, ITimeInterval timeInterval, int numberOfPeriods);

    /**
     *
     * @param timeInterval time interval of patterns
     * @param numberOfPeriods number of time interval periods
     * @return list of all last relevant patterns, or empty list
     */
    List<IPattern> getLastRelevantPatterns(ITimeInterval timeInterval, int numberOfPeriods);

    void storeEventInCache(ITimeSeriesData event);

        /**
         * Initialize aggregated market data / analytics cache / aggregated market data queue name
         * @param object
         */
    void init(Object object);
}






