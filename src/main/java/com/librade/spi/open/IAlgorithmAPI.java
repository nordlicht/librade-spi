package com.librade.spi.open;

/**
 * @author Tomas Fecko
 */
public interface IAlgorithmAPI {

    /**
     * Method provides access to complex event processor functionality.
     * @return cep related functionality interface
     */
    ICEP cep();

    /**
     * Method provides access to event functionality. Events are all the objects comming through the CEP.
     * @return event related functionality interface
     */
    IEvent event();

    /**
     * Method provide access to historical data functionality.
     * @return historical data related interface
     */
    IHistoricalData historicalData();

    /**
     * Method provides access to utils methods.
     * @return utils related interface
     */
    IUtils utils();

    /**
     * Method provides access to computer human interaction methods.
     * @return computer human interface
     */
    IComputerHumanInteraction computerHumanInteraction();

    /**
     * Method provides access to dataTransform functionality.
     * @return dataTransform
     */
    IDataTransform dataTransform();

    /**
     * Analytics handler. provides access to analytics functionality.
     * @return analytics
     */
    IAnalytics analytics();

    /**
     * Method provides access to data storage functionality. (e.g. variables can be stored/retrieved from/to
     * database or other persistent storage through this interface).
     * @return persistence related functionality interface
     */
    IDataStorage dataStorage();

    /**
     * Method provides access to log methods.
     * @return utils related interface
     */
    ILog log();

    /**
     * Method provides access to asynchronous task execution.
     * @return async task related interface
     */
    IAsyncTask async();

    /**
     * Method provides access to time related functionality.
     * @return time related functionality interface
     */
    ITime time();

    /**
     * Method provides access to session related methods.
     * @return session related interface
     */
    ISession session();
}





