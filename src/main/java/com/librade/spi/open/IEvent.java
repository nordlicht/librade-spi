package com.librade.spi.open;

import com.librade.spi.open.data.IControlMessage;
import com.librade.spi.open.exceptions.APIException;
import com.nordlicht.ITimeSeriesData;
import com.nordlicht.commons.base.cep.ICEPEvent;

import java.util.List;
import java.util.function.Predicate;

/**
 *
 */
public interface IEvent {


    /**
     * Method receives one Event from buffer or null if buffer is empty.
     * @return event from buffer
     */
    ICEPEvent poll();

    /**
     * Method receives one Event from buffer. It can have timeout specified, which is the time, during which if no event will not arrive, it returns null.
     * @param pTimeoutInMilliseconds timeout, in milliseconds, which the method will wait on buffer, before returning null, if nothing arrives to
     * buffer
     * @return object, which arrives during the waiting
     */
    ICEPEvent poll(long pTimeoutInMilliseconds);

    /**
     * Receive all events from buffer, or null, if buffer is empty.
     * @return event from buffer
     */
    List<ICEPEvent> pollAll();

    /**
     * Method receives all Events from buffer. It can have timeout specified, which is the time, during which if no event will not arrive, it returns null.
     * @param pTimeoutInMilliseconds timeout, in milliseconds, which the method will wait on buffer, before returning null, if nothing arrives to buffer
     * @return object, which arrives during the waiting
     */
    List<ICEPEvent> pollAll(long pTimeoutInMilliseconds);

    /**
     * Method return number of discarded events due to full buffer.
     * @return the numberOfDiscardedEvents
     */
    long getNumberOfDiscardedEvents();

    /**
     * Method returns free space in buffer.
     * @return CEP Events Buffer Remaining Capacity
     */
    long getCEPEventsBufferRemainingCapacity();

    /**
     * Method returns the number of cep events in buffer.
     * @return CEP Events Buffer Remaining Capacity
     */
    long getCEPEventsBufferSize();

    /**
     * Method returns true, if there are no more cep events in buffer.
     * @return false, if cep events buffer contains events
     */
    boolean isCEPEventBufferEmpty();

    /**
     * Creates last message.
     * @return last cep event message
     */
    IControlMessage getMarkMessage();

    /**
     * Method subscribes subscriber to cep events. CEP Events of this CEP statement id will no longer be placed to main buffer, but to this subscriber.
     * @param pCEPStatementId cep statement id
     * @param pSubscriber subscriber
     * @return subscribtion UID
     */
    String subscribe(String pCEPStatementId, IEventSubscriber pSubscriber) throws APIException;

    /**
     * Unsubscribes subscriber with that subscribtion UID.
     * CEP Events of cep statement id, that the subscribtion was created with will be offered to main buffer.
     * @param pSubscribtionUID subscribtion UID
     */
    void unsubscribe(String pSubscribtionUID) throws APIException;

    /**
     * Creates filter before CEP. If filter is used and false is returned, event is not sent to complex event processor (CEP).
     * Filter is applied right after the event arrives to computing session.
     * @param filter filter
     */
    void filter(Predicate<ITimeSeriesData> filter);

     /**
     * CEP Event subscriber.
     */
    @FunctionalInterface
    interface IEventSubscriber {

        /**
         *
         * @param pCEPEvent cep event
         */
        void onEvent(ICEPEvent pCEPEvent) throws APIException;
    }
}







