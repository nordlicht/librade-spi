package com.librade.spi.open;

import com.librade.spi.open.data.analytics.Matrix;
import com.librade.spi.open.data.analytics.Timeseries;
import com.librade.spi.open.exceptions.APIException;

/**
 * @author Stanislav Fujdiar
 */
public interface IAnalytics {

    /**
     * Linear regression analytics type.
     */
    String LINEAR_REG = "LINEAR_REG";


    /**
     * Draw chart from given data.
     * @param pTimeseries pTimeseries data
     * @throws APIException
     */
    void drawChart(Timeseries... pTimeseries) throws APIException;

    /**
     * Draw sheet from given data.
     * @param pTimeseries pTimeseries data
     * @throws APIException
     */
    void drawSheet(Matrix pMatrix) throws APIException;

    /**
     * Run realtime analytics.
     * @param pUID name of analytics
     * @param pType name of analytics
     * @param pIdentifiers identifiers
     * @param pPeriod period of OHLC data
     * @param pParameters parameters of given analytics
     * @throws APIException
     * @return uid
     */
    void runAnalytics(String pUID, String pType, int pIdentifier, long pPeriod, Object... pParameters) throws APIException;

    /**
     * Run continous analytics.
     * @param pUID name of analytics
     * @param pType name of analytics
     * @param pIdentifiers identifiers
     * @param pPeriod period of OHLC data
     * @param pParameters parameters of given analytics
     * @throws APIException
     * @return uid
     */
    void runAnalytics(String pUID, String pType, long pStart, int pIdentifier, long pPeriod, Object... pParameters) throws APIException;

    /**
     * Run continous analytics.
     * @param pUID name of analytics
     * @param pType name of analytics
     * @param pHistoricalIdentifier historical security
     * @param pRealTimeIdentifier realtime security
     * @param pPeriod period of OHLC data
     * @param pParameters parameters of given analytics
     * @throws APIException
     * @return uid
     */
    void runAnalytics(String pUID, String pType, long pStart, int pHistoricalIdentifier, int pRealTimeIdentifier, long pPeriod, Object... pParameters) throws APIException;

    /**
     * Run historical analytics.
     * @param pUID name of analytics
     * @param pType name of analytics
     * @param pIdentifiers identifiers
     * @param pPeriod period of OHLC data
     * @param pParameters parameters of given analytics
     * @throws APIException
     * @return uid
     */
    void runAnalytics(String pUID, String pType, long pStart, long pEnd, int pIdentifier, long pPeriod, Object... pParameters) throws APIException;
}
