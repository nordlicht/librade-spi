package com.librade.spi.open.data.analytics;

/**
 *
 * @author Stano Fujdiar
 */
public abstract class Timeseries<T> extends Table<T> {

    /**
     *
     * @param pLength length of timeseries
     */
    public Timeseries(int pLength) {
        super(pLength, 2);
    }
}






