package com.librade.spi.open.data.patterns;

import com.nordlicht.IDefinition;

public interface IPatternSubType extends IDefinition<IPatternSubType> {

    String VAL_UNSPECIFIED                = "UNSPECIFIED";
    String VAL_ASCENDING_TRIANGLE         = "ASCENDING_TRIANGLE";
    String VAL_DESCENDING_TRIANGLE        = "DESCENDING_TRIANGLE";
    String VAL_TRIANGLE                   = "TRIANGLE";
    String VAL_CHANNEL_DOWN               = "CHANNEL_DOWN";
    String VAL_CHANNEL_UP                 = "CHANNEL_UP";
    String VAL_FALLING_WEDGE              = "FALLING_WEDGE";
    String VAL_RISING_WEDGE               = "RISING_WEDGE";
    String VAL_RECTANGLE                  = "RECTANGLE";
    String VAL_HEAD_AND_SHOULDERS         = "HEAD_AND_SHOULDERS";
    String VAL_INVERSE_HEAD_AND_SHOULDERS = "INVERSE_HEAD_AND_SHOULDERS";
    String VAL_TRIPLE_TOP                 = "TRIPLE_TOP";
    String VAL_TRIPLE_BOTTOM              = "TRIPLE_BOTTOM";
    String VAL_FLAG                       = "FLAG";
    String VAL_PENNANT                    = "PENNANT";
    String VAL_DOUBLE_TOP                 = "DOUBLE_TOP";
    String VAL_DOUBLE_BOTTOM              = "DOUBLE_BOTTOM";
    String VAL_POINT_RETRACEMENT          = "POINT_RETRACEMENT";
    String VAL_POINT_EXTENSION            = "POINT_EXTENSION";
    String VAL_ABCD                       = "ABCD";
    String VAL_GARTLEY                    = "GARTLEY";
    String VAL_BUTTERFLY                  = "BUTTERFLY";
    String VAL_DRIVE                      = "DRIVE";
    String VAL_HORIZONTAL_RESISTANCE      = "HORIZONTAL_RESISTANCE";
    String VAL_HORIZONTAL_SUPPORT         = "HORIZONTAL_SUPPORT";
    String VAL_CONSECUTIVE_CANDLES        = "CONSECUTIVE_CANDLES";
    String VAL_BIG_MOVEMENT               = "BIG_MOVEMENT";

    String getCode();

}
