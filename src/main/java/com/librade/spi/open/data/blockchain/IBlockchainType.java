package com.librade.spi.open.data.blockchain;

import com.nordlicht.IDefinition;

public interface IBlockchainType extends IDefinition<IBlockchainType> {

    String getCode();

    String getHexChainId();
}
