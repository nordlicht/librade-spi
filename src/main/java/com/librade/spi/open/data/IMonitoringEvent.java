package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IMonitoringType;

/**
 * Monitoring event.
 */
public interface IMonitoringEvent {

    /**
     *
     * @return
     */
    String getMonitoringSubType();

    /**
     *
     * @return
     */
    long getTimestamp();

    /**
     *
     * @return
     */
    double getValue();

    /**
     *
     * @return
     */
    IMonitoringType getMonitoringTypeEnum();

    /**
     *
     * @return
     */
    String getMonitoringType();

    /**
     *
     * @return
     */
    int getMonitoringTypeId();

    /**
     *
     * @return
     */
    String getNodeUID();
}






