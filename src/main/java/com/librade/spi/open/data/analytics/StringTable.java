package com.librade.spi.open.data.analytics;

import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Stano Fujdiar
 */
public class StringTable extends Table<String> {

    private ArrayList<String>    mValues;

    public StringTable() {
    }

    /**
     *
     * @param pRows rows
     * @param pCols columns
     */
    public StringTable(int pRows, int pCols) {
        super(pRows, pCols);
    }

    @Override
    public ArrayList<String> getValues() {
        return mValues;
    }

    @Override
    public void setValues(ArrayList<String> pValues) {
        mValues = pValues;
    }

    @Override
    public int getByteableClassId() {
        return 3205;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        if (!super.equals(pO)) {
            return false;
        }
        final StringTable strings = (StringTable) pO;
        return Objects.equals(mValues, strings.mValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), mValues);
    }
}





