package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Event type.
 */
public interface IEventType extends IDefinition<IEventType> {

    /**
     * Quotation.
     */
    String Q = "Q";
    /**
     * Deal.
     */
    String D = "D";
    /**
     * Volatility calculated.
     */
    String VC = "VC";
}
