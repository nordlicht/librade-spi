package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

import java.util.Calendar;
import java.util.TimeZone;

import static java.util.Calendar.APRIL;
import static java.util.Calendar.AUGUST;
import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.FEBRUARY;
import static java.util.Calendar.JANUARY;
import static java.util.Calendar.JULY;
import static java.util.Calendar.JUNE;
import static java.util.Calendar.MARCH;
import static java.util.Calendar.MAY;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.NOVEMBER;
import static java.util.Calendar.OCTOBER;
import static java.util.Calendar.SEPTEMBER;
import static java.util.concurrent.TimeUnit.DAYS;

/**
 * @author Tomas Fecko
 */
public interface ITimeInterval extends IDefinition<ITimeInterval> {

    String MINUTES1 = "MINUTES1";
    String MINUTES3 = "MINUTES3";
    String MINUTES5 = "MINUTES5";
    String MINUTES15 = "MINUTES15";
    String MINUTES30 = "MINUTES30";
    String HOURLY = "HOURLY";
    String HOURS2 = "HOURS2";
    String HOURS4 = "HOURS4";
    String HOURS6 = "HOURS6";
    String HOURS8 = "HOURS8";
    String HOURS12 = "HOURS12";
    String DAILY = "DAILY";
    String DAYS3 = "DAYS3";
    String WEEKLY = "WEEKLY";

    /**
     *
     * @return
     */
    long getMaxDisplayableInterval();

    /**
     *
     * @return
     */
    long getDefaultDisplayableInterval();

    /**
     *
     * @return
     */
    long getTimespan();

    /**
     *
     * @return if true, interval represents calendar interval: week, month, quarter, year
     */
    default boolean isCalendarInterval() {
        return false;
    }

    /**
     *
     * @return normalized timestamp of interval, or interval start
     */
    default long getNormalizedStartOfInterval() {
        return getNormalizedStartOfInterval(System.currentTimeMillis());
    }

    /**
     *
     * @param timestamp
     * @return normalized timestamp of interval, or interval start
     */
    default long getNormalizedStartOfInterval(long timestamp) {
        if (getTimespan() == 0) {
            return 0;
        }
        if (isCalendarInterval()) { // calendar week, month, quarter, year
            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT-00:00"));
            calendar.setTimeInMillis(timestamp);
            if (getTimespan() >= DAYS.toMillis(8)) { // month
                calendar.set(DAY_OF_MONTH, 1);
            }
            if (getTimespan() >= DAYS.toMillis(31)) { // quarter
                int month = calendar.get(MONTH);
                if (month == JANUARY || month == FEBRUARY || month == MARCH) {
                    calendar.set(MONTH, JANUARY);
                } else if (month == APRIL || month == MAY || month == JUNE) {
                    calendar.set(MONTH, APRIL);
                } else if (month == JULY || month == AUGUST || month == SEPTEMBER) {
                    calendar.set(MONTH, JULY);
                } else if (month == OCTOBER || month == NOVEMBER || month == DECEMBER) {
                    calendar.set(MONTH, OCTOBER);
                }
            }
            if (getTimespan() >= DAYS.toMillis(365)) { // year
                calendar.set(Calendar.DAY_OF_YEAR, 1);
            }
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR, 0);
            return calendar.getTimeInMillis();
        } else {
            return (timestamp / getTimespan()) * getTimespan();
        }
    }
}





