package com.librade.spi.open.data.securityIdentifier;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.StringJoiner;

/**
 *
 * @author Stanislav Fujdiar
 */
public class Withdraw {

    private final BigDecimal min;
    private final BigDecimal max;

    public Withdraw(BigDecimal min, BigDecimal max) {
        this.min = min;
        this.max = max;
    }

    public Withdraw stripTrailingZeros() {
        return new Withdraw(
                min != null ? min.stripTrailingZeros() : null,
                max != null ? max.stripTrailingZeros() : null
        );
    }

    public BigDecimal getMin() {
        return min;
    }

    public BigDecimal getMax() {
        return max;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Withdraw.class.getSimpleName() + "[", "]")
                .add("min=" + min)
                .add("max=" + max)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Withdraw)) return false;
        Withdraw withdraw = (Withdraw) o;
        return Objects.equals(min, withdraw.min) && Objects.equals(max, withdraw.max);
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max);
    }
}
