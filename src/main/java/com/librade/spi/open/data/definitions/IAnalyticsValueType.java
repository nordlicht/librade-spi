package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * @author Tomas Fecko
 */
public interface IAnalyticsValueType extends IDefinition<IAnalyticsValueType> {

    int NUMERICAL_ID = 1;
}





