package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;
import com.nordlicht.commons.base.serialization.byteable.IByteable;

import java.math.BigDecimal;

/**
 *
 */
public interface ISecurityIdentifier extends IDefinition<ISecurityIdentifier>, IByteable {

    BigDecimal TICK_SIZE = new BigDecimal("0.000000000000000001");
    BigDecimal CONTRACT_MULTIPLIER = new BigDecimal("0.000000000000000001");
    /**
     * Exchange of security.
     * @return exchange enum
     */
    IExchange getExchange();

    /**
     * Broker of security.
     * @return broker
     */
    IBroker getBroker();

    /**
     * Symbol of security. (e.g. for currency pair EUR/USD, EUR is symbol)
     * @return symbol
     */
    IAsset getSymbol();

    /**
     * Currency of security. (e.g. for currency pair EUR/USD, USD is currency)
     * @return currency
     */
    IAsset getCurrency();

    /**
     *
     * @return security type
     */
    ISecurityType getSecurityType();

    /**
     *
     * @return symbol at broker
     */
    String getSymbolAtBroker();

    /**
     *
     * @return
     */
    Object getIdAtBroker();

    /**
     *
     * @return FIX symbol
     */
    String getFixSymbol();

    /**
     *
     * @return data level
     */
    ISecurityIdentifierLevel getLevel();

    /**
     * @return the tickSize minimum amount that the price of the market can change (minimum price movement of a trading instrument)
     */
    default BigDecimal getTickSize() {
        return TICK_SIZE;
    }

    /**
     *
     * @return contract multiplier
     */
    BigDecimal getContractMultiplier();

    /**
     *
     * @return true, if security is active (it's be)
     */
    boolean isActive();

    /**
     * In case of option asset type, option exersize style is either AMERICAN or EUROPEAN. For all other asset types, it's unspecified.
     * @return AMERICAN or EUROPEAN
     */
    IOptionExerciseStyle getOptionExerciseStyle();
}






