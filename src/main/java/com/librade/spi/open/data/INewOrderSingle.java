package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IOrderType;
import com.librade.spi.open.data.definitions.ITimeInForce;
import java.math.BigDecimal;

/**
 * New Order single.
 */
public interface INewOrderSingle extends IClientToBrokerMessage {

    /**
     *
     * @return
     */
    long getExpire();

    /**
     *
     * @return
     */
    IOrderType getOrderTypeEnum();

    /**
     *
     * @return
     */
    String getOrderType();

    /**
     *
     * @return
     */
    int getOrderTypeId();

    /**
     *
     * @return
     */
    BigDecimal getPrice();

    /**
     *
     * @return
     */
    BigDecimal getStopPrice();

    /**
     *
     * @return
     */
    BigDecimal getQuantity();

    /**
     *
     * @return
     */
    ITimeInForce getTimeInForceEnum();

    /**
     *
     * @return
     */
    String getTimeInForce();

    /**
     *
     * @return
     */
    int getTimeInForceId();

    /**
     *
     * @return transact time
     */
    long getTransactTime();

    /**
     *
     * @return note
     */
    String getNote();
}





