package com.librade.spi.open.data.patterns;

import com.librade.spi.open.data.definitions.IAsset;
import com.librade.spi.open.data.definitions.IBroker;
import com.librade.spi.open.data.definitions.IExchange;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ITimeInterval;
import com.nordlicht.IDefinition;
import com.nordlicht.ITimeSeriesData;

import java.math.BigDecimal;

import static com.nordlicht.IDefinition.UNSPECIFIED;
import static com.nordlicht.IDefinition.UNSPECIFIED_ID;

public interface IPattern extends ITimeSeriesData {

    Long getId();

    Long getResultUid();

    ISecurityIdentifier getSecurityIdentifier();

    default String getIdentifier() {
        return getSecurityIdentifier() == null ? UNSPECIFIED : getSecurityIdentifier().getName();
    }

    default ISecurityIdentifier getIdentifierEnum() {
        return getSecurityIdentifier();
    }

    default int getIdentifierId() {
        return getSecurityIdentifier() == null ? UNSPECIFIED_ID : getSecurityIdentifier().getId();
    }

    IAsset getSymbolIdentifier();

    Integer getSymbolIdentifierId();

    default String getSymbol() {
        return getSymbolIdentifier() == null ? UNSPECIFIED : getSymbolIdentifier().getName();
    }

    default IAsset getSymbolEnum() {
        return getSymbolIdentifier();
    }

    default int getSymbolId() {
        return getSymbolIdentifier() == null ? UNSPECIFIED_ID : getSymbolIdentifier().getId();
    }

    default IAsset getCurrencyEnum() {
        return getSecurityIdentifier() == null ? null : getSecurityIdentifier().getSymbol();
    }

    IAsset getCurrencyIdentifier();

    Integer getCurrencyIdentifierId();

    IPatternType getPatternType();

    IPatternResultType getPatternResultType();

    IPatternSubType getPatternSubType();

    ITimeInterval getTimeInterval();

    default int getTimeIntervalId() {
        return getTimeInterval() == null ? UNSPECIFIED_ID : getTimeInterval().getId();
    }

    Long getIdentifiedTimestamp();

    Long getPatternIrrelevantTimestamp();

    Long getPatternStartTimestamp();

    Long getPatternStopTimestamp();

    Long getPredictionToTimestamp();

    String getAnalysisText();

    BigDecimal getProbability();

    BigDecimal getQuality();

    Long getExpiresAt();

    String getImageUuid();

    Boolean getRelevant();

    Boolean getComplete();

    Integer getAge();

    Integer getLength();

    Integer getDirection();

    IPatternDirection getPatternDirection();

    Integer getPriority();

    BigDecimal getInitialProfitPotential();

    BigDecimal getSignalEntryLevel();

    BigDecimal getStopLoss();

    BigDecimal getTargetLevel();

    Integer getTargetPeriod();

    String getPatternSubTypeString();

    String getPatternResultTypeString();

    String getPatternDirectionString();

    /**
     * Strips trailing zeroes from every BigDecimal.
     */
    IPattern stripTrailingZeros();

    IPattern copy();

    default IExchange getExchangeEnum() {
        return getSecurityIdentifier() == null ? null : getSecurityIdentifier().getExchange();
    }

    default String getBroker() {
        return getSecurityIdentifier() == null ? null : getSecurityIdentifier().getBroker().getName();
    }

    default IBroker getBrokerEnum() {
        return getSecurityIdentifier() == null ? null : getSecurityIdentifier().getBroker();
    }

    default String getCurrency() {
        return getSecurityIdentifier() == null ? null : getSecurityIdentifier().getCurrency().getName();
    }

    default String getExchange() {
        return getSecurityIdentifier() == null ? null : getSecurityIdentifier().getExchange().getName();
    }

    default int getBrokerId() {
        return getSecurityIdentifier() == null ? 0 : getSecurityIdentifier().getBroker().getId();
    }

    default int getCurrencyId() {
        return getSecurityIdentifier() == null ? 0 : getSecurityIdentifier().getCurrency().getId();
    }

    default int getExchangeId() {
        return getSecurityIdentifier() == null ? 0 : getSecurityIdentifier().getExchange().getId();
    }

    default ISecurityIdentifier getSecurityEnum() {
        return getSecurityIdentifier();
    }

    default String getSecurity() {
        return getSecurityIdentifier() == null ? IDefinition.UNSPECIFIED : getSecurityIdentifier().getName();
    }

    default int getSecurityId() {
        return getSecurityIdentifier() == null ? 0 : getSecurityIdentifier().getId();
    }
}
