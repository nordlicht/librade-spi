package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Price type.
 */
public interface IPriceType extends IDefinition<IPriceType> {

    /**
     * Ask price.
     */
    String ASK = "ASK";

    /**
     * Bid price.
     */
    String BID = "BID";

    /**
     * Mid point.
     */
    String MIDPOINT = "MIDPOINT";
}
