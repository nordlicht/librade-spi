package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IControlMessageType;
import com.nordlicht.ITimeSeriesData;

/**
 * Control message.
 */
public interface IControlMessage extends ITimeSeriesData {

    /**
     *
     * @return
     */
    String getSessionUID();

    /**
     *
     * @return
     */
    int getActualStatus();

    /**
     *
     * @return
     */
    int getFinalStatus();

    /**
     *
     * @return
     */
    IControlMessageType getControlMessageTypeEnum();

    /**
     *
     * @return
     */
    String getControlMessageType();

    /**
     *
     * @return
     */
    Throwable getThrowable();

    /**
     *
     * @return
     */
    String getMessage();

    /**
     *
     * @return
     */
    int getControlMessageTypeId();
}






