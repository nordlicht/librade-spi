package com.librade.spi.open.data.blockchain;

import com.nordlicht.commons.base.serialization.byteable.IJsonByteable;

public class TokenMetadata implements ITokenMetadata {

    private final String  hexChainId;
    private final String  tokenAddress;
    private final String  symbol;
    private final String  name;
    private final int     decimals;
    private final boolean verifiedContract;
    private final boolean possibleSpam;

    public TokenMetadata(
            String hexChainId,
            String tokenAddress,
            String symbol,
            String name,
            int decimals,
            boolean verifiedContract,
            boolean possibleSpam) {
        this.hexChainId = hexChainId;
        this.tokenAddress = tokenAddress;
        this.symbol = symbol;
        this.name = name;
        this.decimals = decimals;
        this.verifiedContract = verifiedContract;
        this.possibleSpam = possibleSpam;
    }

    @Override
    public String getHexChainId() {
        return hexChainId;
    }

    @Override
    public String getTokenAddress() {
        return tokenAddress;
    }

    @Override
    public String getSymbol() {
        return symbol;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getDecimals() {
        return decimals;
    }

    @Override
    public boolean isVerifiedContract() {
        return verifiedContract;
    }

    @Override
    public boolean isPossibleSpam() {
        return possibleSpam;
    }
}
