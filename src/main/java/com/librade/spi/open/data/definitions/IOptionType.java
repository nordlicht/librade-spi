package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * @author Tomas Fecko
 */
public interface IOptionType extends IDefinition<IOptionType> {

    String CALL = "CALL";
    String PUT = "PUT";

}
