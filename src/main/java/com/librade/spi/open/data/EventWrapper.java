package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ITimeInterval;
import com.librade.spi.open.data.patterns.IPattern;
import com.librade.spi.open.data.tradingSignal.ISignal;
import com.nordlicht.ITimeSeriesData;
import com.nordlicht.commons.base.cep.ICEPEvent;
import com.nordlicht.commons.base.cep.IEventWrapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * CEP event wrapper.
 */
public final class EventWrapper implements IEventWrapper {

    public static final BigDecimal			MIN_PRICE = new BigDecimal("0.0000000000000000001");
    public static final BigDecimal			MAX_PRICE = new BigDecimal("10000000000000000000");
    private static final BigDecimal         TWO = new BigDecimal("2");
    private boolean                         mIsNa;
    private boolean					        mIsLast;
    private boolean					        mIsStop;
    private boolean					        mIsFail;
    private boolean					        mIsMark;
    private ICEPEvent                       mCEPEvent;

    private List<IMarketData>           mMarketDataIS;
    private List<IMarketDataBook>       mMarketDataBookIS;
    private List<IAggregatedMarketData> mAggregatedMarketDataIS;
    private List<IExecutionReport>      mExecutionReportIS;
    private List<IMonitoringEvent>      mMonitoringEventIS;
    private List<ICustomEvent>          mCustomEventIS;
    private List<IAccountState>         mAccountStateIS;
    private List<IControlMessage>       mControlMessageIS;
    private List<INotificationEvent>    mNotificationEventIS;
    private List<IAnalyticsEvent>       mAnalyticsEventsIS;
    private List<IPattern>              patternsIS;
    private List<ISignal>               signalsIS;
    private List<IPositionReport>       positionReportIS;

    private List<IMarketData>           mMarketDataRS;
    private List<IMarketDataBook>       mMarketDataBookRS;
    private List<IAggregatedMarketData> mAggregatedMarketDataRS;
    private List<IExecutionReport>      mExecutionReportRS;
    private List<IMonitoringEvent>      mMonitoringEventRS;
    private List<ICustomEvent>          mCustomEventRS;
    private List<IAccountState>         mAccountStateRS;
    private List<IControlMessage>       mControlMessageRS;
    private List<INotificationEvent>    mNotificationEventRS;
    private List<IAnalyticsEvent>       mAnalyticsEventsRS;
    private List<IPattern>              patternsRS;
    private List<ISignal>               signalsRS;
    private List<IPositionReport>       positionReportRS;

    /**
	 * 
	 * @param pEvent cep event
	 */
	public EventWrapper(ICEPEvent pEvent) {
        wrapNoClear(pEvent);
	}

	public EventWrapper stripTrailingZeros() {
        if (mMarketDataIS != null) {
            mMarketDataIS.forEach(IMarketDataParent::stripTrailingZeros);
        }
        if (mMarketDataBookIS != null) {
            mMarketDataBookIS.forEach(IMarketDataParent::stripTrailingZeros);
        }
        if (mAggregatedMarketDataIS != null) {
            mAggregatedMarketDataIS.forEach(IMarketDataParent::stripTrailingZeros);
        }
        if (mExecutionReportIS != null) {
            mExecutionReportIS.forEach(IExecutionReport::stripTrailingZeros);
        }
        if (mAccountStateIS != null) {
            mAccountStateIS.forEach(IAccountState::stripTrailingZeros);
        }
        if (mAnalyticsEventsIS != null) {
            mAnalyticsEventsIS.forEach(IAnalyticsEvent::stripTrailingZeros);
        }
        if (patternsIS != null) {
            patternsIS.forEach(IPattern::stripTrailingZeros);
        }
        if (signalsIS != null) {
            signalsIS.forEach(ISignal::stripTrailingZeros);
        }
        if (positionReportIS != null) {
            positionReportIS.forEach(IPositionReport::stripTrailingZeros);
        }

        if (mMarketDataRS != null) {
            mMarketDataRS.forEach(IMarketDataParent::stripTrailingZeros);
        }
        if (mMarketDataBookRS != null) {
            mMarketDataBookRS.forEach(IMarketDataParent::stripTrailingZeros);
        }
        if (mAggregatedMarketDataRS != null) {
            mAggregatedMarketDataRS.forEach(IMarketDataParent::stripTrailingZeros);
        }
        if (mExecutionReportRS != null) {
            mExecutionReportRS.forEach(IExecutionReport::stripTrailingZeros);
        }
        if (mAccountStateRS != null) {
            mAccountStateRS.forEach(IAccountState::stripTrailingZeros);
        }
        if (mAnalyticsEventsRS != null) {
            mAnalyticsEventsRS.forEach(IAnalyticsEvent::stripTrailingZeros);
        }
        if (patternsRS != null) {
            patternsRS.forEach(IPattern::stripTrailingZeros);
        }
        if (signalsRS != null) {
            signalsRS.forEach(ISignal::stripTrailingZeros);
        }
        if (positionReportRS != null) {
            positionReportRS.forEach(IPositionReport::stripTrailingZeros);
        }
        return this;
    }

    /**
     * Method wraps CEPEvent to wrapper clearing all previous state and data of wrapper.
     * @param pEvent cep event
     * @return this
     */
	public EventWrapper wrap(ICEPEvent pEvent) {
        clearLists();
        return wrapNoClear(pEvent);
    }

    private EventWrapper wrapNoClear(ICEPEvent pEvent) {
        mCEPEvent = pEvent;
        mIsNa = false;
        mIsLast = false;
        mIsStop = false;
        mIsFail = false;
        mIsMark = false;
        if (pEvent == null || (pEvent.isInsertStreamEmpty() && pEvent.isRemoveStreamEmpty())) {
            mIsNa = true;
        } else {
            if (!pEvent.isInsertStreamEmpty()) {
                boolean isLast = false;
                boolean isStop = false;
                boolean isFail = false;
                boolean isMark = false;
                for (Map<String, Object> map : pEvent.getInputStreamOfObjectsFromCEP()) {
                    for (Object eventPart : map.values()) {
                        if (eventPart instanceof IControlMessage) {
                            if (String.valueOf(((IControlMessage) eventPart).getControlMessageType()).startsWith("LAST")) {
                                isLast = true;
                            } else if ("STOP".equals(((IControlMessage) eventPart).getControlMessageType())) {
                                isStop = true;
                            } else if ("FAIL".equals(((IControlMessage) eventPart).getControlMessageType())) {
                                isFail = true;
                            } else if ("MARK".equals(((IControlMessage) eventPart).getControlMessageType())) {
                                isMark = true;
                            }
                        }
                    }
                }
                mIsLast = isLast;
                mIsStop = isStop;
                mIsFail = isFail;
                mIsMark = isMark;
                processInputStreamFromEvent(pEvent.getInputStreamOfObjectsFromCEP());
            }
            if (!pEvent.isRemoveStreamEmpty()) {
                processRemoveStreamFromEvent(pEvent.getRemoveStreamOfObjectsFromCEP());
            }
        }
        return this;
    }

    private void clearLists() {
        if (mMarketDataIS != null && !mMarketDataIS.isEmpty()) {
            mMarketDataIS.clear();
        }
        if (mMarketDataBookIS != null && !mMarketDataBookIS.isEmpty()) {
            mMarketDataBookIS.clear();
        }
        if (mAggregatedMarketDataIS != null && !mAggregatedMarketDataIS.isEmpty()) {
            mAggregatedMarketDataIS.clear();
        }
        if (mExecutionReportIS != null && !mExecutionReportIS.isEmpty()) {
            mExecutionReportIS.clear();
        }
        if (mMonitoringEventIS != null && !mMonitoringEventIS.isEmpty()) {
            mMonitoringEventIS.clear();
        }
        if (mCustomEventIS != null && !mCustomEventIS.isEmpty()) {
            mCustomEventIS.clear();
        }
        if (mAccountStateIS != null && !mAccountStateIS.isEmpty()) {
            mAccountStateIS.clear();
        }
        if (mControlMessageIS != null && !mControlMessageIS.isEmpty()) {
            mControlMessageIS.clear();
        }
        if (mNotificationEventIS != null && !mNotificationEventIS.isEmpty()) {
            mNotificationEventIS.clear();
        }
        if (mAnalyticsEventsIS != null && !mAnalyticsEventsIS.isEmpty()) {
            mAnalyticsEventsIS.clear();
        }
        if (patternsIS != null && !patternsIS.isEmpty()) {
            patternsIS.clear();
        }
        if (signalsIS != null && !signalsIS.isEmpty()) {
            signalsIS.clear();
        }
        if (positionReportIS != null && !positionReportIS.isEmpty()) {
            positionReportIS.clear();
        }

        if (mMarketDataRS != null && !mMarketDataRS.isEmpty()) {
            mMarketDataRS.clear();
        }
        if (mMarketDataBookRS != null && !mMarketDataBookRS.isEmpty()) {
            mMarketDataBookRS.clear();
        }
        if (mAggregatedMarketDataRS != null && !mAggregatedMarketDataRS.isEmpty()) {
            mAggregatedMarketDataRS.clear();
        }
        if (mExecutionReportRS != null && !mExecutionReportRS.isEmpty()) {
            mExecutionReportRS.clear();
        }
        if (mMonitoringEventRS != null && !mMonitoringEventRS.isEmpty()) {
            mMonitoringEventRS.clear();
        }
        if (mCustomEventRS != null && !mCustomEventRS.isEmpty()) {
            mCustomEventRS.clear();
        }
        if (mAccountStateRS != null && !mAccountStateRS.isEmpty()) {
            mAccountStateRS.clear();
        }
        if (mControlMessageRS != null && !mControlMessageRS.isEmpty()) {
            mControlMessageRS.clear();
        }
        if (mNotificationEventRS != null && !mNotificationEventRS.isEmpty()) {
            mNotificationEventRS.clear();
        }
        if (mAnalyticsEventsRS != null && !mAnalyticsEventsRS.isEmpty()) {
            mAnalyticsEventsRS.clear();
        }
        if (patternsRS != null && !patternsRS.isEmpty()) {
            patternsRS.clear();
        }
        if (signalsRS != null && !signalsRS.isEmpty()) {
            signalsRS.clear();
        }
        if (positionReportRS != null && positionReportRS.isEmpty()) {
            positionReportRS.clear();
        }
    }

	/**
	 * 
	 * @param pStream input stream
	 */
	private void processInputStreamFromEvent(List<LinkedHashMap<String, Object>> pStream) {
		if (pStream == null || pStream.isEmpty()) {
			return;
		}
		for (Map<String, Object> row : pStream) {
            for (Map.Entry<String, Object> entry : row.entrySet()) {
                Object object = entry.getValue();
                String key = entry.getKey();
                if (object instanceof IMarketData || String.valueOf(key).startsWith("MarketData")) {
                    if (mMarketDataIS == null) {
                        mMarketDataIS = new ArrayList<>();
                    }
                    mMarketDataIS.add((IMarketData) object);
                } else if (object instanceof IMarketDataBook || String.valueOf(key).startsWith("MarketDataBook")) {
                    if (mMarketDataBookIS == null) {
                        mMarketDataBookIS = new ArrayList<>();
                    }
                    mMarketDataBookIS.add((IMarketDataBook) object);
                } else if (object instanceof IAggregatedMarketData || String.valueOf(key).startsWith("AggregatedMarketData")) {
                    if (mAggregatedMarketDataIS == null) {
                        mAggregatedMarketDataIS = new ArrayList<>();
                    }
                    mAggregatedMarketDataIS.add((IAggregatedMarketData) object);
                } else if (object instanceof IExecutionReport || String.valueOf(key).startsWith("ExecutionReport")) {
                    if (mExecutionReportIS == null) {
                        mExecutionReportIS = new ArrayList<>();
                    }
                    mExecutionReportIS.add((IExecutionReport) object);
                } else if (object instanceof IMonitoringEvent || String.valueOf(key).startsWith("MonitoringEvent")) {
                    if (mMonitoringEventIS == null) {
                        mMonitoringEventIS = new ArrayList<>();
                    }
                    mMonitoringEventIS.add((IMonitoringEvent) object);
                } else if (object instanceof ICustomEvent || String.valueOf(key).startsWith("CustomEvent")) {
                    if (mCustomEventIS == null) {
                        mCustomEventIS = new ArrayList<>();
                    }
                    mCustomEventIS.add((ICustomEvent) object);
                } else if (object instanceof IAccountState || String.valueOf(key).startsWith("AccountState")) {
                    if (mAccountStateIS == null) {
                        mAccountStateIS = new ArrayList<>();
                    }
                    mAccountStateIS.add((IAccountState) object);
                } else if (object instanceof INotificationEvent || String.valueOf(key).startsWith("NotificationEvent")) {
                    if (mNotificationEventIS == null) {
                        mNotificationEventIS = new ArrayList<>();
                    }
                    mNotificationEventIS.add((INotificationEvent) object);
                } else if (object instanceof IControlMessage || String.valueOf(key).startsWith("ControlMessage")) {
                    if (mControlMessageIS == null) {
                        mControlMessageIS = new ArrayList<>();
                    }
                    mControlMessageIS.add((IControlMessage) object);
                } else if (object instanceof IAnalyticsEvent || String.valueOf(key).startsWith("AnalyticsEvent")) {
                    if (mAnalyticsEventsIS == null) {
                        mAnalyticsEventsIS = new ArrayList<>();
                    }
                    mAnalyticsEventsIS.add((IAnalyticsEvent) object);
                } else if (object instanceof IPattern || String.valueOf(key).startsWith("Pattern")) {
                    if (patternsIS == null) {
                        patternsIS = new ArrayList<>();
                    }
                    patternsIS.add((IPattern) object);
                } else if (object instanceof ISignal || String.valueOf(key).startsWith("Signal")) {
                    if (signalsIS == null) {
                        signalsIS = new ArrayList<>();
                    }
                    signalsIS.add((ISignal) object);
                } else if (object instanceof IPositionReport || String.valueOf(key).startsWith("PositionReport")) {
                    if (positionReportIS == null) {
                        positionReportIS = new ArrayList<>();
                    }
                    positionReportIS.add((IPositionReport) object);
                }
            }
		}
	}

    /**
     *
     * @param pStream remove stream
     */
    private void processRemoveStreamFromEvent(List<LinkedHashMap<String, Object>> pStream) {
        if (pStream == null || pStream.isEmpty()) {
            return;
        }
        for (Map<String, Object> row : pStream) {
            for (Map.Entry<String, Object> entry : row.entrySet()) {
                Object object = entry.getValue();
                String key = entry.getKey();
                if (object instanceof IMarketData || String.valueOf(key).startsWith("MarketData")) {
                    if (mMarketDataRS == null) {
                        mMarketDataRS = new ArrayList<>();
                    }
                    mMarketDataRS.add((IMarketData) object);
                } else if (object instanceof IMarketDataBook || String.valueOf(key).startsWith("MarketDataBook")) {
                    if (mMarketDataBookRS == null) {
                        mMarketDataBookRS = new ArrayList<>();
                    }
                    mMarketDataBookRS.add((IMarketDataBook) object);
                } else if (object instanceof IAggregatedMarketData || String.valueOf(key).startsWith("AggregatedMarketData")) {
                    if (mAggregatedMarketDataRS == null) {
                        mAggregatedMarketDataRS = new ArrayList<>();
                    }
                    mAggregatedMarketDataRS.add((IAggregatedMarketData) object);
                } else if (object instanceof IExecutionReport || String.valueOf(key).startsWith("ExecutionReport")) {
                    if (mExecutionReportRS == null) {
                        mExecutionReportRS = new ArrayList<>();
                    }
                    mExecutionReportRS.add((IExecutionReport) object);
                } else if (object instanceof IMonitoringEvent || String.valueOf(key).startsWith("MonitoringEvent")) {
                    if (mMonitoringEventRS == null) {
                        mMonitoringEventRS = new ArrayList<>();
                    }
                    mMonitoringEventRS.add((IMonitoringEvent) object);
                } else if (object instanceof ICustomEvent || String.valueOf(key).startsWith("CustomEvent")) {
                    if (mCustomEventRS == null) {
                        mCustomEventRS = new ArrayList<>();
                    }
                    mCustomEventRS.add((ICustomEvent) object);
                } else if (object instanceof IAccountState || String.valueOf(key).startsWith("AccountState")) {
                    if (mAccountStateRS == null) {
                        mAccountStateRS = new ArrayList<>();
                    }
                    mAccountStateRS.add((IAccountState) object);
                } else if (object instanceof INotificationEvent || String.valueOf(key).startsWith("NotificationEvent")) {
                    if (mNotificationEventRS == null) {
                        mNotificationEventRS = new ArrayList<>();
                    }
                    mNotificationEventRS.add((INotificationEvent) object);
                } else if (object instanceof IControlMessage || String.valueOf(key).startsWith("ControlMessage")) {
                    if (mControlMessageRS == null) {
                        mControlMessageRS = new ArrayList<>();
                    }
                    mControlMessageRS.add((IControlMessage) object);
                } else if (object instanceof IAnalyticsEvent || String.valueOf(key).startsWith("AnalyticsEvent")) {
                    if (mAnalyticsEventsRS == null) {
                        mAnalyticsEventsRS = new ArrayList<>();
                    }
                    mAnalyticsEventsRS.add((IAnalyticsEvent) object);
                } else if (object instanceof IPattern || String.valueOf(key).startsWith("Pattern")) {
                    if (patternsRS == null) {
                        patternsRS = new ArrayList<>();
                    }
                    patternsRS.add((IPattern) object);
                } else if (object instanceof ISignal || String.valueOf(key).startsWith("Signal")) {
                    if (signalsRS == null) {
                        signalsRS = new ArrayList<>();
                    }
                    signalsRS.add((ISignal) object);
                } else if (object instanceof IPositionReport || String.valueOf(key).startsWith("PositionReport")) {
                    if (positionReportRS == null) {
                        positionReportRS = new ArrayList<>();
                    }
                    positionReportRS.add((IPositionReport) object);
                }
            }
        }
    }

    /**
     *
     * @return if there is an control message with mark type
     */
    public boolean isMark() {
        return mIsMark;
    }

    /**
     *
     * @return current timestamp
     */
    public long getCurrentTimestamp() {
        return mCEPEvent.getCurrentTimestamp();
    }

    /**
     *
     * @return next timestamp
     */
    public long getNextTimestamp() {
        return mCEPEvent.getNextTimestamp();
    }

    /**
     *
     * @return input stream from cep engine
     */
    public List<LinkedHashMap<String, Object>> getInputStream() {
        if (mCEPEvent == null) {
            return null;
        }
        return mCEPEvent.getInputStreamOfObjectsFromCEP();
    }

    /**
     *
     * @return remove stream from cep engine
     */
    public List<LinkedHashMap<String, Object>> getRemoveStream() {
        if (mCEPEvent == null) {
            return null;
        }
        return mCEPEvent.getRemoveStreamOfObjectsFromCEP();
    }
	
	/**
	 * 
	 * @return true, is the event is NaN or null
	 */
	public boolean isNa() {
		return mIsNa;
	}

	/**
	 * 
	 * @return true, if wrapper contains last event
	 */
	public boolean isLast() {
		return mIsLast;
	}

    /**
     *
     * @return true, if fail happen, which prevents from continuing trading session
     */
    public boolean isFail() {
        return mIsFail;
    }

    /**
     *
     * @return true, if stop event is present in the wrapper
     */
    public boolean isStop() {
        return mIsStop;
    }

    /**
     *
     * @return true, if wrapper contains marketData
     */
    public boolean containsMarketData() {
        return (mMarketDataIS != null && !mMarketDataIS.isEmpty())
                || (mMarketDataRS != null && !mMarketDataRS.isEmpty());
    }

    /**
     *
     * @return true, if wrapper contains marketData
     */
    public boolean containsMarketDataBook() {
        return (mMarketDataBookIS != null && !mMarketDataBookIS.isEmpty())
                || (mMarketDataBookRS != null && !mMarketDataBookRS.isEmpty());
    }

    /**
     *
     * @return true, if wrapper contains marketData
     */
    public boolean containsAggregatedMarketData() {
        return (mAggregatedMarketDataIS != null && !mAggregatedMarketDataIS.isEmpty())
                || (mAggregatedMarketDataRS != null && !mAggregatedMarketDataRS.isEmpty());
    }

	/**
	 * 
	 * @return true, if wrapper contains some execution reports
	 */
	public boolean containsExecutionReports() {
		return (mExecutionReportIS != null && !mExecutionReportIS.isEmpty())
                || (mExecutionReportRS != null && !mExecutionReportRS.isEmpty());
	}

	/**
	 * 
	 * @return true, if monitoring events are present in wrapper
	 */
	public boolean containsMonitoringEvents() {
		return (mMonitoringEventIS != null && !mMonitoringEventIS.isEmpty())
                || (mMonitoringEventRS != null && !mMonitoringEventRS.isEmpty());
	}

    /**
     *
     * @return true, if custom events are present in wrapper
     */
    public boolean containsCustomEvents() {
        return (mCustomEventIS != null && !mCustomEventIS.isEmpty())
                || (mCustomEventRS != null && !mCustomEventRS.isEmpty());
    }

    /**
     *
     * @return true, if account states are present in wrapper
     */
    public boolean containsAccountStates() {
        return (mAccountStateIS != null && !mAccountStateIS.isEmpty())
                || (mAccountStateRS != null && !mAccountStateRS.isEmpty());
    }

    /**
     *
     * @return true, if notification events are present in wrapper
     */
    public boolean containsNotificationEvents() {
        return (mNotificationEventIS != null && !mNotificationEventIS.isEmpty())
                || (mNotificationEventRS != null && !mNotificationEventRS.isEmpty());
    }

    /**
     *
     * @return true, if control messages are present in wrapper
     */
    public boolean containsControlMessages() {
        return (mControlMessageIS != null && !mControlMessageIS.isEmpty())
                || (mControlMessageRS != null && !mControlMessageRS.isEmpty());
    }

    /**
     *
     * @return true, if analytics events are present in wrapper
     */
    public boolean containsAnalyticsEvents() {
        return (mAnalyticsEventsIS != null && !mAnalyticsEventsIS.isEmpty())
                || (mAnalyticsEventsRS != null && !mAnalyticsEventsRS.isEmpty());
    }

    /**
     *
     * @return true, if patterns present in wrapper
     */
    public boolean containsPatterns() {
        return (patternsIS != null && !patternsIS.isEmpty())
                || (patternsRS != null && !patternsRS.isEmpty());
    }

    public boolean containsSignals() {
        return (signalsIS != null && !signalsIS.isEmpty()) || (signalsRS != null && !signalsRS.isEmpty());
    }

    public boolean containsPositionReports() {
        return (positionReportIS != null && !positionReportIS.isEmpty()) || (positionReportRS != null && !positionReportRS.isEmpty());
    }

    /**
     *
     * @return the query ID in this wrapper
     */
    public String getStatementId() {
        return mCEPEvent == null ? null : mCEPEvent.getQueryId();
    }

    /**
     *
     * @return values
     */
    public List<IMarketData> getMarketData() {
        if (mMarketDataIS != null && !mMarketDataIS.isEmpty()) {
            return mMarketDataIS;
        } else if (mMarketDataRS != null && !mMarketDataRS.isEmpty()) {
            return mMarketDataRS;
        }
        return Collections.emptyList();
    }

    /**
     *
     * @return values
     */
    public List<IMarketDataBook> getMarketDataBook() {
        if (mMarketDataBookIS != null && !mMarketDataBookIS.isEmpty()) {
            return mMarketDataBookIS;
        } else if (mMarketDataBookRS != null && !mMarketDataBookRS.isEmpty()) {
            return mMarketDataBookRS;
        }
        return Collections.emptyList();
    }

    /**
     *
     * @return values
     */
    public List<IAggregatedMarketData> getAggregatedMarketData() {
        if (mAggregatedMarketDataIS != null && !mAggregatedMarketDataIS.isEmpty()) {
            return mAggregatedMarketDataIS;
        } else if (mAggregatedMarketDataRS != null && !mAggregatedMarketDataRS.isEmpty()) {
            return mAggregatedMarketDataRS;
        }
        return Collections.emptyList();
    }

	/**
	 * 
	 * @return list of execution reports
	 */
	public List<IExecutionReport> getExecutionReports() {
        if (mExecutionReportIS != null && !mExecutionReportIS.isEmpty()) {
            return mExecutionReportIS;
        } else if (mExecutionReportRS != null && !mExecutionReportRS.isEmpty()) {
            return mExecutionReportRS;
        }
        return Collections.emptyList();
	}

	/**
	 * 
	 * @return lists of monitoring events
	 */
	public List<IMonitoringEvent> getMonitoringEvents() {
        if (mMonitoringEventIS != null && !mMonitoringEventIS.isEmpty()) {
            return mMonitoringEventIS;
        } else if (mMonitoringEventRS != null && !mMonitoringEventRS.isEmpty()) {
            return mMonitoringEventRS;
        }
        return Collections.emptyList();
	}

	/**
     *
     * @return values
     */
    public List<ICustomEvent> getCustomEvents() {
        if (mCustomEventIS != null && !mCustomEventIS.isEmpty()) {
            return mCustomEventIS;
        } else if (mCustomEventRS != null && !mCustomEventRS.isEmpty()) {
            return mCustomEventRS;
        }
        return Collections.emptyList();
    }

    /**
     *
     * @return values
     */
    public List<IAccountState> getAccountStates() {
        if (mAccountStateIS != null && !mAccountStateIS.isEmpty()) {
            return mAccountStateIS;
        } else if (mAccountStateRS != null && !mAccountStateRS.isEmpty()) {
            return mAccountStateRS;
        }
        return Collections.emptyList();
    }

    /**
     *
     * @return values
     */
    public List<IControlMessage> getControlMessages() {
        if (mControlMessageIS != null && !mControlMessageIS.isEmpty()) {
            return mControlMessageIS;
        } else if (mControlMessageRS != null && !mControlMessageRS.isEmpty()) {
            return mControlMessageRS;
        }
        return Collections.emptyList();
    }

    /**
     *
     * @return values
     */
    public List<INotificationEvent> getNotificationEvents() {
        if (mNotificationEventIS != null && !mNotificationEventIS.isEmpty()) {
            return mNotificationEventIS;
        } else if (mNotificationEventRS != null && !mNotificationEventRS.isEmpty()) {
            return mNotificationEventRS;
        }
        return Collections.emptyList();
    }

    /**
     *
     * @return values
     */
    public List<IAnalyticsEvent> getAnalyticsEvents() {
        if (mAnalyticsEventsIS != null && !mAnalyticsEventsIS.isEmpty()) {
            return mAnalyticsEventsIS;
        } else if (mAnalyticsEventsRS != null && !mAnalyticsEventsRS.isEmpty()) {
            return mAnalyticsEventsRS;
        }
        return Collections.emptyList();
    }

    /**
     *
     * @return values
     */
    public List<IPattern> getPatterns() {
        if (patternsIS != null && !patternsIS.isEmpty()) {
            return patternsIS;
        } else if (patternsRS != null && !patternsRS.isEmpty()) {
            return patternsRS;
        }
        return Collections.emptyList();
    }

    public List<ISignal> getSignals() {
        if (signalsIS != null && !signalsIS.isEmpty()) {
            return signalsIS;
        } else if (signalsRS != null && !signalsRS.isEmpty()) {
            return signalsRS;
        }
        return Collections.emptyList();
    }

    public List<IPositionReport> getPositionReports() {
        if (positionReportIS != null && !positionReportIS.isEmpty()) {
            return positionReportIS;
        } else if (positionReportRS != null && !positionReportRS.isEmpty()) {
            return positionReportRS;
        }
        return Collections.emptyList();
    }

    /**
     *
     * @return values
     */
    public List<IMarketData> getMarketDataIS() {
        if (mMarketDataIS == null) {
            return Collections.emptyList();
        }
        return mMarketDataIS;
    }

    /**
     *
     * @return values
     */
    public List<IMarketDataBook> getMarketDataBookIS() {
        return Objects.requireNonNullElse(mMarketDataBookIS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<IAggregatedMarketData> getAggregatedMarketDataIS() {
        return Objects.requireNonNullElse(mAggregatedMarketDataIS, Collections.emptyList());
    }

    /**
     *
     * @return list of execution reports
     */
    public List<IExecutionReport> getExecutionReportsIS() {
        return Objects.requireNonNullElse(mExecutionReportIS, Collections.emptyList());
    }

    /**
     *
     * @return lists of monitoring events
     */
    public List<IMonitoringEvent> getMonitoringEventsIS() {
        return Objects.requireNonNullElse(mMonitoringEventIS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<ICustomEvent> getCustomEventsIS() {
        if (mCustomEventIS == null) {
            return Collections.emptyList();
        }
        return mCustomEventIS;
    }

    /**
     *
     * @return values
     */
    public List<IAccountState> getAccountStatesIS() {
        if (mAccountStateIS == null) {
            return Collections.emptyList();
        }
        return mAccountStateIS;
    }

    /**
     *
     * @return values
     */
    public List<IControlMessage> getControlMessagesIS() {
        return Objects.requireNonNullElse(mControlMessageIS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<INotificationEvent> getNotificationEventsIS() {
        return Objects.requireNonNullElse(mNotificationEventIS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<IAnalyticsEvent> getAnalyticsEventsIS() {
        return Objects.requireNonNullElse(mAnalyticsEventsIS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<IPattern> getPatternsIS() {
        return Objects.requireNonNullElse(patternsIS, Collections.emptyList());
    }

    public List<ISignal> getSignalsIS() { return Objects.requireNonNullElse(signalsIS, Collections.emptyList()); }

    public List<IPositionReport> getPositionReportsIS() {return Objects.requireNonNullElse(positionReportIS, Collections.emptyList());}

    /**
     *
     * @return values
     */
    public List<IMarketData> getMarketDataRS() {
        if (mMarketDataRS == null) {
            return Collections.emptyList();
        }
        return mMarketDataRS;
    }

    /**
     *
     * @return values
     */
    public List<IMarketDataBook> getMarketDataBookRS() {
        return Objects.requireNonNullElse(mMarketDataBookRS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<IAggregatedMarketData> getAggregatedMarketDataRS() {
        return Objects.requireNonNullElse(mAggregatedMarketDataRS, Collections.emptyList());
    }

    /**
     *
     * @return list of execution reports
     */
    public List<IExecutionReport> getExecutionReportsRS() {
        return Objects.requireNonNullElse(mExecutionReportRS, Collections.emptyList());
    }

    /**
     *
     * @return lists of monitoring events
     */
    public List<IMonitoringEvent> getMonitoringEventsRS() {
        return Objects.requireNonNullElse(mMonitoringEventRS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<ICustomEvent> getCustomEventsRS() {
        if (mCustomEventRS == null) {
            return Collections.emptyList();
        }
        return mCustomEventRS;
    }

    /**
     *
     * @return values
     */
    public List<IAccountState> getAccountStatesRS() {
        if (mAccountStateRS == null) {
            return Collections.emptyList();
        }
        return mAccountStateRS;
    }

    /**
     *
     * @return values
     */
    public List<IControlMessage> getControlMessagesRS() {
        return Objects.requireNonNullElse(mControlMessageRS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<INotificationEvent> getNotificationEventsRS() {
        return Objects.requireNonNullElse(mNotificationEventRS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<IAnalyticsEvent> getAnalyticsEventsRS() {
        return Objects.requireNonNullElse(mAnalyticsEventsRS, Collections.emptyList());
    }

    /**
     *
     * @return values
     */
    public List<IPattern> getPatternsRS() {
        return Objects.requireNonNullElse(patternsRS, Collections.emptyList());
    }

    public List<ISignal> getSignalsRS() { return Objects.requireNonNullElse(signalsRS, Collections.emptyList()); }

    public List<IPositionReport> getPositionReportRS() { return Objects.requireNonNullElse(positionReportRS, Collections.emptyList()); }

    /**
     * ASK and BID are default price types in OHLCView, otherwise you can specify price types in CEP query for ohlcview(parameter no. 3)
     * @return
     */
    public List<IAggregatedMarketData> createAggregatedMarketDataPricesFrom_ASK_BID_OHLC(IAggregatedMarketDataFactory factory, ITimeInterval timeInterval) {
        if (!containsMarketData()) {
            return Collections.emptyList();
        }
        int noOfPriceTypes = 3; //ASK and BID and MID
        List<IMarketData> marketData = getMarketData();
        int noOfItemsInOneGroup = (noOfPriceTypes * 4 + 1);
        int noOfGroups = marketData.size() / noOfItemsInOneGroup;
        List<IAggregatedMarketData> returnList = new ArrayList<>(noOfGroups);
        for (int i = 0; i < noOfGroups; i++) {
            int offset = i * noOfItemsInOneGroup;
            IMarketData openAsk = marketData.get(offset);
            IMarketData highAsk = marketData.get(offset + 1);
            IMarketData lowAsk = marketData.get(offset + 2);
            IMarketData closeAsk = marketData.get(offset + 3);
            IMarketData openBid = marketData.get(offset + 4);
            IMarketData highBid = marketData.get(offset + 5);
            IMarketData lowBid = marketData.get(offset + 6);
            IMarketData closeBid = marketData.get(offset + 7);
            IMarketData volume = marketData.get(offset + 8);
            List<IMarketData> iMarketData = Arrays.asList(openAsk, highAsk, lowAsk, closeAsk, openBid, highBid, lowBid, closeBid);
            IAggregatedMarketData aggregatedMarketData = factory.createAggregatedMarketData(
                    openAsk == null ? openBid.getSecurityEnum() : openAsk.getSecurityEnum(),
                    openAsk == null ? openBid.getTimestamp() : openAsk.getTimestamp(), timeInterval,
                    countMidPointPrice(openAsk, openBid, iMarketData),
                    countMidPointPrice(highAsk, highBid, iMarketData),
                    countMidPointPrice(lowAsk, lowBid, iMarketData),
                    countMidPointPrice(closeAsk, closeBid, iMarketData),
                    volume.getVolumeBD());
            if (aggregatedMarketData != null) {
                returnList.add(aggregatedMarketData);
            }
        }
        return returnList;
    }

    /**
     * ASK and BID are default price types in OHLCView, otherwise you can specify price types in CEP query for ohlcview(parameter no. 3)
     * @return
     */
    public List<IAggregatedMarketData> createAggregatedMarketDataPricesFrom_ASK_BID_MID_OHLC(IAggregatedMarketDataFactory factory, ITimeInterval timeInterval) {
        if (!containsMarketData()) {
            return Collections.emptyList();
        }
        int noOfPriceTypes = 3; //ASK and BID and MID
        List<IMarketData> marketData = getMarketData();
        int noOfItemsInOneGroup = (noOfPriceTypes * 4 + 1);
        int noOfGroups = marketData.size() / noOfItemsInOneGroup;
        List<IAggregatedMarketData> returnList = new ArrayList<>(noOfGroups);
        for (int i = 0; i < noOfGroups; i++) {
            int offset = i * noOfItemsInOneGroup;
            IMarketData openAsk = marketData.get(offset);
            IMarketData highAsk = marketData.get(offset + 1);
            IMarketData lowAsk = marketData.get(offset + 2);
            IMarketData closeAsk = marketData.get(offset + 3);
            IMarketData openBid = marketData.get(offset + 4);
            IMarketData highBid = marketData.get(offset + 5);
            IMarketData lowBid = marketData.get(offset + 6);
            IMarketData closeBid = marketData.get(offset + 7);

            IMarketData openMid = marketData.get(offset + 8);
            IMarketData highMid = marketData.get(offset + 9);
            IMarketData lowMid = marketData.get(offset + 10);
            IMarketData closeMid = marketData.get(offset + 11);

            IMarketData volume = marketData.get(offset + 12);

            IAggregatedMarketData aggregatedMarketData = null;
            if (check(openMid) && check(highMid) && check(lowMid) && check(closeMid)) {
                aggregatedMarketData = factory.createAggregatedMarketData(
                        openMid.getSecurityEnum(),
                        openMid.getTimestamp(), timeInterval,
                        openMid.getPriceBD(),
                        highMid.getPriceBD(),
                        lowMid.getPriceBD(),
                        closeMid.getPriceBD(),
                        volume.getVolumeBD());
            } else {
                List<IMarketData> iMarketData = Arrays.asList(openAsk, highAsk, lowAsk, closeAsk, openBid, highBid, lowBid, closeBid);
                aggregatedMarketData = factory.createAggregatedMarketData(
                        openAsk == null ? openBid.getSecurityEnum() : openAsk.getSecurityEnum(),
                        openAsk == null ? openBid.getTimestamp() : openAsk.getTimestamp(), timeInterval,
                        countMidPointPrice(openAsk, openBid, iMarketData),
                        countMidPointPrice(highAsk, highBid, iMarketData),
                        countMidPointPrice(lowAsk, lowBid, iMarketData),
                        countMidPointPrice(closeAsk, closeBid, iMarketData),
                        volume.getVolumeBD());
            }

            if (aggregatedMarketData != null) {
                returnList.add(aggregatedMarketData);
            }
        }
        return returnList;
    }

    public void onAggregatedMarketDataPricesFrom_ASK_BID_OHLC(IAggregatedMarketDataVisitor visitor, ITimeInterval timeInterval) {
        if (!containsMarketData()) {
            return;
        }
        int noOfPriceTypes = 2; //ASK and BID
        List<IMarketData> marketData = getMarketData();
        int noOfItemsInOneGroup = (noOfPriceTypes * 4 + 1);
        int noOfGroups = marketData.size() / noOfItemsInOneGroup;
        for (int i = 0; i < noOfGroups; i++) {
            int offset = i * noOfItemsInOneGroup;
            IMarketData openAsk = marketData.get(offset);
            IMarketData highAsk = marketData.get(offset + 1);
            IMarketData lowAsk = marketData.get(offset + 2);
            IMarketData closeAsk = marketData.get(offset + 3);
            IMarketData openBid = marketData.get(offset + 4);
            IMarketData highBid = marketData.get(offset + 5);
            IMarketData lowBid = marketData.get(offset + 6);
            IMarketData closeBid = marketData.get(offset + 7);
            IMarketData volume = marketData.get(offset + 8);

            List<IMarketData> iMarketData = Arrays.asList(openAsk, highAsk, lowAsk, closeAsk, openBid, highBid, lowBid, closeBid);
            visitor.visit(
                    openAsk == null ? openBid.getSecurityEnum() : openAsk.getSecurityEnum(),
                    openAsk == null ? openBid.getTimestamp() : openAsk.getTimestamp(), timeInterval,
                    countMidPointPrice(openAsk, openBid, iMarketData),
                    countMidPointPrice(highAsk, highBid, iMarketData),
                    countMidPointPrice(lowAsk, lowBid, iMarketData),
                    countMidPointPrice(closeAsk, closeBid, iMarketData),
                    volume.getVolumeBD());
        }
    }

    public void onAggregatedMarketDataPricesFrom_ASK_BID_MID_OHLC(IAggregatedMarketDataVisitor visitor, ITimeInterval timeInterval) {
        if (!containsMarketData()) {
            return;
        }
        int noOfPriceTypes = 3; //ASK and BID and MID
        List<IMarketData> marketData = getMarketData();
        int noOfItemsInOneGroup = (noOfPriceTypes * 4 + 1);
        int noOfGroups = marketData.size() / noOfItemsInOneGroup;
        for (int i = 0; i < noOfGroups; i++) {
            int offset = i * noOfItemsInOneGroup;
            IMarketData openAsk = marketData.get(offset);
            IMarketData highAsk = marketData.get(offset + 1);
            IMarketData lowAsk = marketData.get(offset + 2);
            IMarketData closeAsk = marketData.get(offset + 3);

            IMarketData openBid = marketData.get(offset + 4);
            IMarketData highBid = marketData.get(offset + 5);
            IMarketData lowBid = marketData.get(offset + 6);
            IMarketData closeBid = marketData.get(offset + 7);

            IMarketData openMid = marketData.get(offset + 8);
            IMarketData highMid = marketData.get(offset + 9);
            IMarketData lowMid = marketData.get(offset + 10);
            IMarketData closeMid = marketData.get(offset + 11);

            IMarketData volume = marketData.get(offset + 12);

            if (check(openMid) && check(highMid) && check(lowMid) && check(closeMid)) {
                visitor.visit(
                        openMid.getSecurityEnum(),
                        openMid.getTimestamp(), timeInterval,
                        openMid.getPriceBD(),
                        highMid.getPriceBD(),
                        lowMid.getPriceBD(),
                        closeMid.getPriceBD(),
                        volume.getVolumeBD());
            } else {
                List<IMarketData> iMarketData = Arrays.asList(openAsk, highAsk, lowAsk, closeAsk, openBid, highBid, lowBid, closeBid);
                visitor.visit(
                        openAsk == null ? openBid.getSecurityEnum() : openAsk.getSecurityEnum(),
                        openAsk == null ? openBid.getTimestamp() : openAsk.getTimestamp(), timeInterval,
                        countMidPointPrice(openAsk, openBid, iMarketData),
                        countMidPointPrice(highAsk, highBid, iMarketData),
                        countMidPointPrice(lowAsk, lowBid, iMarketData),
                        countMidPointPrice(closeAsk, closeBid, iMarketData),
                        volume.getVolumeBD());
            }
        }
    }

    public static BigDecimal countMidPointPrice(IMarketData pData1, IMarketData pData2, List<IMarketData> pAllData) {
        if (check(pData1) && check(pData2)) {
            return pData1.getPriceBD().add(pData2.getPriceBD()).divide(TWO, 8, RoundingMode.HALF_UP);
        } else if (!check(pData1) && !check(pData2)) {
            return getFirstValidPrice(pAllData);
        } else if (!check(pData1)) {
            return pData2.getPriceBD();
        } else {
            return pData1.getPriceBD();
        }
    }

    public static BigDecimal getFirstValidPrice(List<IMarketData> pAllData) {
        if (pAllData == null || pAllData.isEmpty()) {
            return null;
        }
        for (IMarketData marketData : pAllData) {
            if (marketData != null && marketData.getPriceBD() != null) {
                return marketData.getPriceBD();
            }
        }
        return null;
    }

    public static boolean check(ITimeSeriesData timeSeriesData) {
        if (timeSeriesData instanceof IMarketDataOption) {
            return check((IMarketDataOption) timeSeriesData);
        } else if (timeSeriesData instanceof IMarketDataFuture) {
            return check((IMarketDataFuture) timeSeriesData);
        } else if (timeSeriesData instanceof IMarketData) {
            return check((IMarketData) timeSeriesData);
        } else {
            return false;
        }
    }

    /**
     * Returns true, if marketData is valid.
     * @param pData data
     * @return true if data is OK
     */
    public static boolean check(IMarketData pData) {
        return pData != null
                && pData.getSecurityEnum() != null
                && pData.getTimestamp() > 0
                && pData.getPriceBD() != null
                && pData.getPriceBD().compareTo(MIN_PRICE) >= 0
                && pData.getPriceBD().compareTo(MAX_PRICE) <= 0;
    }

    /**
     * Returns true, if marketData is valid.
     * @param pData data
     * @return true if data is OK
     */
    public static boolean check(IMarketDataFuture pData) {
        return check((IMarketData) pData) && pData.getDeliveryTime() > 0;
    }

    /**
     * Returns true, if marketData is valid.
     * @param pData data
     * @return true if data is OK
     */
    public static boolean check(IMarketDataOption pData) {
        return check((IMarketData) pData)
                && pData.getStrikePriceBD() != null
                && pData.getStrikePriceBD().compareTo(MIN_PRICE) >= 0
                && pData.getStrikePriceBD().compareTo(MAX_PRICE) <= 0
                && pData.getExpirationTime() > 0;
    }

     /**
     * Returns true, if AggregatedMarketData is valid.
     * @param pData data
     * @return true if data is OK
     */
    public static boolean check(IAggregatedMarketData pData) {
        return pData != null
                && pData.isNotEmpty()
                && pData.getTimestamp() > 0
                && pData.getVolumeBD().compareTo(BigDecimal.ZERO) >= 0
                && pData.getOpenBD().compareTo(MIN_PRICE) >= 0
                && pData.getOpenBD().compareTo(MAX_PRICE) <= 0
                && pData.getHighBD().compareTo(MIN_PRICE) >= 0
                && pData.getHighBD().compareTo(MAX_PRICE) <= 0
                && pData.getLowBD().compareTo(MIN_PRICE) >= 0
                && pData.getLowBD().compareTo(MAX_PRICE) <= 0
                && pData.getCloseBD().compareTo(MIN_PRICE) >= 0
                && pData.getCloseBD().compareTo(MAX_PRICE) <= 0;
    }


    /**
     * Returns true, if AggregatedMarketData is valid.
     * @param pData data
     * @return true if data is OK
     */
    public static boolean check(IMarketDataBook pData) {
        if (pData == null || pData.getTimestamp() <= 0) {
            return false;
        }
        if (pData.getAsks() != null) {
            for (IPriceTick ask : pData.getAsks()) {
                if (ask.getPriceBD().compareTo(MIN_PRICE) < 0 || ask.getPriceBD().compareTo(MAX_PRICE) > 0) {
                    return false;
                }
            }
        }
        if (pData.getBids() != null) {
            for (IPriceTick bid : pData.getBids()) {
                if (bid.getPriceBD().compareTo(MIN_PRICE) < 0 || bid.getPriceBD().compareTo(MAX_PRICE) > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    @FunctionalInterface
    public interface IAggregatedMarketDataFactory {
        IAggregatedMarketData createAggregatedMarketData(ISecurityIdentifier securityIdentifier, long timestamp, ITimeInterval timeInterval,
                                                         BigDecimal open, BigDecimal high, BigDecimal low, BigDecimal close, BigDecimal volume);
    }

    @FunctionalInterface
    public interface IAggregatedMarketDataVisitor {
        void visit(ISecurityIdentifier securityIdentifier, long timestamp, ITimeInterval timeInterval, BigDecimal open, BigDecimal high, BigDecimal low,
                   BigDecimal close, BigDecimal volume);
    }
}





