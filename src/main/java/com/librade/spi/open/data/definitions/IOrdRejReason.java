package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Order reject reason. Possible values:
 * <ul>
 *     <li>UNSPECIFIED</li>
 *     <li>BrokerOption</li>
 *     <li>UnknownSymbol</li>
 *     <li>ExchangeClosed</li>
 *     <li>OrderExceedsLimit</li>
 *     <li>TooLateToEnter</li>
 *     <li>UnknownOrder</li>
 *     <li>DuplicateOrder</li>
 *     <li>StaleOrder</li>
 *     <li>Other</li>
 * </ul>
 */
public interface IOrdRejReason extends IDefinition<IOrdRejReason> {

    String UNSPECIFIED = "UNSPECIFIED";
    String BrokerOption = "BrokerOption";
    String UnknownSymbol = "UnknownSymbol";
    String ExchangeClosed = "ExchangeClosed";
    String OrderExceedsLimit = "OrderExceedsLimit";
    String TooLateToEnter = "TooLateToEnter";
    String UnknownOrder = "UnknownOrder";
    String DuplicateOrder = "DuplicateOrder";
    String StaleOrder = "StaleOrder";
    String Other = "Other";

    int getFIXValue();
}





