package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IAccountPropertyType;

import java.math.BigDecimal;

public interface IAccountProperty {

    IAccountProperty stripTrailingZeros();

    IAccountPropertyType getAccountPropertyType();

    BigDecimal getValue();

    String getStringValue();
}
