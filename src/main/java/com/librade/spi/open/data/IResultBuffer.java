package com.librade.spi.open.data;

import java.util.List;

/**
 * Item buffer.
 */
public interface IResultBuffer<T> {

    /**
     * Method returns one item or null.
     * @return item from buffer
     */
    T poll();

    /**
     * Method returns one item or null, it waits at least the interval for next item (blocking method).
     * If there is an item in the buffer, it's returned instantly, if not, method is blocking for max. pTimeoutInMillis.
     * @param pTimeoutInMillis timeout to wait for the data arrival
     * @return item from buffer
     */
    T poll(long pTimeoutInMillis);

    /**
     * Method returns all the items from the buffer, or empty list.
     * @return items from buffer
     */
    List<T> pollAll();

    /**
     * Method returns all the items from the buffer, or empty list. Blocking method, see poll(long pTimeoutInMillis)
     * @param pTimeoutInMillis timeout to wait for the data arrival
     * @return  items from buffer
     */
    List<T> pollAll(long pTimeoutInMillis);

    /**
     *
     * @return remaining capacity of the buffer
     */
    int getRemainingCapacity();

    /**
     *
     * @return number if items in the buffer
     */
    int getBufferSize();

    /**
     *
     * @return true, if there is more data coming to buffer
     */
    boolean hasNext();

    /**
     *
     * @return if true, result buffer is in error state, which means, that there was an error during buffer filling
     */
    boolean isInErrorState();

    /**
     *
     * @return if buffer is in error state, this returns the text associated with error, otherwise it returns null
     */
    String getErrorMessage();
}





