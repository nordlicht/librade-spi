package com.librade.spi.open.data.securityIdentifier;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.StringJoiner;

/**
 *
 * @author Stanislav Fujdiar
 */
public class Price {

    private final BigDecimal min;
    private final BigDecimal max;

    public Price(BigDecimal min, BigDecimal max) {
        this.min = min;
        this.max = max;
    }

    public Price stripTrailingZeros() {
        return new Price(
                min != null ? min.stripTrailingZeros() : null,
                max != null ? max.stripTrailingZeros() : null
        );
    }

    public BigDecimal getMin() {
        return min;
    }

    public BigDecimal getMax() {
        return max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Price)) return false;
        Price price = (Price) o;
        return Objects.equals(min, price.min) && Objects.equals(max, price.max);
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Price.class.getSimpleName() + "[", "]")
                .add("min=" + min)
                .add("max=" + max)
                .toString();
    }
}
