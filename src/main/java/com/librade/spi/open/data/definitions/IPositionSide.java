package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Position side. Possible values (case sensitive):<br />
 * <ul>
 *     <li>LONG</li>
 *     <li>SHORT</li>
 * </ul>
 */
public interface IPositionSide extends IDefinition<IPositionSide> {

        String LONG = "LONG";
        String SHORT = "SHORT";
}
