package com.librade.spi.open.data.tradingSignal;

import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.nordlicht.ITimeSeriesData;

import java.math.BigDecimal;
import java.time.Instant;

public interface ISignal extends ITimeSeriesData {

    ISignal copy();

    ISignal stripTrailingZeros();

    Instant getTradingSignalCreatedAt();

    BigDecimal getSignalPrice();

    String getSignalSymbol();

    ITradingSignal getTradingSignal();

    ITradingSignalAction getTradingSignalAction();

    Instant getTradingSignalTimestamp();

    ISecurityIdentifier getSecurityEnum();
}
