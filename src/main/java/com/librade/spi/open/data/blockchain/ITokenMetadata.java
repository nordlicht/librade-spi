package com.librade.spi.open.data.blockchain;

import com.nordlicht.commons.base.serialization.byteable.IJsonByteable;

public interface ITokenMetadata extends IJsonByteable {

    String getHexChainId();

    String getTokenAddress();

    String getSymbol();

    String getName();

    int getDecimals();

    boolean isVerifiedContract();

    boolean isPossibleSpam();
}
