package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.ICustomEventType;
import com.nordlicht.ITimeSeriesData;

/**
 * Custom event.
 */
public interface ICustomEvent extends ITimeSeriesData {

    /**
     *
     * @return
     */
    String getSessionUID();

    /**
     *
     * @return
     */
    String getFrom();

    /**
     *
     * @return
     */
    String getTo();

    /**
     *
     * @return
     */
    String getId();

    /**
     *
     * @return
     */
    String getEventType();

    /**
     *
     * @return
     */
    ICustomEventType getEventTypeEnum();

    /**
     *
     * @return
     */
    int getEventTypeId();

    /**
     *
     * @return
     */
    String getContent();
}






