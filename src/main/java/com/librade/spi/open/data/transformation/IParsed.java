package com.librade.spi.open.data.transformation;

import java.util.Date;
import java.util.List;

/**
 * @author Tomas Fecko
 */
public interface IParsed {

    /**
     *
     * @return source text
     */
    String getSource();

    /**
     *
     * @param pKey key
     * @return value under key as string
     */
    String getString(String pKey);

    /**
     *
     * @param pKey key
     * @param defaultValue default value
     * @return value under key as string
     */
    String getString(String pKey, String defaultValue);

    /**
     *
     * @param pKey key
     * @return value under key as date
     */
    Date getDate(String pKey);

    /**
     *
     * @param pKey key
     * @param defaultValue default value
     * @return value under key as date
     */
    Date getDate(String pKey, Date defaultValue);

    /**
     *
     * @param pKey key
     * @return value under key as boolean
     */
    Boolean getBoolean(String pKey);

    /**
     *
     * @param pKey key
     * @param defaultValue default value
     * @return value under key as boolean
     */
    Boolean getBoolean(String pKey, Boolean defaultValue);

    /**
     *
     * @param pKey key
     * @param defaultValue default value
     * @return value under key as long
     */
    Long getLong(String pKey);

    /**
     *
     * @param pKey key
     * @param defaultValue default value
     * @return value under key as long
     */
    Long getLong(String pKey, Long defaultValue);

    /**
     *
     * @param pKey key
     * @return value under key as doubre
     */
    Double getDouble(String pKey);

    /**
     *
     * @param pKey key
     * @param defaultValue default value
     * @return value under key as doubre
     */
    Double getDouble(String pKey, Double defaultValue);

    /**
     *
     * @param pKey key
     * @return value under key as doubre
     */
    double[] getDoubleArray(String pKey);

    /**
     *
     * @param pKey key
     * @param pClass expected type of items in list
     * @param <T> type of items
     * @return value under key as list
     */
    <T> List<T> getList(String pKey, Class<T> pClass);

    /**
     *
     * @param pKey key
     * @param pClass expected type of items in list
     * @param <T> type of items
     * @param defaultValue default value
     * @return value under key as list
     */
    <T> List<T> getList(String pKey, Class<T> pClass, List<T> defaultValue);

    /**
     *
     * @param pKey key
     * @return value under key as new IParsed object
     */
    IParsed getTable(String pKey);

    /**
     *
     * @param pKey key
     * @return value uder key ale list of new IParsed objects
     */
    List<IParsed> getTables(String pKey);
}









