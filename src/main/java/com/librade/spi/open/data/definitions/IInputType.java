package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Possible values (case insensitive):<br />
 * <ul>
 *     <li>TEXT</li> (plain text)
 *     <li>TOML</li> (<a href="https://github.com/toml-lang/toml">TOML</a>)
 * </ul>
 */
public interface IInputType extends IDefinition<IInputType> {

    /**
     * Text input type.
     */
    String TEXT = "TEXT";
    /**
     * TOML input type format.
     */
    String TOML = "TOML";
}




