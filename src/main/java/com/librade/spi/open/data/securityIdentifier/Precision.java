package com.librade.spi.open.data.securityIdentifier;

/**
 *
 * @author Stanislav Fujdiar
 */
public class Precision {

    private final Integer amount;
    private final Integer price;

    public Precision(Integer amount, Integer price) {
        this.amount = amount;
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Precision{" +
                "amount=" + amount +
                ", price=" + price +
                '}';
    }
}
