package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ISide;

/**
 * Clinet to broker message. (e.g. NewOrderSingle)
 */
public interface IClientToBrokerMessage extends IBrokerMessage {

    /**
     *
     * @return
     */
    String getClOrdId();

    /**
     *
     * @return
     */
    int getSecurityId();

    /**
     *
     * @return
     */
    String getSecurity();

    /**
     *
     * @return
     */
    ISecurityIdentifier getSecurityEnum();

    /**
     *
     * @return
     */
    ISide getSideEnum();

    /**
     *
     * @return
     */
    String getSide();

    /**
     *
     * @return
     */
    int getSideId();

    Integer getBrokerAccountId();
}





