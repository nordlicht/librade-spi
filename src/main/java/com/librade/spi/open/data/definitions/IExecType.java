package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Execution type. Possible values:
 * <ul>
 *     <li>UNSPECIFIED</li>
 *     <li>New</li>
 *     <li>PartialFill</li>
 *     <li>Fill</li>
 *     <li>DoneForDay</li>
 *     <li>Canceled</li>
 *     <li>Replaced</li>
 *     <li>PendingCancel</li>
 *     <li>Stopped</li>
 *     <li>Rejected</li>
 *     <li>Suspended</li>
 *     <li>PendingNew</li>
 *     <li>Calculated</li>
 *     <li>Expired</li>
 *     <li>Restated</li>
 *     <li>PendingReplace</li>
 *     <li>Trade</li>
 *     <li>TradeCorrect</li>
 *     <li>TradeCancel</li>
 *     <li>OrderStatus</li>
 *     <li>TradeInAClearingHold</li>
 *     <li>TradeHasBeenReleasedToClearing</li>
 *     <li>TriggeredOrActivatedBySystem</li>
 * </ul>
 */
public interface IExecType extends IDefinition<IExecType> {

    String UNSPECIFIED = "UNSPECIFIED";
    String New = "New";
    String PartialFill = "PartialFill";
    String Fill = "Fill";
    String DoneForDay = "DoneForDay";
    String Canceled = "Canceled";
    String Replaced = "Replaced";
    String PendingCancel = "PendingCancel";
    String Stopped = "Stopped";
    String Rejected = "Rejected";
    String Suspended = "Suspended";
    String PendingNew = "PendingNew";
    String Calculated = "Calculated";
    String Expired = "Expired";
    String Restated = "Restated";
    String PendingReplace = "PendingReplace";
    String Trade = "Trade";
    String TradeCorrect = "TradeCorrect";
    String TradeCancel = "TradeCancel";
    String OrderStatus = "OrderStatus";
    String TradeInAClearingHold = "TradeInAClearingHold";
    String TradeHasBeenReleasedToClearing = "TradeHasBeenReleasedToClearing";
    String TriggeredOrActivatedBySystem = "TriggeredOrActivatedBySystem";

    char getFIXValue();
}




