package com.librade.spi.open.data;

import java.util.List;

/**
 *
 */
public interface IMarketDataBook extends IMarketDataParent {

    boolean isSnapshot();

    long getSequenceNumber();

    List<? extends IPriceTick> getAsks();

    List<? extends IPriceTick> getBids();

    /**
     *
     * @return data UID
     */
    String getDataUID();

    /**
     *
     * @return
     */
    IMarketDataBook copy();
}






