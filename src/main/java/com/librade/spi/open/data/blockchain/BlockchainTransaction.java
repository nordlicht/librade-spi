package com.librade.spi.open.data.blockchain;

import com.nordlicht.commons.base.serialization.byteable.IJsonByteable;

import java.math.BigDecimal;

public class BlockchainTransaction implements IJsonByteable {

    private String     hash;
    private String     chainId;
    private String     walletAddress;
    private String     symbol;
    private String     commissionCurrency;
    private boolean    confirmed;
    private boolean    approveOnly;
    private int        receiptStatus;
    private long       blockTimestamp;
    private BigDecimal buyAmount;
    private BigDecimal sellAmount;
    private BigDecimal buyTokenUsdPrice;
    private BigDecimal commissionFee;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    public String getWalletAddress() {
        return walletAddress;
    }

    public void setWalletAddress(String walletAddress) {
        this.walletAddress = walletAddress;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCommissionCurrency() {
        return commissionCurrency;
    }

    public void setCommissionCurrency(String commissionCurrency) {
        this.commissionCurrency = commissionCurrency;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public boolean isApproveOnly() {
        return approveOnly;
    }

    public void setApproveOnly(boolean approveOnly) {
        this.approveOnly = approveOnly;
    }

    public int getReceiptStatus() {
        return receiptStatus;
    }

    public void setReceiptStatus(int receiptStatus) {
        this.receiptStatus = receiptStatus;
    }

    public long getBlockTimestamp() {
        return blockTimestamp;
    }

    public void setBlockTimestamp(long blockTimestamp) {
        this.blockTimestamp = blockTimestamp;
    }

    public BigDecimal getBuyAmount() {
        return buyAmount;
    }

    public void setBuyAmount(BigDecimal buyAmount) {
        this.buyAmount = buyAmount;
    }

    public BigDecimal getSellAmount() {
        return sellAmount;
    }

    public void setSellAmount(BigDecimal sellAmount) {
        this.sellAmount = sellAmount;
    }

    public BigDecimal getBuyTokenUsdPrice() {
        return buyTokenUsdPrice;
    }

    public void setBuyTokenUsdPrice(BigDecimal buyTokenUsdPrice) {
        this.buyTokenUsdPrice = buyTokenUsdPrice;
    }

    public BigDecimal getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(BigDecimal commissionFee) {
        this.commissionFee = commissionFee;
    }

}
