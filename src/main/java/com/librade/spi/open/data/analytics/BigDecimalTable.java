package com.librade.spi.open.data.analytics;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Stano Fujdiar
 */
public class BigDecimalTable extends Table<BigDecimal> {

    private ArrayList<BigDecimal>    mValues;

    public BigDecimalTable() {
    }

    /**
     *
     * @param pRows rows
     * @param pCols columns
     */
    public BigDecimalTable(int pRows, int pCols) {
        super(pRows, pCols);
    }

    @Override
    public ArrayList<BigDecimal> getValues() {
        return mValues;
    }

    @Override
    public void setValues(ArrayList<BigDecimal> pValues) {
        mValues = pValues;
    }

    @Override
    public int getByteableClassId() {
        return 3203;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        if (!super.equals(pO)) {
            return false;
        }
        final BigDecimalTable that = (BigDecimalTable) pO;
        return Objects.equals(mValues, that.mValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), mValues);
    }
}





