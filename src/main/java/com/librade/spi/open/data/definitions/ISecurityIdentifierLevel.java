package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Level of aggregation. <br />
 * <ul>
 *     <li>LEVEL_1 - Aggregated currency cross all brokers/exchanges and all pairs. (e.g. USD)</li>
 *     <li>LEVEL_2 - Aggregated pair cross all brokers/exchanges that's traded. e.g. EUR/USD on ALL_BROKERS</li>
 *     <li>LEVEL_3 - Tradeable instrument at specific broker and exchange. e.g. EUR/USD at LMAX</li>
 * </ul>
 * @author Tomas Fecko
 */
public interface ISecurityIdentifierLevel extends IDefinition<ISecurityIdentifierLevel> {

    /**
     * Aggregated currency cross all brokers/exchanges and all pairs. (e.g. USD)
     */
    String LEVEL_1_NAME = "LEVEL_1";
    /**
     * Aggregated pair cross all brokers/exchanges that's traded. e.g. EUR/USD on ALL_BROKERS
     */
    String LEVEL_2_NAME = "LEVEL_2";
    /**
     * Tradeable instrument at specific broker and exchange. e.g. EUR/USD at LMAX
     */
    String LEVEL_3_NAME = "LEVEL_3";

}







