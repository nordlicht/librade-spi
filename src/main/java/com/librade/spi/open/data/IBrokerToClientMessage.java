package com.librade.spi.open.data;

/**
 * @author Tomas Fecko
 */
public interface IBrokerToClientMessage extends IBrokerMessage {
}
