package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Time in force. Possible values:
 * <ul>
 *     <li>UNSPECIFIED</li>
 *     <li>DAY</li>
 *     <li>GTC</li>
 *     <li>OPG</li>
 *     <li>IOC</li>
 *     <li>FOK</li>
 *     <li>GTX</li>
 *     <li>GTD</li>
 *     <li>ATC</li>
 * </ul>
 * Day (or session) (DAY).<br />
 * Good Till Cancel (GTC).<br />
 * At the Opening (OPG).<br />
 * Immediate Or Cancel (IOC).<br />
 * Fill Or Kill (FOK).<br />
 * Good Till Crossing (GTX).<br />
 * Good Till Date (GTD).<br />
 * At the Close (ATC).<br />
 */
public interface ITimeInForce extends IDefinition<ITimeInForce> {

    String DAY = "DAY";
    String GTC = "GTC";
    String OPG = "OPG";
    String IOC = "IOC";
    String FOK = "FOK";
    String GTX = "GTX";
    String GTD = "GTD";
    String ATC = "ATC";

    char getFIXValue();
}





