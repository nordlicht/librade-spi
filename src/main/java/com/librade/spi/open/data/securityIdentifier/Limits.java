package com.librade.spi.open.data.securityIdentifier;

import java.util.Objects;
import java.util.StringJoiner;

/**
 *
 * @author Stanislav Fujdiar
 */
public class Limits {

    private final Amount amount;
    private final Price price;
    private final Cost cost;
    private final Withdraw withdraw;

    public Limits(Amount amount, Price price, Cost cost, Withdraw withdraw) {
        this.amount = amount;
        this.price = price;
        this.cost = cost;
        this.withdraw = withdraw;
    }

    public Limits stripTrailingZeros() {
        return new Limits(
                amount != null ? amount.stripTrailingZeros() : null,
                price != null ? price.stripTrailingZeros() : null,
                cost != null ? cost.stripTrailingZeros() : null,
                withdraw != null ? withdraw.stripTrailingZeros() : null
        );
    }

    public Amount getAmount() {
        return amount;
    }

    public Price getPrice() {
        return price;
    }

    public Cost getCost() {
        return cost;
    }

    public Withdraw getWithdraw() {
        return withdraw;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Limits)) return false;
        Limits limits = (Limits) o;
        return Objects.equals(amount, limits.amount)
                && Objects.equals(price, limits.price)
                && Objects.equals(cost, limits.cost)
                && Objects.equals(withdraw, limits.withdraw);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount, price, cost, withdraw);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Limits.class.getSimpleName() + "[", "]")
                .add("amount=" + amount)
                .add("price=" + price)
                .add("cost=" + cost)
                .add("withdraw=" + withdraw)
                .toString();
    }
}
