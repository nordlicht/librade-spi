package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

public interface IMarginMode extends IDefinition<IMarginMode> {

    String CROSS = "CROSS";
    String ISOLATED = "ISOLATED";
}
