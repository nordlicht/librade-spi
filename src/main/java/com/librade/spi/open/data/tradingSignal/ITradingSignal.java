package com.librade.spi.open.data.tradingSignal;

import com.nordlicht.IDefinition;

public interface ITradingSignal extends IDefinition<ITradingSignal> {
}
