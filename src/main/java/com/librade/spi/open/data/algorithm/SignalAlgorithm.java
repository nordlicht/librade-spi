package com.librade.spi.open.data.algorithm;

import com.librade.spi.open.IExecutable;
import com.librade.spi.open.ILibradeAPI;
import com.librade.spi.open.data.EventWrapper;
import com.librade.spi.open.data.IControlMessage;
import com.librade.spi.open.data.IExecutionReport;
import com.librade.spi.open.data.INewOrderSingle;
import com.librade.spi.open.data.IOrderCancelRequest;
import com.librade.spi.open.data.IPositionReport;
import com.librade.spi.open.data.definitions.IOrderStatus;
import com.librade.spi.open.data.definitions.IOrderType;
import com.librade.spi.open.data.definitions.IPositionSide;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ISide;
import com.librade.spi.open.data.tradingSignal.ISignal;
import com.librade.spi.open.data.tradingSignal.ITradingSignalAction;
import com.librade.spi.open.exceptions.APIException;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class SignalAlgorithm implements IExecutable {

    private final Map<Integer, IPositionReport>  accountPositions = new ConcurrentHashMap<>();
    private final Map<Integer, IExecutionReport> accountOrders    = new ConcurrentHashMap<>();
    private final Map<Integer, BigDecimal>       leverageLevels   = new ConcurrentHashMap<>();
    private String                               customAccountId;
    private Map<String, Integer> decimals;

    @Override
    public void execute(ILibradeAPI api, String inputParameters) throws APIException {
        api.log().info("SignalAlgorithm.execute, method started");

        // base events
        api.cep().createAndStartEPLQuery("ALL_ControlMessage", "SELECT * FROM ControlMessage");
        api.cep().createAndStartEPLQuery("ALL_Signal", "SELECT * FROM Signal");
        api.cep().createAndStartEPLQuery("ALL_PositionReport", "SELECT * FROM PositionReport");
        api.cep().createAndStartEPLQuery("ALL_ExecutionReport", "SELECT * FROM ExecutionReport");

        decimals = getDecimalsPlaces();
        customAccountId = getCustomAccountId(api, inputParameters);
        var positionSize = getPositionSize(api, inputParameters);
        if (customAccountId != null && positionSize != null) {
            api.account().subscribeToUserDataStream(customAccountId);

            var event = new EventWrapper(null);
            while (!api.session().isStopped()) {
                var cepEvent = api.event().poll(9);
                if (cepEvent == null) {
                    continue;
                }
                event.wrap(cepEvent);
                if (event.isNa()) {
                    continue;
                }
                if (event.isStop()) {
                    api.log().debug("TestAlgorithm.execute()->Stop event arrived, stopping algo...");
                    break;
                } else if (api.session().isStopped()) {
                    api.log().debug("TestAlgorithm.execute()->Session is stopped, stopping algo...");
                    break;
                } else if (event.isFail()) {
                    // if error arrived from platform
                    List<IControlMessage> controlMessages = event.getControlMessages();
                    String error = "TestAlgorithm.execute()->Failure detected, finishing. Error:" + controlMessages;
                    api.log().warn(error);
                    break;
                }
                event.stripTrailingZeros();

                if (event.containsPositionReports()) {
                    for (IPositionReport positionReport : event.getPositionReports()) {
                        var securityIdentifierId = positionReport.getSecurityId();
                        api.log().info("SignalAlgorithm, store position, " + getPositionMessage(positionReport));
                        accountPositions.put(securityIdentifierId, positionReport);
                        leverageLevels.put(securityIdentifierId, positionReport.getLeverage());
                    }
                }

                if (event.containsExecutionReports()) {
                    for (IExecutionReport executionReport : event.getExecutionReports()) {
                        accountOrders.put(executionReport.getSecurityId(), executionReport);
                    }
                }

                if (event.containsSignals()) {
                    for (ISignal signal : event.getSignals()) {
                        // CLOSE_SHORT -> BUY_LONG
                        if (ITradingSignalAction.CLOSE_SHORT.equals(signal.getTradingSignalAction().getName())) {
                            processSignal(api, signal, ISide.BUY, positionSize);
                        }
                        // CLOSE_LONG -> SHORT_SELL
                        if (ITradingSignalAction.CLOSE_LONG.equals(signal.getTradingSignalAction().getName())) {
                            processSignal(api, signal, ISide.SELL, positionSize);
                        }
                    }
                }
            }
        } else {
            api.log().warn("SignalAlgorithm.execute, method was not executed, customAccountId parameter does not exist");
        }
    }

    private String getCustomAccountId(ILibradeAPI api, String inputParameters) {
        try {
            if (inputParameters != null && !inputParameters.isEmpty()) {
                return api.dataTransform().parseInputParameters(inputParameters).getString("accountName");
            }
        } catch (APIException e) {
            api.log().error("SignalAlgorithm.getCustomAccountId, parse input parameters", e);
        }
        return null;
    }

    private BigDecimal getPositionSize(ILibradeAPI api, String inputParameters) {
        try {
            if (inputParameters != null && !inputParameters.isEmpty()) {
                return BigDecimal.valueOf(api.dataTransform().parseInputParameters(inputParameters)
                        .getLong("positionSizeAbsolute"));
            }
        } catch (APIException e) {
            api.log().error("SignalAlgorithm.getCustomAccountId, parse input parameters", e);
        }
        return null;
    }

    private void processSignal(
            ILibradeAPI api,
            ISignal signal,
            String orderSide,
            BigDecimal positionSize) {
        api.log().info("SignalAlgorithm.processSignal, process signal starting, " + getSignalMessage(signal));
        try {
            var securityIdentifier = signal.getSecurityEnum();
            var securityId = securityIdentifier.getId();
            var openPosition = getOpenPositionReport(securityId);
            var orderQuantity = calculateOrderQuantity(signal.getSignalPrice(), positionSize, signal.getSignalSymbol());
            if (openPosition == null) {
                var newOrder = createOrder(api, signal, orderSide, IOrderType.LIMIT, orderQuantity, signal.getSignalPrice(), "{}");
                var openOrder = getOpenOrder(securityId);
                if (openOrder == null) {
                    api.log().info("SignalAlgorithm.processSignal, position is not opened, send new order: " + getNewOrderSingleMessage(newOrder));
                    setLeverage(api, securityIdentifier);
                    api.orderExecution().sendOrder(newOrder);
                } else {
                    api.log().info("SignalAlgorithm.processSignal, open order, " + getExecutionReportMessage(openOrder));
                    var cancelOrder = api.orderExecution().createOrderBuilder()
                            .customAccountUID(customAccountId)
                            .origClOrdId(openOrder.getOrderId())
                            .securityIdentifier(openOrder.getSecurityEnum())
                            .buildOrderCancelRequest();
                    api.log().info("SignalAlgorithm.processSignal, cancel open order" + getOrderCancelMessage(cancelOrder));
                    api.orderExecution().sendOrder(cancelOrder);
                    sendNewOrderAfterCancelOpenOrder(api, securityId, newOrder);
                }
            } else {
                api.log().info("SignalAlgorithm.processSignal, open position, " + getPositionMessage(openPosition));
                var side = IPositionSide.SHORT.equals(openPosition.getSide().getName()) ? ISide.BUY : ISide.SELL;
                var order = createOrder(api, signal, side, IOrderType.MARKET, BigDecimal.ZERO, null, "{\"closeOnTrigger\":true,\"reduceOnly\":true}");
                api.log().info("SignalAlgorithm.processSignal, close open position by sending order, " + order);
                api.orderExecution().sendOrder(order);
                sendOrderAfterClosePosition(api, signal, orderSide, orderQuantity);
            }
        } catch (Exception ex) {
            api.log().error("SignalAlgorithm.processSignal, signal error", ex);
        }
    }

    private BigDecimal calculateOrderQuantity(BigDecimal price, BigDecimal positionSize, String signalSymbol) {
        var scale = decimals.get(signalSymbol);
        if (scale != null) {
            return positionSize.divide(price, scale, RoundingMode.HALF_UP);
        } else {
            var result = positionSize.divide(price, 2, RoundingMode.HALF_UP);
            return result.toBigInteger().compareTo(BigInteger.ZERO) == 0 ? result
                    : result.scale() > 0 ? result.setScale(0, RoundingMode.HALF_UP) : result;
        }
    }

    private INewOrderSingle createOrder(
            ILibradeAPI api,
            ISignal signal,
            String orderSide,
            String orderType,
            BigDecimal orderQuantity,
            BigDecimal price,
            String additionalParams) {
        var orderBuilder = api.orderExecution().createOrderBuilder()
                .securityIdentifier(signal.getSecurityEnum())
                .customAccountUID(customAccountId)
                .side(orderSide)
                .orderType(orderType)
                .quantity(orderQuantity)
                .setAdditionalParams(additionalParams);
        if (price != null) {
            orderBuilder.price(price);
        }
        return orderBuilder.buildNewOrderSingle();
    }

    private void setLeverage(ILibradeAPI api, ISecurityIdentifier securityIdentifier) {
        try {
            var securityIdentifierId = securityIdentifier.getId();
            var leverage = leverageLevels.get(securityIdentifierId);
            if (leverage == null) {
                var positionReport = api.account().getPositionReport(customAccountId, securityIdentifier, Map.of());
                leverage = positionReport.getLeverage();
            }

            if (leverage == null) {
                api.log().info("SignalAlgorithm.setLeverage, leverage is unknown for: " + securityIdentifier.getName());
            } else if (leverage.compareTo(BigDecimal.ONE) != 0) {
                api.account().setLeverage(customAccountId, 1D, securityIdentifier, Map.of());
                leverageLevels.put(securityIdentifierId, BigDecimal.ONE);
            }
        } catch (Exception ex) {
            api.log().error("SignalAlgorithm.setLeverage, setLeverage method failed", ex);
        }
    }

    private void sendOrderAfterClosePosition(
            ILibradeAPI api,
            ISignal signal,
            String orderSide,
            BigDecimal orderQuantity) {
        var counter = new AtomicInteger(0);
        var scheduledFutureRef = new ScheduledFuture<?>[1];
        Runnable task = () -> {
            int count = counter.incrementAndGet();
            if (isPositionReportClosed(signal.getSecurityEnum().getId())) {
                var order = createOrder(api, signal, orderSide, IOrderType.LIMIT, orderQuantity, signal.getSignalPrice(), "{}");
                api.log().info("SignalAlgorithm.sendOrderAfterClosePosition, send new order: " + getNewOrderSingleMessage(order));
                try {
                    setLeverage(api, signal.getSecurityEnum());
                    api.orderExecution().sendOrder(order);
                    scheduledFutureRef[0].cancel(false);
                } catch (Exception ex) {
                    api.log().error("SignalAlgorithm.sendOrderAfterClosePositionSend, order sending failed", ex);
                }
            }
            // wait to closing position 3minutes (6x30seconds)
            if (count >= 6) {
                api.log().info("SignalAlgorithm.sendOrderAfterClosePosition, cancellation of task execution, reportPosition for signal: "
                        + getSignalMessage(signal) + " was not closed until 3 minutes", signal);
                scheduledFutureRef[0].cancel(false);
            }
        };
        scheduledFutureRef[0] = api.async().scheduleAtFixedRate(task, 30, 30, TimeUnit.SECONDS);
    }

    private void sendNewOrderAfterCancelOpenOrder(
            ILibradeAPI api,
            Integer securityIdentifierId,
            INewOrderSingle newOrder) {
        var counter = new AtomicInteger(0);
        var scheduledFutureRef = new ScheduledFuture<?>[1];
        Runnable task = () -> {
            int count = counter.incrementAndGet();
            if (isOrderCanceled(securityIdentifierId)) {
                api.log().info("SignalAlgorithm.sendNewOrderAfterCancelOpenOrder, open order is cancelled, send new order: " + getNewOrderSingleMessage(newOrder));
                try {
                    setLeverage(api, newOrder.getSecurityEnum());
                    api.orderExecution().sendOrder(newOrder);
                    scheduledFutureRef[0].cancel(false);
                } catch (Exception ex) {
                    api.log().error("SignalAlgorithm.sendNewOrderAfterCancelOpenOrder, new order sending failed", ex);
                }
            }
            // waiting to cancel open order 3minutes (6x30seconds)
            if (count >= 6) {
                api.log().warn("SignalAlgorithm.sendNewOrderAfterCancelOpenOrder, cancellation of task execution, open order for securityIdentifier: "
                        + newOrder.getSecurityEnum().getName() + " was not cancelled until 3 minutes");
                scheduledFutureRef[0].cancel(false);
            }
        };
        scheduledFutureRef[0] = api.async().scheduleAtFixedRate(task, 30, 30, TimeUnit.SECONDS);
    }

    private IPositionReport getOpenPositionReport(Integer securityIdentifierId) {
        var positionReport = accountPositions.get(securityIdentifierId);
        return positionReport != null
                && positionReport.getContracts().compareTo(BigDecimal.ZERO) > 0
                && positionReport.getSide().getId() != IPositionSide.UNSPECIFIED_ID
                ? positionReport
                : null;
    }

    private boolean isPositionReportClosed(Integer securityIdentifierId) {
        var positionReport = accountPositions.get(securityIdentifierId);
        return positionReport != null && positionReport.getContracts().compareTo(BigDecimal.ZERO) == 0 && positionReport.getSide().getId() == IPositionSide.UNSPECIFIED_ID;
    }

    private IExecutionReport getOpenOrder(Integer securityIdentifierId) {
        var openOrder = accountOrders.get(securityIdentifierId);
        return openOrder != null && IOrderStatus.New.equals(openOrder.getOrdStatusEnum().getName())
                ? openOrder
                : null;
    }

    private boolean isOrderCanceled(Integer securityIdentifierId) {
        var order = accountOrders.get(securityIdentifierId);
        return order != null && IOrderStatus.Cancelled.equals(order.getOrdStatusEnum().getName());
    }

    private String getPositionMessage(IPositionReport positionReport) {
        return "PositionReport"
                + ", identifier: " + positionReport.getSecurityEnum().getName()
                + ", contracts: " + positionReport.getContracts()
                + ", side: " + positionReport.getSide().getName()
                + ", leverage: " + positionReport.getLeverage();
    }

    private String getSignalMessage(ISignal signal) {
        return "Signal"
                + ", identifier: " + signal.getSecurityEnum().getName()
                + ", signalAction: " + signal.getTradingSignalAction().getName()
                + ", price: " + signal.getSignalPrice()
                + ", signalCreatedAt: " + signal.getTradingSignalCreatedAt()
                + ", signal: " + signal.getTradingSignal().getName();
    }

    private String getNewOrderSingleMessage(INewOrderSingle newOrderSingle) {
        return "NewOrderSingle"
                + ", identifier: " + newOrderSingle.getSecurityEnum().getName()
                + ", side: " + newOrderSingle.getSide()
                + ", orderType: " + newOrderSingle.getOrderType()
                + ", quantity: " + newOrderSingle.getQuantity()
                + ", price: " + newOrderSingle.getPrice()
                + ", note: " + newOrderSingle.getNote();
    }

    private String getExecutionReportMessage(IExecutionReport executionReport) {
        return "ExecutionReport"
                + ", identifier: " + executionReport.getSecurityEnum().getName()
                + ", orderStatus: " + executionReport.getOrdStatus()
                + ", orderId: " + executionReport.getOrderId()
                + ", price: " + executionReport.getPrice()
                + ", side: " + executionReport.getSide();
    }

    private String getOrderCancelMessage(IOrderCancelRequest order) {
        return "OrderCancelRequest"
                + ", identifier: " + order.getSecurityEnum().getName()
                + ", side: " + order.getSide()
                + ", orderId: " + order.getOrigClOrdId();
    }

    private Map<String, Integer> getDecimalsPlaces() {
        var decimalPlaces = new HashMap<String, Integer>();
        decimalPlaces.put("BTC", 3);
        decimalPlaces.put("ADA", 0);
        decimalPlaces.put("DOT", 1);
        decimalPlaces.put("LINK", 1);
        decimalPlaces.put("AVAX", 1);
        decimalPlaces.put("ETH", 2);
        decimalPlaces.put("DOGE", 0);
        decimalPlaces.put("LTC", 1);
        decimalPlaces.put("TRX", 0);
        decimalPlaces.put("BNB", 2);
        decimalPlaces.put("SHIB", 0);
        decimalPlaces.put("SOL", 1);
        decimalPlaces.put("XRP", 0);
        decimalPlaces.put("ICP", 1);
        return decimalPlaces;
    }

}
