package com.librade.spi.open.data.patterns;

import com.nordlicht.IDefinition;

public interface IPatternDirection extends IDefinition<IPatternDirection> {

    String VAL_UNSPECIFIED = "UNSPECIFIED";
    String VAL_BULLISH     = "BULLISH";
    String VAL_BEARISH     = "BEARISH";

    String getCode();

}
