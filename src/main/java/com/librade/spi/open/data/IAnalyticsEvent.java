package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IAnalyticsType;
import com.librade.spi.open.data.definitions.ITimeInterval;
import com.nordlicht.ITimeSeriesData;

import java.math.MathContext;
import java.math.RoundingMode;

/**
 * @author Stanislav Fujdiar
 */
public interface IAnalyticsEvent<IdT, ValueT> extends ITimeSeriesData {

    int          DEFAULT_SCALE           = 18;
    int          DEFAULT_PRECISION       = 18;
    RoundingMode DEFAULT_ROUNDING_MODE   = RoundingMode.HALF_UP;
    MathContext  DEFAULT_MATH_CONTEXT    = new MathContext(DEFAULT_PRECISION, DEFAULT_ROUNDING_MODE);

    /**
     *
     * @return strip trailing zeros from all BigDecimals
     */
    IAnalyticsEvent stripTrailingZeros();

    /**
     *
     * @return value is round to default scale of 18
     */
    IAnalyticsEvent roundToDefaultScale();

    /**
     *
     * @return UID of event session
     */
    IdT getIdentifierEnum();

    /**
     *
     * @return UID of event session
     */
    String getIdentifier();

    /**
     *
     * @return UID of event session
     */
    int getIdentifierId();

    /**
     *
     * @return analytics value
     */
    ValueT getValue();

    /**
     * e.g. type of analytics LINEAR_REG.
     * @return
     */
    IAnalyticsType getTypeEnum();

    /**
     * e.g. type of analytics LINEAR_REG.
     * @return
     */
    String getType();

    /**
     *
     * @return type id
     */
    int getTypeId();

    /**
     * Creates a deep copy of object.
     * @return copy
     */
    IAnalyticsEvent copy();

    /**
     *
     * @return time interval
     */
    ITimeInterval getTimeInterval();

    long getTimeIntervalTimespan();

    boolean isRealTimeEvent();

    default boolean isValid() {
        if (getTypeEnum() == null) {
            return false;
        }
        return getTypeEnum().isValueValid(this);
    }
}







