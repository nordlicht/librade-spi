package com.librade.spi.open.data.analytics;

/**
 * @author Stano Fujdiar
 */
public abstract class Scalar<T> extends Matrix<T> {

    /**
     *
     */
    public Scalar() {
        super(1);
    }

}
