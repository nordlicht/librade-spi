package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IMsgType;
import com.nordlicht.ITimeSeriesData;

/**
 * @author Tomas Fecko
 */
public interface IBrokerMessage extends ITimeSeriesData {

    /**
     *
     * @return
     */
    int getMsgSeqNum();

    /**
     *
     * @return
     */
    IMsgType getMsgTypeEnum();

    /**
     *
     * @return
     */
    String getMsgType();

    /**
     *
     * @return
     */
    int getMsgTypeId();

    /**
     *
     * @return
     */
    long getSendingTime();

    /**
     *
     * @return
     */
    String getSessionId();

    /**
     *
     * @return
     */
    String getCustomAccountID();
}






