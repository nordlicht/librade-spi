package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Message type.
 */
public interface IMsgType extends IDefinition<IMsgType> {

    String UNSPECIFIED = "UNSPECIFIED";
    String Heartbeat = "Heartbeat";
    String TestRequest = "TestRequest";
    String ResendRequest = "ResendRequest";
    String Reject = "Reject";
    String SequenceReset = "SequenceReset";
    String Logout = "Logout";
    String IndicationOfInterest = "IndicationOfInterest";
    String Advertisement = "Advertisement";
    String ExecutionReport = "ExecutionReport";
    String OrderCancelReject = "OrderCancelReject";
    String QuoteStatusRequest = "QuoteStatusRequest";
    String Logon = "Logon";
    String DerivativeSecurityList = "DerivativeSecurityList";
    String NewOrderMultileg = "NewOrderMultileg";
    String MultilegOrderCancelReplace = "MultilegOrderCancelReplace";
    String TradeCaptureReportRequest = "TradeCaptureReportRequest";
    String TradeCaptureReport = "TradeCaptureReport";
    String OrderMassStatusRequest = "OrderMassStatusRequest";
    String QuoteRequestReject = "QuoteRequestReject";
    String RFQRequest = "RFQRequest";
    String QuoteStatusReport = "QuoteStatusReport";
    String QuoteResponse = "QuoteResponse";
    String Confirmation = "Confirmation";
    String PositionMaintenanceRequest = "PositionMaintenanceRequest";
    String PositionMaintenanceReport = "PositionMaintenanceReport";
    String RequestForPositions = "RequestForPositions";
    String RequestForPositionsAck = "RequestForPositionsAck";
    String PositionReport = "PositionReport";
    String TradeCaptureReportRequestAck = "TradeCaptureReportRequestAck";
    String TradeCaptureReportAck = "TradeCaptureReportAck";
    String AllocationReport = "AllocationReport";
    String AllocationReportAck = "AllocationReportAck";
    String ConfirmationAck = "ConfirmationAck";
    String SettlementInstructionRequest = "SettlementInstructionRequest";
    String AssignmentReport = "AssignmentReport";
    String CollateralRequest = "CollateralRequest";
    String CollateralAssignment = "CollateralAssignment";
    String CollateralResponse = "CollateralResponse";
    String News = "News";
    String MassQuoteAcknowledgement = "MassQuoteAcknowledgement";
    String CollateralReport = "CollateralReport";
    String CollateralInquiry = "CollateralInquiry";
    String NetworkCounterpartySystemStatusRequest = "NetworkCounterpartySystemStatusRequest";
    String NetworkCounterpartySystemStatusResponse = "NetworkCounterpartySystemStatusResponse";
    String UserRequest = "UserRequest";
    String UserResponse = "UserResponse";
    String CollateralInquiryAck = "CollateralInquiryAck";
    String ConfirmationRequest = "ConfirmationRequest";
    String Email = "Email";
    String SecurityDefinitionRequest = "SecurityDefinitionRequest";
    String SecurityDefinition = "SecurityDefinition";
    String NewOrderSingle = "NewOrderSingle";
    String SecurityStatusRequest = "SecurityStatusRequest";
    String NewOrderList = "NewOrderList";
    String OrderCancelRequest = "OrderCancelRequest";
    String SecurityStatus = "SecurityStatus";
    String OrderCancelReplaceRequest = "OrderCancelReplaceRequest";
    String TradingSessionStatusRequest = "TradingSessionStatusRequest";
    String OrderStatusRequest = "OrderStatusRequest";
    String TradingSessionStatus = "TradingSessionStatus";
    String MassQuote = "MassQuote";
    String BusinessMessageReject = "BusinessMessageReject";
    String AllocationInstruction = "AllocationInstruction";
    String BidRequest = "BidRequest";
    String ListCancelRequest = "ListCancelRequest";
    String BidResponse = "BidResponse";
    String ListExecute = "ListExecute";
    String ListStrikePrice = "ListStrikePrice";
    String ListStatusRequest = "ListStatusRequest";
    String XMLmessage = "XMLmessage";
    String ListStatus = "ListStatus";
    String RegistrationInstructions = "RegistrationInstructions";
    String RegistrationInstructionsResponse = "RegistrationInstructionsResponse";
    String AllocationInstructionAck = "AllocationInstructionAck";
    String OrderMassCancelRequest = "OrderMassCancelRequest";
    String DontKnowTrade = "DontKnowTrade";
    String QuoteRequest = "QuoteRequest";
    String OrderMassCancelReport = "OrderMassCancelReport";
    String Quote = "Quote";
    String NewOrderCross = "NewOrderCross";
    String SettlementInstructions = "SettlementInstructions";
    String CrossOrderCancelReplaceRequest = "CrossOrderCancelReplaceRequest";
    String CrossOrderCancelRequest = "CrossOrderCancelRequest";
    String MarketDataRequest = "MarketDataRequest";
    String SecurityTypeRequest = "SecurityTypeRequest";
    String SecurityTypes = "SecurityTypes";
    String MarketDataSnapshotFullRefresh = "MarketDataSnapshotFullRefresh";
    String SecurityListRequest = "SecurityListRequest";
    String MarketDataIncrementalRefresh = "MarketDataIncrementalRefresh";
    String MarketDataRequestReject = "MarketDataRequestReject";
    String SecurityList = "SecurityList";
    String QuoteCancel = "QuoteCancel";
    String DerivativeSecurityListRequest = "DerivativeSecurityListRequest";
    String ContraryIntentionReport = "ContraryIntentionReport";
    String SecurityDefinitionUpdateReport = "SecurityDefinitionUpdateReport";
    String SecurityListUpdateReport = "SecurityListUpdateReport";
    String AdjustedPositionReport = "AdjustedPositionReport";
    String AllocationInstructionAlert = "AllocationInstructionAlert";
    String ExecutionAcknowledgement = "ExecutionAcknowledgement";
    String TradingSessionList = "TradingSessionList";
    String TradingSessionListRequest = "TradingSessionListRequest";

    String getFIXValue();
}





