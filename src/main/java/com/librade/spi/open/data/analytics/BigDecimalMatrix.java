package com.librade.spi.open.data.analytics;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * @param <T> type of value
 * @author Stano Fujdiar
 */
public class BigDecimalMatrix extends Matrix<BigDecimal> {

    private ArrayList<BigDecimal>    mValues;

    public BigDecimalMatrix() {
    }

    public BigDecimalMatrix(int... pSize) {
        super(pSize);
    }

    public BigDecimalMatrix(BigDecimal[] pValues, int... pSize) {
        super(pValues, pSize);
    }

    public BigDecimalMatrix(Collection<BigDecimal> pValues, int... pSize) {
        super(pValues, pSize);
    }

    @Override
    public ArrayList<BigDecimal> getValues() {
        return mValues;
    }

    @Override
    public void setValues(ArrayList<BigDecimal> pValues) {
        mValues = pValues;
    }

    @Override
    public int getByteableClassId() {
        return 3201;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        if (!super.equals(pO)) {
            return false;
        }
        final BigDecimalMatrix that = (BigDecimalMatrix) pO;
        return Objects.equals(mValues, that.mValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), mValues);
    }
}








