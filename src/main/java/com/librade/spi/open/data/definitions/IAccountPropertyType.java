package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * @author Stanislav Fujdiar
 * Account property. Possible values:
 * <ul>
 *     <li>UNSPECIFIED</li>
 *     <li>NAV</li>
 *     <li>MARGIN</li>
 *     <li>UNREALIZED_PL</li>
 *     <li>AVAILABLE_FUNDS</li>
 *     <li>BALANCE</li>
 *     <li>REALIZED_PL</li>
 * </ul>
 */
public interface IAccountPropertyType extends IDefinition<IAccountPropertyType> {

    /**
     * Net asset value algorithm type.
     */
    String NAV = "NAV";
    /**
     * Account's total margin.
     */
    String MARGIN = "MARGIN";
    /**
     * Signed amount that is the account's unrealised profit (or loss).
     */
    String UNREALIZED_PL = "UNREALIZED_PL";
    /**
     * Account's available funds.
     */
    String AVAILABLE_FUNDS = "AVAILABLE_FUNDS";
    /**
     * Accounts current balance.
     */
    String BALANCE = "BALANCE";
    /**
     * Signed amount that is the account's unrealised profit (or loss).
     */
    String REALIZED_PL = "REALIZED_PL";

    String USED_FUNDS = "USED_FUNDS";
}
