package com.librade.spi.open.data.alert;

import com.librade.spi.open.data.definitions.IAnalyticsType;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ITimeInterval;

public interface IAlertData {

    ITimeInterval getTimeInterval();

    long getTimestamp();

    ISecurityIdentifier getSecurityIdentifier();

    default Double getAnalytics(IAnalyticsType analyticsType){
        return null;
    }

    default Long getTimeSpan() {
        return getTimeInterval() == null ? null : getTimeInterval().getTimespan();
    }

    IAlertOrigin getAlertOrigin();

}
