package com.librade.spi.open.data.analytics;

import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Stano Fujdiar
 */
public class LongTable extends Table<Long> {

    private ArrayList<Long>    mValues;

    public LongTable() {
    }

    /**
     *
     * @param pRows rows
     * @param pCols columns
     */
    public LongTable(int pRows, int pCols) {
        super(pRows, pCols);
    }

    @Override
    public ArrayList<Long> getValues() {
        return mValues;
    }

    @Override
    public void setValues(ArrayList<Long> pValues) {
        mValues = pValues;
    }

    @Override
    public int getByteableClassId() {
        return 3204;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        if (!super.equals(pO)) {
            return false;
        }
        final LongTable longs = (LongTable) pO;
        return Objects.equals(mValues, longs.mValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), mValues);
    }
}





