package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IOptionExerciseStyle;
import com.librade.spi.open.data.definitions.IOptionType;

import java.math.BigDecimal;

import static com.nordlicht.IDefinition.UNSPECIFIED;
import static com.nordlicht.IDefinition.UNSPECIFIED_ID;

/**
 *
 */
public interface IMarketDataBookOption extends IMarketDataBook {

    /**
     *
     * @return option type enum
     */
    IOptionType getOptionTypeEnum();

    /**
     *
     * @return option type id
     */
    int getOptionTypeId();

    /**
     *
     * @return option type string
     */
    String getOptionType();

    /**
     *
     * @return strike price
     */
    double getStrikePrice();

    /**
     *
     * @return strike price BD
     */
    BigDecimal getStrikePriceBD();

    /**
     *
     * @return expiration time
     */
    long getExpirationTime();

    /**
     *
     * @return option type enum
     */
    IOptionExerciseStyle getOptionExersizeStyleEnum();

    /**
     *
     * @return option type id
     */
    default int getOptionExersizeStyleId() {
        return getOptionExersizeStyleEnum() == null ? UNSPECIFIED_ID : getOptionExersizeStyleEnum().getId();
    }

    /**
     *
     * @return option type string
     */
    default String getOptionExersizeStyle() {
        return getOptionExersizeStyleEnum() == null ? UNSPECIFIED : getOptionExersizeStyleEnum().getName();
    }

    /**
     * Base amount of underlying spot asset which single option represents.
     * @return
     */
    BigDecimal getContractUnit();
}






