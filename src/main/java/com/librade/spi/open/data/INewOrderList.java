package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IContingencyType;

import java.util.List;

public interface INewOrderList extends IClientToBrokerMessage {

    /**
     *
     * @return Number of orders in this message (number of repeating groups to follow).
     */
    int getNoOrders();

    /**
     *
     * @return Total number of list order entries across all messages. Should be the sum of all NoOrders <73> in each
     * message that has repeating list order entries related to the same ListID <66>. Used to support fragmentation.
     */
    int getTotNoOrders();

    /**
     *
     * @return Unique identifier for list as assigned by institution, used to associate multiple individual orders.
     * Uniqueness must be guaranteed within a single trading day. Firms which generate multi-day orders should consider
     * embedding a date within the ListID field to assure uniqueness across days.
     */
    String getListID();

    /**
     *
     * @return orders in group
     */
    List<INewOrderSingle> getListOrdGrp();

    /**
     *
     * @return Defines the type of contingency.<br />
     * <ul>
     *     <li>One Cancels the Other (OCO)</li>
     *     <li>One Triggers the Other (OTO)</li>
     *     <li>One Updates the Other (OUO) - Absolute Quantity Reduction</li>
     *     <li>One Updates the Other (OUO) - Proportional Quantity Reduction</li>
     *</ul>
     */
    IContingencyType getContingencyType();
}





