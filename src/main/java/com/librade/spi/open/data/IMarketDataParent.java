package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IAsset;
import com.librade.spi.open.data.definitions.IBroker;
import com.librade.spi.open.data.definitions.IExchange;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ISecurityIdentifierLevel;
import com.nordlicht.ITimeSeriesData;

/**
 * @author Tomas Fecko
 */
public interface IMarketDataParent extends ITimeSeriesData {

    String OPTION_AMERICAN_NAME_PREFIX = "O_A_";
    String OPTION_EUROPEAN_NAME_PREFIX = "O_E_";
    String FUTURES_NAME_PREFIX = "F_";
    String CREDIT_NAME_PREFIX = "C_";
    String INDEX_NAME_PREFIX = "I_";
    String PERPETUAL_NAME_PREFIX = "P_";
    String SPOT_NAME_PREFIX = "";

    /**
     *
     * @return
     */
    String getBroker();

    /**
     *
     * @return
     */
    int getBrokerId();

    /**
     *
     * @return
     */
    IBroker getBrokerEnum();

    /**
     *
     * @return
     */
    String getExchange();

    /**
     *
     * @return
     */
    int getExchangeId();

    /**
     *
     * @return
     */
    IExchange getExchangeEnum();

    /**
     *
     * @return
     */
    String getSymbol();

    /**
     *
     * @return
     */
    int getSymbolId();

    /**
     *
     * @return
     */
    IAsset getSymbolEnum();

    /**
     *
     * @return
     */
    String getCurrency();

    /**
     *
     * @return
     */
    int getCurrencyId();

    /**
     *
     * @return
     */
    IAsset getCurrencyEnum();

    /**
     *
     * @return
     */
    String getSecurity();

    /**
     *
     * @return
     */
    int getSecurityId();
    /**
     *
     * @return
     */
    ISecurityIdentifier getSecurityEnum();

    /**
     *
     * @return
     */
    String getSecurityLevel();

    /**
     *
     * @return
     */
    int getSecurityLevelId();

    /**
     *
     * @return
     */
    ISecurityIdentifierLevel getSecurityLevelEnum();

    /**
     * Strips trailing zeroes from every BigDecimal.
     */
    IMarketDataParent stripTrailingZeros();
}





