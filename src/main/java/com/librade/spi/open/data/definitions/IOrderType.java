package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Order type. Possible values (case sensitive):<br />
 * <ul>
 * <li>UNSPECIFIED</li>
 * <li>MARKET</li>
 * <li>LIMIT</li>
 * <li>STOP</li>
 * <li>STOP_LIMIT</li>
 * <li>MARKET_ON_CLOSE</li>
 * <li>WITH_OR_WITHOUT</li>
 * <li>LIMIT_OR_BETTER</li>
 * <li>LIMIT_WITH_OR_WITHOUT</li>
 * <li>ON_BASIS</li>
 * <li>ON_CLOSE</li>
 * <li>LIMIT_ON_CLOSE</li>
 * <li>FOREX_MARKET</li>
 * <li>PREVIOUSLY_QUOTED</li>
 * <li>PREVIOUSLY_INDICATED</li>
 * <li>FOREX_LIMIT</li>
 * <li>FOREX_SWAP</li>
 * <li>FOREX_PREVIOUSLY_QUOTED</li>
 * <li>FUNARI</li>
 * <li>MIT</li>
 * <li>MARKET_WITH_LEFTOVER_AS_LIMIT</li>
 * <li>PREVIOUS_FUND_VALUATION_POINT</li>
 * <li>NEXT_FUND_VALUATION_POINT</li>
 * <li>PEGGED</li>
 * <li>COUNTER_ORDER_SELECTION</li>
 * </ul>
 */
public interface IOrderType extends IDefinition<IOrderType> {

    String MARKET = "MARKET";
    String LIMIT = "LIMIT";
    String STOP = "STOP";
    String STOP_LIMIT = "STOP_LIMIT";
    String MARKET_ON_CLOSE = "MARKET_ON_CLOSE";
    String WITH_OR_WITHOUT = "WITH_OR_WITHOUT";
    String LIMIT_OR_BETTER = "LIMIT_OR_BETTER";
    String LIMIT_WITH_OR_WITHOUT = "LIMIT_WITH_OR_WITHOUT";
    String ON_BASIS = "ON_BASIS";
    String ON_CLOSE = "ON_CLOSE";
    String LIMIT_ON_CLOSE = "LIMIT_ON_CLOSE";
    String FOREX_MARKET = "FOREX_MARKET";
    String PREVIOUSLY_QUOTED = "PREVIOUSLY_QUOTED";
    String PREVIOUSLY_INDICATED = "PREVIOUSLY_INDICATED";
    String FOREX_LIMIT = "FOREX_LIMIT";
    String FOREX_SWAP = "FOREX_SWAP";
    String FOREX_PREVIOUSLY_QUOTED = "FOREX_PREVIOUSLY_QUOTED";
    String FUNARI = "FUNARI";
    String MIT = "MIT";
    String MARKET_WITH_LEFTOVER_AS_LIMIT = "MARKET_WITH_LEFTOVER_AS_LIMIT";
    String PREVIOUS_FUND_VALUATION_POINT = "PREVIOUS_FUND_VALUATION_POINT";
    String NEXT_FUND_VALUATION_POINT = "NEXT_FUND_VALUATION_POINT";
    String PEGGED = "PEGGED";
    String COUNTER_ORDER_SELECTION = "COUNTER_ORDER_SELECTION";

    char getFIXValue();

}





