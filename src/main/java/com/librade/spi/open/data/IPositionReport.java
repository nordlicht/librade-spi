package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IMarginMode;
import com.librade.spi.open.data.definitions.IPositionSide;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;

/**
 * <p>
 *     Positions allow you to borrow money from an exchange to go long or short on an market. Some exchanges require you
 *     to pay a funding fee to keep the position open.
 * </p>
 * <p>
 *     When you go long on a position you are betting that the price will be higher in the future and that the price will
 *     never be less than the liquidationPrice.
 * </p>
 * <p>
 *     As the price of the underlying index changes so does the unrealisedPnl and as a consequence the amount of collateral
 *     you have left in the position (since you can only close it at market price or worse). At some price you will have
 *     zero collateral left, this is called the "bust" or "zero" price. Beyond this point, if the price goes in the opposite
 *     direction far enough, the collateral of the position will drop below the maintenanceMargin. The maintenanceMargin
 *     acts as a safety buffer between your position and negative collateral, a scenario where the exchange incurs losses
 *     on your behalf. To protect itself the exchange will swiftly liquidate your position if and when this happens.
 *     Even if the price returns back above the liquidationPrice you will not get your money back since the exchange
 *     sold all the contracts you bought at market. In other words the maintenanceMargin is a hidden fee to borrow money.
 * </p>
 * <p>
 *     It is recommended to use the maintenanceMargin and initialMargin instead of the maintenanceMarginPercentage and
 *     initialMarginPercentage since these tend to be more accurate. The maintenanceMargin might be calculated from
 *     other factors outside of the maintenanceMarginPercentage including the funding rate and taker fees, for example on kucoin.
 * </p>
 * <p>
 *     An inverse contract will allow you to go long or short on BTC/USD by putting up BTC as collateral. Our API for
 *     inverse contracts is the same as for linear contracts. The amounts in an inverse contracts are quoted as if they
 *     were traded USD/BTC, however the price is still quoted terms of BTC/USD. The formula for the profit and loss of
 *     a inverse contract is (1/markPrice - 1/price) * contracts. The profit and loss and collateral will now be quoted
 *     in BTC, and the number of contracts are quoted in USD.
 * </p>
 */
public interface IPositionReport extends IBrokerToClientMessage {

    /**
     *
     * @return removes trailing zeros from all BigDecimals
     */
    IPositionReport stripTrailingZeros();

    Integer getBrokerAccountId();

    /**
     * @return response returned from the exchange as is
     */
    Map<String, Object> getInfo();

    /**
     * @return position id to reference the position, similar to an order id
     */
    String getPositionId();

    int getSecurityId();

    String getSecurity();

    ISecurityIdentifier getSecurityEnum();

    Instant getDateTime();

    /**
     * @return whether or not the position is hedged, i.e. if trading in the opposite direction will close this position or make a new one
     */
    Boolean getHedged();

    /**
     * @return long or short
     */
    IPositionSide getSide();

    /**
     * @return number of contracts bought, aka the amount or size of the position
     */
    BigDecimal getContracts();

    /**
     * @return the size of one contract in quote units
     */
    BigDecimal getContractSize();

    /**
     * @return the average entry price of the position
     */
    BigDecimal getEntryPrice();

    /**
     * @return a price that is used for funding calculations
     */
    BigDecimal getMarkPrice();

    /**
     * @return the value of the position in the settlement currency
     */
    BigDecimal getNotional();

    /**
     * @return the leverage of the position, related to how many contracts you can buy with a given amount of collateral
     */
    BigDecimal getLeverage();

    /**
     * @return the maximum amount of collateral that can be lost, affected by pnl
     */
    BigDecimal getCollateral();

    /**
     * @return the amount of collateral that is locked up in this position
     */
    BigDecimal getInitialMargin();

    /**
     * @return the mininum amount of collateral needed to avoid being liquidated
     */
    BigDecimal getMaintenanceMargin();

    /**
     * @return the initialMargin as a percentage of the notional
     */
    BigDecimal getInitialMarginPercentage();

    /**
     * @return the maintenanceMargin as a percentage of the notional
     */
    BigDecimal getMaintenanceMarginPercentage();

    /**
     * @return the difference between the market price and the entry price times the number of contracts, can be negative
     */
    BigDecimal getUnrealizedPnl();

    /**
     * @return the price at which collateral becomes less than maintenanceMargin
     */
    BigDecimal getLiquidationPrice();

    /**
     * @return can be cross or isolated
     */
    IMarginMode getMarginMode();

    /**
     * @return represents unrealizedPnl / initialMargin * 100
     */
    BigDecimal getPercentage();

}