package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Execution report status. Possible values:
 * <ul>
 * <li>UNSPECIFIED</li>
 * <li>New</li>
 * <li>PartiallyFilled</li>
 * <li>Filled</li>
 * <li>DoneForDay</li>
 * <li>Cancelled</li>
 * <li>Replaced</li>
 * <li>PendingCancel</li>
 * <li>Stopped</li>
 * <li>Rejected</li>
 * <li>Suspended</li>
 * <li>PendingNew</li>
 * <li>Calculated</li>
 * <li>Expired</li>
 * <li>AcceptedForBidding</li>
 * <li>PendingReplace</li>
 * </ul>
 */
public interface IOrderStatus extends IDefinition<IOrderStatus> {

    String UNSPECIFIED = "UNSPECIFIED";
    String New = "New";
    String PartiallyFilled = "PartiallyFilled";
    String Filled = "Filled";
    String DoneForDay = "DoneForDay";
    String Cancelled = "Cancelled";
    String Replaced = "Replaced";
    String PendingCancel = "PendingCancel";
    String Stopped = "Stopped";
    String Rejected = "Rejected";
    String Suspended = "Suspended";
    String PendingNew = "PendingNew";
    String Calculated = "Calculated";
    String Expired = "Expired";
    String AcceptedForBidding = "AcceptedForBidding";
    String PendingReplace = "PendingReplace";

    char getFIXValue();
}





