package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Defines the type of contingency.<br />
 *  * <ul>
 *  *     <li>One Cancels the Other (OCO)</li>
 *  *     <li>One Triggers the Other (OTO)</li>
 *  *     <li>One Updates the Other (OUO) - Absolute Quantity Reduction</li>
 *  *     <li>One Updates the Other (OUO) - Proportional Quantity Reduction</li>
 *  * </ul>
 * @author Tomas Fecko
 */
public interface IContingencyType extends IDefinition<ISide> {

    /**
     * One Cancels the Other (OCO).
     */
    String OCO = "OCO";
    /**
     * One Triggers the Other (OTO).
     */
    String OTO = "OTO";
    /**
     * One Updates the Other (OUO) - Absolute Quantity Reduction.
     */
    String OUO = "OUO";
    /**
     * One Updates the Other (OUO) - Proportional Quantity Reduction.
     */
    String OUOP = "OUOP";

    char getFIXValue();

}




