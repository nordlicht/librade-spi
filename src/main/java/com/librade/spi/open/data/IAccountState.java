package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IAccountPropertyType;
import com.librade.spi.open.data.definitions.IAsset;
import com.librade.spi.open.data.definitions.IBroker;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static com.librade.spi.open.data.definitions.IAccountPropertyType.BALANCE;

/**
 * Account state.
 */
public interface IAccountState extends IBrokerToClientMessage {

    IAccountState stripTrailingZeros();

    BigDecimal getPropertyValue(IAccountPropertyType pProperty);

    Map<IAsset, Set<IAccountProperty>> getAccountAssetProperties();

    IBroker getBroker();

    Integer getBrokerAccountId();

    String getText();

    default Optional<Map<IAsset, BigDecimal>> getAccountPropertiesByType(String accountPropertyTypeName) {
        Map<IAsset, Set<IAccountProperty>> accountAssetProperties = getAccountAssetProperties();
        if (accountAssetProperties == null) {
            return Optional.empty();
        }
        Map<IAsset, BigDecimal> returnMap = new HashMap<>();
        for (Map.Entry<IAsset, Set<IAccountProperty>> entry : accountAssetProperties.entrySet()) {
            entry.getValue().stream()
                    .filter(iAccountProperty -> iAccountProperty.getAccountPropertyType() != null)
                    .filter(iAccountProperty -> iAccountProperty.getValue() != null)
                    .filter(iAccountProperty -> String.valueOf(accountPropertyTypeName).equals(iAccountProperty.getAccountPropertyType().getName()))
                    .forEach(iAccountProperty -> returnMap.put(entry.getKey(), iAccountProperty.getValue()));
        }
        return Optional.of(returnMap);
    }

    default Optional<BigDecimal> getAssetPropertyValue(IAsset asset, IAccountPropertyType type) {
        if (getAccountAssetProperties() == null || getAccountAssetProperties().isEmpty() || type == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(getAccountAssetProperties().get(asset))
                .flatMap(properties -> properties.stream()
                        .filter(property -> type.equals(property.getAccountPropertyType()))
                        .findFirst()
                )
                .map(IAccountProperty::getValue);
    }

    default Set<IAsset> getAccountAssets() {
        Map<IAsset, Set<IAccountProperty>> properties = getAccountAssetProperties();
        if (properties == null || properties.isEmpty()) {
            return Collections.emptySet();
        }
        return Collections.unmodifiableSet(properties.keySet());
    }

    default Map<IAsset, BigDecimal> getBalances() {
        Map<IAsset, Set<IAccountProperty>> properties = getAccountAssetProperties();
        if (properties == null || properties.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<IAsset, BigDecimal> returnMap = new HashMap<>();
        for (Map.Entry<IAsset, Set<IAccountProperty>> entry : properties.entrySet()) {
            if (entry.getValue() == null) {
                continue;
            }
            entry.getValue().stream()
                    .filter(Objects::nonNull)
                    .filter(iAccountProperty -> BALANCE.equals(iAccountProperty.getAccountPropertyType().getName()))
                    .findFirst()
                    .ifPresent(iAccountProperty -> returnMap.put(entry.getKey(), iAccountProperty.getValue()));
        }
        return returnMap;
    }
}
