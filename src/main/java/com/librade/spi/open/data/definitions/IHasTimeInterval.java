package com.librade.spi.open.data.definitions;

@FunctionalInterface
public interface IHasTimeInterval {

    ITimeInterval getTimeInterval();

}
