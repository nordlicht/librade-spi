package com.librade.spi.open.data;

import com.librade.spi.open.data.blockchain.IBlockchainType;
import com.librade.spi.open.data.definitions.*;

import java.math.BigDecimal;

/**
 * Execution report.
 */
public interface IExecutionReport extends IBrokerToClientMessage {

    /**
     *
     * @return broker account id
     */
    Integer getBrokerAccountId();

    /**
     *
     * @return removes trailing zeros from all BigDecimals
     */
    IExecutionReport stripTrailingZeros();

    /**
     *
     * @return
     */
    double getPriceD();

    /**
     *
     * @return
     */
    int getPriceUnscaled();

    /**
     *
     * @return
     */
    int getPriceScale();

    /**
     *
     * @return
     */
    BigDecimal getAvgPrice();

    /**
     * Unique id which Librade platform sets to the order.
     * @return
     */
    String getClOrdId();

    /**
     *
     * @return
     */
    BigDecimal getCumulativeQuantity();

    /**
     *
     * @return
     */
    String getExecId();

    /**
     *
     * @return
     */
    IExecType getExecTypeEnum();

    /**
     *
     * @return
     */
    String getExecType();

    /**
     *
     * @return
     */
    int getExecTypeId();

    /**
     *
     * @return
     */
    long getExpire();

    /**
     *
     * @return
     */
    BigDecimal getLastPrice();

    /**
     *
     * @return
     */
    BigDecimal getLastShares();

    /**
     *
     * @return
     */
    BigDecimal getLeavesQuantity();

    /**
     * Unique id which broker sets to the order.
     * @return
     */
    String getOrderId();

    /**
     *
     * @return
     */
    IOrderType getOrderTypeEnum();

    /**
     *
     * @return
     */
    String getOrderType();

    /**
     *
     * @return
     */
    int getOrderTypeId();

    /**
     *
     * @return
     */
    String getOrigClOrdId();

    /**
     *
     * @return
     */
    BigDecimal getPrice();

    /**
     *
     * @return
     */
    BigDecimal getOrdQuantity();

    /**
     *
     * @return
     */
    IMsgType getRefMsgTypeEnum();

    /**
     * .
     * @return
     */
    String getRefMsgType();

    /**
     *
     * @return
     */
    int getRefMsgTypeId();

    /**
     *
     * @return
     */
    IOrdRejReason getRejReasonEnum();

    /**
     *
     * @return
     */
    String getRejReason();

    /**
     *
     * @return
     */
    int getRejReasonId();

    /**
     *
     * @return
     */
    int getSecurityId();

    /**
     *
     * @return
     */
    String getSecurity();

    /**
     *
     * @return
     */
    ISecurityIdentifier getSecurityEnum();

    /**
     *
     * @return
     */
    ISide getSideEnum();

    /**
     *
     * @return
     */
    String getSide();

    /**
     *
     * @return
     */
    int getSideId();

    /**
     *
     * @return
     */
    IOrderStatus getOrdStatusEnum();

    /**
     *
     * @return
     */
    String getOrdStatus();

    /**
     *
     * @return
     */
    int getOrdStatusId();

    /**
     *
     * @return
     */
    BigDecimal getStopPrice();

    /**
     *
     * @return
     */
    String getSymbol();

    /**
     *
     * @return
     */
    String getText();

    /**
     *
     * @return
     */
    ITimeInForce getTimeInForceEnum();

    /**
     *
     * @return
     */
    String getTimeInForce();

    /**
     *
     * @return
     */
    int getTimeInForceId();

    /**
     *
     * @return
     */
    long getTransactTime();

    /**
     *
     * @return
     */
    int getRefSeqNum();

    IBlockchainType getBlockchainType();
}






