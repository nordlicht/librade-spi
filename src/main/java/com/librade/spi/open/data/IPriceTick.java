package com.librade.spi.open.data;

import com.nordlicht.commons.base.serialization.byteable.IByteable;

import java.math.BigDecimal;

/**
 * @author Tomas Fecko
 */
public interface IPriceTick extends IByteable, Comparable<IPriceTick> {

    /**
     *
     * @return
     */
    BigDecimal getPriceBD();

    /**
     *
     * @return
     */
    long getPriceUnscaled();

    /**
     *
     * @return
     */
    int getPriceScale();

    /**
     *
     * @return
     */
    double getPrice();

    /**
     *
     * @return
     */
    BigDecimal getVolumeBD();

    /**
     *
     * @return volume
     */
    double getVolume();

    @Override
    default int compareTo(IPriceTick o) {
        return compareToOnPrice(o);
    }

    default int compareToOnPrice(IPriceTick o) {
        return compareBD(getPriceBD(), o.getPriceBD());
    }

    default int compareToOnVolume(IPriceTick o) {
        return compareBD(getVolumeBD(), o.getVolumeBD());
    }

    default int compareBD(BigDecimal o1, BigDecimal o2) {
        if (o1 == o2 || (o1 == null && o2 == null)) {
            return 0;
        } else if (o1 == null) {
            return -1;
        } else if (o2 == null) {
            return 1;
        } else {
            return o1.compareTo(o2);
        }
    }
}
