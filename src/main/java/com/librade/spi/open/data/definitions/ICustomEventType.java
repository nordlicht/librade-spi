package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Custom event type.
 *
 */
public interface ICustomEventType extends IDefinition<ICustomEventType> {
}






