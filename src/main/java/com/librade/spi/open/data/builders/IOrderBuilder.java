package com.librade.spi.open.data.builders;

import com.librade.spi.open.data.INewOrderList;
import com.librade.spi.open.data.INewOrderSingle;
import com.librade.spi.open.data.IOrderCancelRequest;
import com.librade.spi.open.data.IOrderStatusRequest;
import com.librade.spi.open.data.definitions.IContingencyType;
import com.librade.spi.open.data.definitions.IOrderType;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ISide;
import com.librade.spi.open.data.definitions.ITimeInForce;

/**
 * Order builder.
 */
public interface IOrderBuilder {

    /**
     *
     * @param pCustomAccountUID custom account uid
     * @return this builder
     */
    IOrderBuilder customAccountUID(String pCustomAccountUID);

    /**
     *
     * @param pSecurityIdentifier security identifier
     * @return this
     */
    IOrderBuilder securityIdentifier(ISecurityIdentifier pSecurityIdentifier);

    /**
     *
     * @param securityType (SPOT, PERPETUAL, ...) ISecurityType
     * @param broker broker
     * @param exchange exchange
     * @param symbol symbol
     * @param currency currency
     * @return this
     */
    IOrderBuilder securityIdentifier(String securityType, String broker, String exchange, String symbol, String currency);

    /**
     *
     * @param pSecurityIdentifier security identifier
     * @return this
     */
    IOrderBuilder securityIdentifier(int pSecurityIdentifier);

    /**
     *
     * @param pSide side
     * @return this
     */
    IOrderBuilder side(ISide pSide);

    /**
     *
     * @param pSide side
     * @return this
     */
    IOrderBuilder side(String pSide);

    /**
     *
     * @param pSide side
     * @return this
     */
    IOrderBuilder side(int pSide);

    /**
     *
     * @param pQuantity quantity
     * @return this
     */
    IOrderBuilder quantity(Number pQuantity);

    /**
     *
     * @param contingencyType contingency type
     * @return this
     */
    IOrderBuilder contingencyType(IContingencyType contingencyType);

    /**
     *
     * @param contingencyType  contingency type
     * @return this
     */
    IOrderBuilder contingencyType(String contingencyType);

    /**
     *
     * @param totNoOrders Total number of list order entries across all messages. Should be the sum of all NoOrders <73> in each
     *      message that has repeating list order entries related to the same ListID <66>. Used to support fragmentation.
     * @return this
     */
    IOrderBuilder totNoOrders(int totNoOrders);

    /**
     * Use this method, if building a list of orders for e.g. OCO order.
     * @param newOrderSingle new order single to be added to list
     * @return this
     */
    IOrderBuilder addToList(INewOrderSingle newOrderSingle);

    /**
     * Order type. Possible values (case sensitive):<br />
     * <ul>
     *     <li>UNSPECIFIED</li>
     *     <li>MARKET</li>
     *     <li>LIMIT</li>
     *     <li>STOP</li>
     *     <li>STOP_LIMIT</li>
     *     <li>MARKET_ON_CLOSE</li>
     *     <li>WITH_OR_WITHOUT</li>
     *     <li>LIMIT_OR_BETTER</li>
     *     <li>LIMIT_WITH_OR_WITHOUT</li>
     *     <li>ON_BASIS</li>
     *     <li>ON_CLOSE</li>
     *     <li>LIMIT_ON_CLOSE</li>
     *     <li>FOREX_MARKET</li>
     *     <li>PREVIOUSLY_QUOTED</li>
     *     <li>PREVIOUSLY_INDICATED</li>
     *     <li>FOREX_LIMIT</li>
     *     <li>FOREX_SWAP</li>
     *     <li>FOREX_PREVIOUSLY_QUOTED</li>
     *     <li>FUNARI</li>
     *     <li>MIT</li>
     *     <li>MARKET_WITH_LEFTOVER_AS_LIMIT</li>
     *     <li>PREVIOUS_FUND_VALUATION_POINT</li>
     *     <li>NEXT_FUND_VALUATION_POINT</li>
     *     <li>PEGGED</li>
     *     <li>COUNTER_ORDER_SELECTION</li>
     * </ul>
     * @param pOrderType order type
     * @return this
     */
    IOrderBuilder orderType(IOrderType pOrderType);

    /**
     * Order type. Possible values (case sensitive):<br />
     * <ul>
     *     <li>UNSPECIFIED</li>
     *     <li>MARKET</li>
     *     <li>LIMIT</li>
     *     <li>STOP</li>
     *     <li>STOP_LIMIT</li>
     *     <li>MARKET_ON_CLOSE</li>
     *     <li>WITH_OR_WITHOUT</li>
     *     <li>LIMIT_OR_BETTER</li>
     *     <li>LIMIT_WITH_OR_WITHOUT</li>
     *     <li>ON_BASIS</li>
     *     <li>ON_CLOSE</li>
     *     <li>LIMIT_ON_CLOSE</li>
     *     <li>FOREX_MARKET</li>
     *     <li>PREVIOUSLY_QUOTED</li>
     *     <li>PREVIOUSLY_INDICATED</li>
     *     <li>FOREX_LIMIT</li>
     *     <li>FOREX_SWAP</li>
     *     <li>FOREX_PREVIOUSLY_QUOTED</li>
     *     <li>FUNARI</li>
     *     <li>MIT</li>
     *     <li>MARKET_WITH_LEFTOVER_AS_LIMIT</li>
     *     <li>PREVIOUS_FUND_VALUATION_POINT</li>
     *     <li>NEXT_FUND_VALUATION_POINT</li>
     *     <li>PEGGED</li>
     *     <li>COUNTER_ORDER_SELECTION</li>
     * </ul>
     * @param pOrderType order type
     * @return this
     */
    IOrderBuilder orderType(String pOrderType);

    /**
     *
     * @param pPrice price (e.g. for limit, stop orders, ...)
     * @return this
     */
    IOrderBuilder price(Number pPrice);

    /**
     *
     * @param pStopPrice stop price (e.g. for stop orders)
     * @return this
     */
    IOrderBuilder stopPrice(Number pStopPrice);

    /**
     * Time in force. Possible values:
     * <ul>
     *     <li>UNSPECIFIED</li>
     *     <li>DAY</li>
     *     <li>GTC</li>
     *     <li>OPG</li>
     *     <li>IOC</li>
     *     <li>FOK</li>
     *     <li>GTX</li>
     *     <li>GTD</li>
     *     <li>ATC</li>
     * </ul>
     * Day (or session) (DAY).<br />
     * Good Till Cancel (GTC).<br />
     * At the Opening (OPG).<br />
     * Immediate Or Cancel (IOC).<br />
     * Fill Or Kill (FOK).<br />
     * Good Till Crossing (GTX).<br />
     * Good Till Date (GTD).<br />
     * At the Close (ATC).<br />
     * @param pTimeInForce time in force
     * @return this
     */
    IOrderBuilder timeInForce(ITimeInForce pTimeInForce);

    /**
     * Time in force. Possible values:
     * <ul>
     *     <li>UNSPECIFIED</li>
     *     <li>DAY</li>
     *     <li>GTC</li>
     *     <li>OPG</li>
     *     <li>IOC</li>
     *     <li>FOK</li>
     *     <li>GTX</li>
     *     <li>GTD</li>
     *     <li>ATC</li>
     * </ul>
     * Day (or session) (DAY).<br />
     * Good Till Cancel (GTC).<br />
     * At the Opening (OPG).<br />
     * Immediate Or Cancel (IOC).<br />
     * Fill Or Kill (FOK).<br />
     * Good Till Crossing (GTX).<br />
     * Good Till Date (GTD).<br />
     * At the Close (ATC).<br />
     * @param pTimeInForce time in force
     * @return this
     */
    IOrderBuilder timeInForce(String pTimeInForce);

    /**
     *
     * @param pExpire expire time
     * @return this
     */
    IOrderBuilder expire(long pExpire);

    /**
     *
     * @param pExpireTime expire time in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pTimeZone e.g.: "America/Los_Angeles"
     * @return this
     */
    IOrderBuilder expire(String pExpireTime, String pTimeZone);

    /**
     *
     * @param pOrigClOrdId original client order id
     * @return this
     */
    IOrderBuilder origClOrdId(String pOrigClOrdId);

    /**
     * Orders with same position id mark are grouped together allowing advanced statistics.
     * @param pPositionId position id must be positive integer, it must be unique for position in 1 session
     * @return this
     */
    IOrderBuilder markPosition(int pPositionId);

    IOrderBuilder note(String pNote);

    IOrderBuilder setAdditionalParams(String pAdditionalParams);

    /**
     *
     * @return new order single
     */
    INewOrderSingle buildNewOrderSingle();

    /**
     *
     * @return order cancel request
     */
    IOrderCancelRequest buildOrderCancelRequest();

    /**
     *
     * @param pOrderToCreateCancelFrom order to create cancel order from
     * @return order cancel request
     */
    default IOrderCancelRequest buildOrderCancelRequest(INewOrderSingle pOrderToCreateCancelFrom) {
        if (pOrderToCreateCancelFrom == null) {
            return null;
        }
        this.customAccountUID(pOrderToCreateCancelFrom.getCustomAccountID());
        this.expire(pOrderToCreateCancelFrom.getExpire());
        this.orderType(pOrderToCreateCancelFrom.getOrderType());
        this.origClOrdId(pOrderToCreateCancelFrom.getClOrdId());
        this.price(pOrderToCreateCancelFrom.getPrice());
        this.quantity(pOrderToCreateCancelFrom.getQuantity());
        this.securityIdentifier(pOrderToCreateCancelFrom.getSecurityId());
        this.side(pOrderToCreateCancelFrom.getSideEnum());
        this.timeInForce(pOrderToCreateCancelFrom.getTimeInForce());
        return buildOrderCancelRequest();
    }

    /**
     * To build combined orders like OCO, use this methid.
     * @param newOrderSingles if empty, only all the orders that were added through addToList(INewOrderSingle) method is used, otherwise it's combined
     * @return order list
     */
    INewOrderList buildOrderList(INewOrderSingle... newOrderSingles);

    /**
     *
     * @return order status request
     */
    IOrderStatusRequest buildOrderStatusRequest();

    /**
     *
     * @param pOrderToCreateStatusFrom order to create status request from
     * @return order status request
     */
    default IOrderStatusRequest buildOrderStatusRequest(INewOrderSingle pOrderToCreateStatusFrom) {
        if (pOrderToCreateStatusFrom == null) {
            return null;
        }
        this.customAccountUID(pOrderToCreateStatusFrom.getCustomAccountID());
        this.origClOrdId(pOrderToCreateStatusFrom.getClOrdId());
        this.securityIdentifier(pOrderToCreateStatusFrom.getSecurityId());
        this.side(pOrderToCreateStatusFrom.getSideEnum());
        return buildOrderStatusRequest();
    }
}






