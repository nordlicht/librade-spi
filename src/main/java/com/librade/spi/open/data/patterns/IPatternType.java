package com.librade.spi.open.data.patterns;

import com.nordlicht.IDefinition;

public interface IPatternType extends IDefinition<IPatternType> {

    String UNSPECIFIED         = "UNSPECIFIED";
    String KEY_LEVELS          = "KEY_LEVELS";
    String FIBONACCI_PATTERNS  = "FIBONACCI_PATTERNS";
    String CHART_PATTERNS      = "CHART_PATTERNS";
    String CONSECUTIVE_CANDLES = "CONSECUTIVE_CANDLES";
    String BIG_MOVEMENT        = "BIG_MOVEMENT";

    String getCode();

}
