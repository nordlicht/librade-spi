package com.librade.spi.open.data.securityIdentifier;

import java.math.BigDecimal;
import java.util.Objects;
import java.util.StringJoiner;

/**
 *
 * @author Stanislav Fujdiar
 */
public class Amount {

    private final BigDecimal min;
    private final BigDecimal max;

    public Amount(BigDecimal min, BigDecimal max) {
        this.min = min;
        this.max = max;
    }

    public Amount stripTrailingZeros() {
        return new Amount(
                min != null ? min.stripTrailingZeros() : null,
                max != null ? max.stripTrailingZeros() : null
        );
    }

    public BigDecimal getMin() {
        return min;
    }

    public BigDecimal getMax() {
        return max;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Amount)) return false;
        Amount amount = (Amount) o;
        return Objects.equals(min, amount.min) && Objects.equals(max, amount.max);
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Amount.class.getSimpleName() + "[", "]")
                .add("min=" + min)
                .add("max=" + max)
                .toString();
    }
}
