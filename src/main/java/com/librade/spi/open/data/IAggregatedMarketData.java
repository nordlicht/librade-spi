package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.ITimeInterval;

import java.math.BigDecimal;

/**
 *
 */
public interface IAggregatedMarketData extends IMarketDataParent {

    /**
     *
     * @return
     */
    int getId();

    double getOpen();

    BigDecimal getOpenBD();

    int getOpenUnscaled();

    int getOpenScale();

    double getHigh();

    BigDecimal getHighBD();

    int getHighUnscaled();

    int getHighScale();

    double getLow();

    BigDecimal getLowBD();

    int getLowUnscaled();

    int getLowScale();

    double getClose();

    BigDecimal getCloseBD();

    double getVolume();

    BigDecimal getVolumeBD();

    int getVolumeUnscaled();

    int getVolumeScale();

    int getCloseUnscaled();

    int getCloseScale();

    int getTimeIntervalId();

    String getTimeInterval();

    ITimeInterval getTimeIntervalEnum();

    long getTimeIntervalTimespan();

    IAggregatedMarketData copy();

    boolean isNotEmpty();
}






