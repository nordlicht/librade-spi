package com.librade.spi.open.data;

/**
 * Order status request.
 */
public interface IOrderStatusRequest extends IClientToBrokerMessage {
}
