package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Data storage type. Possible values:
 * <ul>
 *     <li>NEAR_CACHE</li>
 *     <li>DISTRIBUTED_CACHE</li>
 *     <li>DATABASE</li>
 *     <li>FILE_STORAGE</li>
 * </ul>
 * Description:
 * <ul>
 *     <li>NEAR_CACHE - is inMemory cache of this server instance container, it's limited only by the RAM of the container</li>
 *     <li>DISTRIBUTED_CACHE - one variable can't be bigger than 64KB</li>
 *     <li>DATABASE - one variable can't be bigger than 16MB</li>
 *     <li>FILE_STORAGE - one variable can't be bigger than 1GB</li>
 * </ul>
 * @author Tomas Fecko
 */
public interface IDataStorageType extends IDefinition<IDataStorageType> {

    String NEAR_CACHE = "NEAR_CACHE";
    String DISTRIBUTED_CACHE = "DISTRIBUTED_CACHE";
    String DATABASE = "DATABASE";
    String FILE_STORAGE = "FILE_STORAGE";
}





