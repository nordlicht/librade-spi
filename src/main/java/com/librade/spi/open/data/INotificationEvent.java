package com.librade.spi.open.data;

import com.nordlicht.ITimeSeriesData;

/**
 * Notification event.
 */
public interface INotificationEvent extends ITimeSeriesData {

    /**
     *
     * @return
     */
    String getValue();

    /**
     *
     * @return
     */
    String getCustomAccountId();

    /**
     *
     * @return
     */
    int getEventCode();

    /**
     *
     * @return
     */
    String getDescription();
}
