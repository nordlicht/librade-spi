package com.librade.spi.open.data.analytics;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author Stano Fujdiar
 */
public class SerializableTable extends Table<Serializable> {

    private ArrayList<Serializable>    mValues;

    public SerializableTable() {
    }

    /**
     *
     * @param pRows rows
     * @param pCols columns
     */
    public SerializableTable(int pRows, int pCols) {
        super(pRows, pCols);
    }

    @Override
    public ArrayList<Serializable> getValues() {
        return mValues;
    }

    @Override
    public void setValues(ArrayList<Serializable> pValues) {
        mValues = pValues;
    }

    @Override
    public int getByteableClassId() {
        return 3200;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        if (!super.equals(pO)) {
            return false;
        }
        final SerializableTable that = (SerializableTable) pO;
        return Objects.equals(mValues, that.mValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), mValues);
    }
}





