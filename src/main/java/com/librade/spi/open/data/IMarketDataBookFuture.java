package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IAsset;

import java.math.BigDecimal;

import static com.nordlicht.IDefinition.UNSPECIFIED;
import static com.nordlicht.IDefinition.UNSPECIFIED_ID;

/**
 *
 */
public interface IMarketDataBookFuture extends IMarketDataBook {

    /**
     *
     * @return future delivery time
     */
    long getDeliveryTime();

    /**
     * Identifier of the asset used to denominate the contract unit.
     * @return
     */
    IAsset getContractUnitAssetEnum();

    /**
     * Identifier of the asset used to denominate the contract unit.
     * @return
     */
    default int getContractUnitAssetId() {
        return getContractUnitAssetEnum() == null ? UNSPECIFIED_ID : getContractUnitAssetEnum().getId();
    }

    /**
     * Identifier of the asset used to denominate the contract unit.
     * @return
     */
    default String getContractUnitAsset() {
        return getContractUnitAssetEnum() == null ? UNSPECIFIED : getContractUnitAssetEnum().getName();
    }

    /**
     * Base amount of underlying spot asset which single future represents.
     * @return
     */
    BigDecimal getContractUnit();
}






