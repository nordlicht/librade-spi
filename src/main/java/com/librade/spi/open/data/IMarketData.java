package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IEventType;
import com.librade.spi.open.data.definitions.IPriceType;

import java.math.BigDecimal;

/**
 *
 */
public interface IMarketData extends IMarketDataParent {

    /**
     *
     * @return
     */
    int getId();

    /**
     *
     * @return
     */
    String getEventType();

    /**
     *
     * @return
     */
    int getEventTypeId();

    /**
     *
     * @return
     */
    IEventType getEventTypeEnum();

    /**
     *
     * @return
     */
    String getPriceType();

    /**
     *
     * @return
     */
    int getPriceTypeId();

    /**
     *
     * @return
     */
    IPriceType getPriceTypeEnum();

    /**
     *
     * @return
     */
    BigDecimal getPriceBD();

    /**
     *
     * @return
     */
    long getPriceUnscaled();

    /**
     *
     * @return
     */
    int getPriceScale();

    /**
     *
     * @return
     */
    double getPrice();

    /**
     *
     * @return
     */
    BigDecimal getVolumeBD();

    /**
     *
     * @return volume
     */
    double getVolume();

    /**
     *
     * @return data UID
     */
    String getDataUID();

    /**
     *
     * @return
     */
    IMarketData copy();
}






