package com.librade.spi.open.data.securityIdentifier;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.StringJoiner;

/**
 *
 * @author Stanislav Fujdiar
 */
public class Cost implements Serializable {

    private final BigDecimal min;
    private final BigDecimal max;

    public Cost(BigDecimal min, BigDecimal max) {
        this.min = min;
        this.max = max;
    }

    public Cost stripTrailingZeros() {
        return new Cost(
                min != null ? min.stripTrailingZeros() : null,
                max != null ? max.stripTrailingZeros() : null
        );
    }

    public BigDecimal getMin() {
        return min;
    }

    public BigDecimal getMax() {
        return max;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Cost.class.getSimpleName() + "[", "]")
                .add("min=" + min)
                .add("max=" + max)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cost)) return false;
        Cost cost = (Cost) o;
        return Objects.equals(min, cost.min) && Objects.equals(max, cost.max);
    }

    @Override
    public int hashCode() {
        return Objects.hash(min, max);
    }
}
