package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * @author Tomas Fecko
 */
public interface IOptionExerciseStyle extends IDefinition<IOptionExerciseStyle> {

    String EUROPEAN = "EUROPEAN";
    String AMERICAN = "AMERICAN";

}






