package com.librade.spi.open.data.analytics;

/**
 * @author Stano Fujdiar
 */
public abstract class Table<T> extends Matrix<T> {

    public Table() {
    }

    /**
     *
     * @param pRows rows
     * @param pCols columns
     */
    public Table(int pRows, int pCols) {
        super(pRows, pCols);
    }
}





