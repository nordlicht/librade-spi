package com.librade.spi.open.data;

/**
 * Order cancel request.
 */
public interface IOrderCancelRequest extends IClientToBrokerMessage {

    /**
     *
     * @return
     */
    String getOrigClOrdId();

    /**
     *
     * @return
     */
    long getTransactTime();
}






