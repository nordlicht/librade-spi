package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Monitoring type.
 */
public interface IMonitoringType extends IDefinition<IMonitoringType> {
}
