package com.librade.spi.open.data.tradingSignal;

import com.nordlicht.IDefinition;

public interface ITradingSignalAction extends IDefinition<ITradingSignalAction>  {

    String STAY_SHORT = "STAY_SHORT";
    String STAY_LONG = "STAY_LONG";
    String CLOSE_SHORT = "CLOSE_SHORT";
    String CLOSE_LONG = "CLOSE_LONG";
}
