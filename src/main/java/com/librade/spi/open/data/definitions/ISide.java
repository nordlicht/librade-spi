package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Side. Possible values (case sensitive):<br />
 * <ul>
 *     <li>BUY</li>
 *     <li>SELL</li>
 * </ul>
 */
public interface ISide extends IDefinition<ISide> {

    String BUY = "BUY";
    String SELL = "SELL";

    char getFIXValue();

}
