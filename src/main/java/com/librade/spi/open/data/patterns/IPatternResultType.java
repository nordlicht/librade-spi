package com.librade.spi.open.data.patterns;

import com.nordlicht.IDefinition;

public interface IPatternResultType extends IDefinition<IPatternResultType> {

    String VAL_UNSPECIFIED = "UNSPECIFIED";
    String VAL_EMERGING = "EMERGING";
    String VAL_BREAKOUT = "BREAKOUT";
    String VAL_APPROACH = "APPROACH";
    String VAL_COMPLETED = "COMPLETED";

    String getCode();

}
