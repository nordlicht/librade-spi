package com.librade.spi.open.data.analytics;

import com.nordlicht.commons.base.serialization.byteable.IByteable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * @param <T> type of value
 * @author Stano Fujdiar
 */
public abstract class Matrix<T> implements Iterable<T>, IByteable {

    private int[]     mSize;

    public Matrix() {
    }

    /**
     *
     * @param pSize size of matrix
     */
    @SuppressWarnings("unchecked")
    public Matrix(int... pSize) {
        mSize = pSize;
        int length = 1;
        for (Integer i : pSize) {
            length = length * i;
        }
        setValues(new ArrayList<>());
    }

    /**
     * @param pValues values of new matrix
     * @param pSize size of matrix
     */
    public Matrix(T[] pValues, int... pSize) {
        mSize = pSize;
        int size = 1;
        for (int i : pSize) {
            size = size * i;
        }
        setValues(new ArrayList<>(size));
        if (pValues != null) {
            if (size != pValues.length) {
                throw new IllegalArgumentException("Wrong count of input values. According to pSize, there should be:" + size + " items in array. Array size:"
                        + pValues.length);
            }
            for (T value : pValues) {
                getValues().add(value);
            }
        }
    }

    /**
     * @param pValues values of new matrix
     * @param pSize size of matrix
     */
    public Matrix(Collection<T> pValues, int... pSize) {
        mSize = pSize;
        int size = 1;
        for (int i : pSize) {
            size = size * i;
        }
        setValues(new ArrayList<>(size));
        if (pValues != null) {
            if (size != pValues.size()) {
                throw new IllegalArgumentException("Wrong count of input values. According to pSize, there should be:" + size + " items in collection."
                        +  " Collection size:" + pValues.size());
            }
            for (T value : pValues) {
                getValues().add(value);
            }
        }
    }



    public abstract ArrayList<T> getValues();

    public abstract void setValues(ArrayList<T> pValues);

    public final int[] getSize() {
        return mSize;
    }

    public void setSize(int[] pSize) {
        mSize = pSize;
    }

    public final void setValue(T pValue, int... pCoordinates) {
        if (pCoordinates.length != mSize.length) {
            throw new IllegalArgumentException("bad coordinates number");
        }
        int index = 0;
        index = getIndex(index, pCoordinates);
        getValues().set(index, pValue);
    }

    public final void setValue(T pValue, int pIndex) {
        if (pIndex < getValues().size() && pIndex >= 0) {
            getValues().set(pIndex, pValue);
        }
    }

    /**
     *
     * @param pCoordinates
     * @return
     */
    public final T getValue(int... pCoordinates) {
        if (pCoordinates.length != mSize.length) {
            throw new IllegalArgumentException("bad coordinates number");
        }
        int index = 0;
        index = getIndex(index, pCoordinates);
        return getValues().get(index);
    }

    /**
     *
     * @param pIndex
     * @param pCoordinates
     * @return
     */
    private int getIndex(int pIndex, int[] pCoordinates) {
        for (int i = 0; i < pCoordinates.length; i++) {
            int innerSize = 1;
            for (int j = i + 1; j < mSize.length; j++) {
                innerSize = innerSize * mSize[j];
            }
            pIndex = pIndex + pCoordinates[i] * innerSize;
        }
        return pIndex;
    }

    /**
     *
     * @param pIndex
     * @return
     */
    public T getValue(int pIndex) {
        if (getValues() == null) {
            setValues(new ArrayList<>());
        }
        return getValues().get(pIndex);
    }

    /**
     *
     * @return
     */
    public final Iterator<T> iterator() {
        if (getValues() == null) {
            setValues(new ArrayList<>());
        }
        return getValues().iterator();
    }

    @Override
    public String toString() {
        return "Matrix{"
                + "mSize=" + Arrays.toString(mSize)
                + ", mValues.size()=" + (getValues() != null ? getValues().size() : "0")
                + '}';
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        final Matrix<?> matrix = (Matrix<?>) pO;
        return Arrays.equals(mSize, matrix.mSize);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(mSize);
    }
}
