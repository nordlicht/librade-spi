package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * Control message type.
 */
public interface IControlMessageType extends IDefinition<IControlMessageType> {
}
