package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * @author Tomas Fecko
 */
public interface ISecurityType extends IDefinition<ISecurityType> {

    String UNSPECIFIED = "UNSPECIFIED";
    /**
     * In finance, a spot contract, spot transaction, or simply spot, is a contract of buying or selling a commodity, security or currency
     * for immediate settlement (payment and delivery) on the spot date, which is normally two business days after the trade date.
     */
    String SPOT = "SPOT";
    /**
     * An options contract is an agreement between a buyer and seller that gives the purchaser of the option the right to buy or sell a particular
     * asset at a later date at an agreed upon price. Options contracts are often used in securities, commodities, and real estate transactions.
     */
    String OPTION = "OPTION";
    /**
     * A futures contract is a legal agreement to buy or sell a particular commodity asset, or security at a predetermined price
     * at a specified time in the future. Futures contracts are standardized for quality and quantity to facilitate trading on a futures exchange.
     */
    String FUTURES = "FUTURES";
    /**
     * A perpetual contract is a special type of futures contract, but unlike the traditional form of futures, it doesn't have an expiry date.
     * So one can hold a position for as long as they like. Other than that, the trading of perpetual contracts is based on an underlying Index Price.
     */
    String PERPETUAL = "PERPETUAL";
    /**
     * Statistical composite that measures changes in the economy or markets.
     */
    String INDEX = "INDEX";
    /**
     * Margin funding contract. Order book displays lending offers and borrow bids. Price represents the daily rate.
     */
    String CREDIT = "CREDIT";

}
