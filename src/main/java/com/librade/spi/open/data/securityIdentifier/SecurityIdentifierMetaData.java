package com.librade.spi.open.data.securityIdentifier;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nordlicht.commons.base.serialization.byteable.IJsonByteable;

import java.util.Objects;
import java.util.StringJoiner;

/**
 *
 * @author Stanislav Fujdiar
 */
public class SecurityIdentifierMetaData implements IJsonByteable {

    private final Precision precision;
    private final Limits limits;

    public SecurityIdentifierMetaData(Precision precision, Limits limits) {
        this.precision = precision;
        this.limits = limits;
    }

    public Precision getPrecision() {
        return precision;
    }

    public Limits getLimits() {
        return limits;
    }

    @JsonIgnore
    public SecurityIdentifierMetaData stripTrailingZeros() {
        return new SecurityIdentifierMetaData(
                precision,
                limits != null ? limits.stripTrailingZeros() : null
        );
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SecurityIdentifierMetaData.class.getSimpleName() + "[", "]")
                .add("precision=" + precision)
                .add("limits=" + limits)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SecurityIdentifierMetaData)) return false;
        SecurityIdentifierMetaData that = (SecurityIdentifierMetaData) o;
        return Objects.equals(precision, that.precision) && Objects.equals(limits, that.limits);
    }

    @Override
    public int hashCode() {
        return Objects.hash(precision, limits);
    }
}





