package com.librade.spi.open.data.definitions;

import com.nordlicht.IDefinition;

/**
 * @author Stanislav Fujdiar
 */
public interface IComputingSessionState extends IDefinition<IComputingSessionState> {
}
