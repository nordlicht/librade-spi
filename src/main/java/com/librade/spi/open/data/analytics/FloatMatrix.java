package com.librade.spi.open.data.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 * @param <T> type of value
 * @author Stano Fujdiar
 */
public class FloatMatrix extends Matrix<Float> {

    private ArrayList<Float>    mValues;

    public FloatMatrix() {
    }

    public FloatMatrix(int... pSize) {
        super(pSize);
    }

    public FloatMatrix(Float[] pValues, int... pSize) {
        super(pValues, pSize);
    }

    public FloatMatrix(Collection<Float> pValues, int... pSize) {
        super(pValues, pSize);
    }

    @Override
    public ArrayList<Float> getValues() {
        return mValues;
    }

    @Override
    public void setValues(ArrayList<Float> pValues) {
        mValues = pValues;
    }

    @Override
    public int getByteableClassId() {
        return 3202;
    }

    @Override
    public boolean equals(Object pO) {
        if (this == pO) {
            return true;
        }
        if (pO == null || getClass() != pO.getClass()) {
            return false;
        }
        if (!super.equals(pO)) {
            return false;
        }
        final FloatMatrix floats = (FloatMatrix) pO;
        return Objects.equals(mValues, floats.mValues);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), mValues);
    }
}








