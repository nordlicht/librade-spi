package com.librade.spi.open;

import com.librade.spi.open.data.definitions.IInputType;
import com.librade.spi.open.data.transformation.IParsed;
import com.librade.spi.open.exceptions.APIException;

/**
 * @author Tomas Fecko
 */
public interface IDataTransform {

    /**
     *
     * @param pInput input string
     * @param pInputType input type
     * @return input dataTransform
     */
    IParsed parse(String pInput, IInputType pInputType) throws APIException;

    /**
     *
     * @param pInput input string
     * @return input dataTransform
     */
    IParsed parseInputParameters(String pInput) throws APIException;


}





