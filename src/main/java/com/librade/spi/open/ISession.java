package com.librade.spi.open;

import com.librade.spi.open.exceptions.APIException;

/**
 *
 */
public interface ISession {

    /**
     * Method stops the analystAPI.
     * @throws APIException any exception
     */
    void stopSession() throws APIException;

    /**
     * Returns true, if session is stopped. Algo should check this, and if it returns true, it should stop itself.
     * @return true, if analyst API is stopped
     */
    boolean isStopped();

    /**
     * Session UID.
     * @return real time trading session identifier
     */
    String getSessionUID();

    /**
     * Session UID.
     * @return real time trading session identifier
     */
    String getSessionName();

    /**
     * Method returns true, if user restores the session, that was started before.
     * @return true, if session is restored (not started)
     */
    boolean isRestoredSession();
}








