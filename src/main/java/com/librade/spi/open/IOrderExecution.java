package com.librade.spi.open;

import com.librade.spi.open.data.IClientToBrokerMessage;
import com.librade.spi.open.data.IExecutionReport;
import com.librade.spi.open.data.blockchain.BlockchainTransaction;
import com.librade.spi.open.data.builders.IOrderBuilder;
import com.librade.spi.open.exceptions.APIException;

import java.util.List;
import java.util.Optional;

/**
 *
 */
public interface IOrderExecution {

    /**
     * Sends orders to broker. (e.g. OrderCancelRequest, OrderStatusRequest, NewOrderSingle, ...)
     * @param pOrders orders
     * @throws APIException exception during the order processing
     * @return ids of orders
     */
    List<String> sendOrders(IClientToBrokerMessage... pOrders) throws APIException;

    /**
     * Sends order to broker. (e.g. OrderCancelRequest, OrderStatusRequest, NewOrderSingle, ...)
     * @param pOrder order
     * @throws APIException exception during the order processing
     * @return id of order
     */
    String sendOrder(IClientToBrokerMessage pOrder) throws APIException;

    /**
     * Sends order to broker and waiting for ExecutionReport.
     * @param pOrder order
     * @throws APIException exception during the order processing
     * @return execution report, returned by broker/exchange
     */
    default Optional<IExecutionReport> sendOrderSync(IClientToBrokerMessage pOrder) throws APIException {
        return Optional.empty();
    }

    /**
     * Send order to broker, which is selected automaticaly based on predefined criteria (best price, amount available, ...).
     * @param pOrder order, which has special security identifier set: broker == ALL_BROKERS, exchange == ALL_EXCHANGES, symbol != currency
     * @throws APIException exception during the order processing
     * @return id of order
     */
    default String sendSmartOrder(IClientToBrokerMessage pOrder) throws APIException {
        return null;
    }

    /**
     * Method creates order factory.
     * @return order builder
     */
    IOrderBuilder createOrderBuilder();

    /**
     * Method returns ExecutionReports for order which was sent, from the database.
     * When order is sent for execution, ExcutionReport about the order execution should be sent through CEP in next couple of milliseconds.
     * If it's not catched with the query, this method should return all the execution reports that was sent for that order.
     * @param pOrder order to get execution reports for
     * @return execution reports or empty list, if there is none stored in db
     */
    List<IExecutionReport> getExecutionReports(IClientToBrokerMessage pOrder) throws APIException;

    /**
     * Method returns execution reports ordered DESC by timestamp.
     * @param page page to return, if the number is <= 0, 0 is sent
     * @param itemsPerPage items per page (max. is 1000, if bigger number is sent or a number <= 0, 1000 is sent)
     * @return execution reports
     */
    List<IExecutionReport> getLastExecutionReports(int page, int itemsPerPage);

    void processBlockchainTransaction(BlockchainTransaction transaction, String pCustomAccountId) throws APIException;
}







