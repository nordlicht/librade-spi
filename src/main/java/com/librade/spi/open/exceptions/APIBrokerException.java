package com.librade.spi.open.exceptions;

/**
 * @author Stanislav Fujdiar
 */
public class APIBrokerException extends APIRuntimeException {

    /**
     *
     */
    public APIBrokerException() {
        super();
    }

    /**
     *
     * @param pMessage exception message
     */
    public APIBrokerException(String pMessage) {
        super(pMessage);
    }

    /**
     *
     * @param message exception message
     * @param pCause cause
     */
    public APIBrokerException(String message, Throwable pCause) {
        super(message, pCause);
    }

    /**
     *
     * @param pCause cause
     */
    public APIBrokerException(Throwable pCause) {
        super(pCause);
    }
}
