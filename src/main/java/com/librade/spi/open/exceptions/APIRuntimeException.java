package com.librade.spi.open.exceptions;

import com.nordlicht.commons.base.exceptions.ARuntimeException;

/**
 * Librade API checked exception.
 *
 */
public class APIRuntimeException extends ARuntimeException {

    /**
     *
     */
    public APIRuntimeException() {
    }

    /**
     *
     * @param pMessage exception message
     */
    public APIRuntimeException(String pMessage) {
        super(pMessage);
    }

    /**
     *
     * @param pMmessage exception message
     * @param pCause cause
     */
    public APIRuntimeException(String pMmessage, Throwable pCause) {
        super(pMmessage, pCause);
    }

    /**
     *
     * @param pCause cause
     */
    public APIRuntimeException(Throwable pCause) {
        super(pCause);
    }
}






