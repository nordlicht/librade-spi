package com.librade.spi.open.exceptions;

/**
 * Librade API checked exception.
 *
 */
public class APIException extends Exception {

    /**
     *
     */
    public APIException() {
    }

    /**
     *
     * @param pMessage exception message
     */
    public APIException(String pMessage) {
        super(pMessage);
    }

    /**
     *
     * @param pMmessage exception message
     * @param pCause cause
     */
    public APIException(String pMmessage, Throwable pCause) {
        super(pMmessage, pCause);
    }

    /**
     *
     * @param pCause cause
     */
    public APIException(Throwable pCause) {
        super(pCause);
    }
}






