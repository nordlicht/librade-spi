package com.librade.spi.open;

import com.librade.spi.open.exceptions.APIException;

/**
 * Computer human interaction methods.
 */
public interface IComputerHumanInteraction {

    /**
     * Method calls a specified number, while it reads the text provided by parameter over the phone.
     * If user enters input through phone keyboard, CustomEvent is sent with the UID, which is returned by this methodI.
     * @param pTelephoneNumber telephone number
     * @param pRequestToUser request to user
     * @return custom event UID
     * @throws APIException any exception
     */
    String requestVoiceUserInput(String pTelephoneNumber, String pRequestToUser) throws APIException;

    /**
     * Method sends sms to specified number.
     * @param pTelephoneNumber telephone number
     * @param pSMSText sms text
     * @throws APIException if sending of sms fail (it could be e.g. insufficient credit, or technical issue)
     */
    void sendSMS(String pTelephoneNumber, String pSMSText) throws APIException;

    /**
     * Method sends email to specified email address.
     * @param pEmailAddress email address
     * @param pEmailSubject email subject
     * @param pEmailText email text
     * @throws APIException if sending of email fails
     */
    void sendEmail(String pEmailAddress, String pEmailSubject, String pEmailText) throws APIException;

    /**
     *
     * @param pEmailAddress
     * @param pEmailSubject
     * @param pEmailTextHTML
     * @throws APIException
     */
    void sendHTMLEmail(String pEmailAddress, String pEmailSubject, String pEmailTextHTML) throws APIException;
}





