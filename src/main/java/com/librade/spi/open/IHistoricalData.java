package com.librade.spi.open;

import com.librade.spi.open.data.IAggregatedMarketData;
import com.librade.spi.open.data.IAnalyticsEvent;
import com.librade.spi.open.data.IResultBuffer;
import com.librade.spi.open.data.definitions.IAnalyticsType;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ITimeInterval;
import com.librade.spi.open.exceptions.APIException;
import com.nordlicht.ITimeSeriesData;

import java.math.BigDecimal;
import java.util.*;

/**
 *
 */
public interface IHistoricalData {

    /**
     *
     * @param pStartTime start time in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pEndTime end time in format: dd.MM.yyyy HH:mm:ss.SSS
     * @param pTimeZone e.g.: "America/Los_Angeles"
     * @param pIdentifiers identifiers
     * @return result buffer
     * @throws APIException any exception during select
     */
    IResultBuffer<ITimeSeriesData> selectMarketData(String pStartTime, String pEndTime, String pTimeZone, int[] pIdentifiers)
            throws APIException;

    /**
     *
     * @param pStartTime start time in millis (GMT0)
     * @param pEndTime end time in millis  (GMT0)
     * @param pIdentifiers identifiers
     * @return result buffer
     * @throws APIException any exception during select
     */
    IResultBuffer<ITimeSeriesData> selectMarketData(long pStartTime, long pEndTime, int[] pIdentifiers) throws APIException;

    /**
     *
     * @param pStartTime start time in millis (GMT0)
     * @param pEndTime end time in millis  (GMT0)
     * @param pIdentifiers identifiers
     * @return Map of max high prices of pIdentifiers
     * @throws APIException any exception during select
     */
    Map<Integer, IAggregatedMarketData> selectMaxHighPrices(long pStartTime, long pEndTime, int[] pIdentifiers) throws APIException;

    /**
     *
     * @param numberOfLastValues last values count, max. is 1000
     * @param analyticsType analytics type (e.g. SMA5)
     * @param securityIdentifiers security identifiers
     * @param timeInterval time interval
     * @return map of security identifiers to analyticValues
     * @throws APIException exception, that could happen during the selection from db
     */
    Map<ISecurityIdentifier, List<IAnalyticsEvent<ISecurityIdentifier, BigDecimal>>> selectLastAnalytics(int numberOfLastValues,
                                                                                                         IAnalyticsType analyticsType,
                                                                                                         Collection<ISecurityIdentifier> securityIdentifiers,
                                                                                                         ITimeInterval timeInterval) throws APIException;

    /**
     *
     * @param numberOfLastValues number of values
     * @param analyticsType analytics type
     * @param securityIdentifier security identifier
     * @param timeInterval time interval
     * @return last analytics values
     * @throws APIException
     */
    default List<IAnalyticsEvent<ISecurityIdentifier, BigDecimal>> selectLastAnalytics(int numberOfLastValues,
                                              IAnalyticsType analyticsType,
                                              ISecurityIdentifier securityIdentifier,
                                              ITimeInterval timeInterval) throws APIException {
        Map<ISecurityIdentifier, List<IAnalyticsEvent<ISecurityIdentifier, BigDecimal>>> map = selectLastAnalytics(numberOfLastValues, analyticsType,
                Collections.singletonList(securityIdentifier), timeInterval);
        if (map == null || map.isEmpty()) {
            return Collections.emptyList();
        }
        return map.get(securityIdentifier);
    }

}





