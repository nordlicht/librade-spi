package com.librade.spi.open;

/**
 *
 */
public interface ILog {

    /**
     * Method logs the message with trace level.
     * @param pMessage message to log
     */
    void trace(Object... pMessage);

    /**
     * Method logs the message with debug level.
     * @param pMessage message to log
     */
    void debug(Object... pMessage);

    /**
     * Method logs the message with info level.
     * @param pMessage message to log
     */
    void info(Object... pMessage);

    /**
     * Method logs the message with warn level.
     * @param pMessage message to log
     */
    void warn(Object... pMessage);

    /**
     * Method logs the message with error level.
     * @param pMessage message to log
     */
    void error(Object... pMessage);
}





