package com.librade.spi.open;

import com.librade.spi.open.data.IExecutionReport;
import com.librade.spi.open.data.IMarketData;
import com.librade.spi.open.data.definitions.IAccountPropertyType;
import com.librade.spi.open.data.definitions.IAnalyticsType;
import com.librade.spi.open.data.definitions.IAsset;
import com.librade.spi.open.data.definitions.IBroker;
import com.librade.spi.open.data.definitions.ICustomEventType;
import com.librade.spi.open.data.definitions.IDataStorageType;
import com.librade.spi.open.data.definitions.IEventType;
import com.librade.spi.open.data.definitions.IExchange;
import com.librade.spi.open.data.definitions.IInputType;
import com.librade.spi.open.data.definitions.IOptionType;
import com.librade.spi.open.data.definitions.IOrderType;
import com.librade.spi.open.data.definitions.IPriceType;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ISecurityType;
import com.librade.spi.open.data.definitions.ISide;
import com.librade.spi.open.data.definitions.ITimeInForce;
import com.librade.spi.open.data.definitions.ITimeInterval;
import com.librade.spi.open.data.patterns.IPatternResultType;
import com.librade.spi.open.data.patterns.IPatternSubType;
import com.librade.spi.open.data.securityIdentifier.SecurityIdentifierMetaData;
import com.nordlicht.commons.base.definitions.IDefinitionProvider;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Various util methods.
 */
public interface IUtils {

    /**
     * Get metadata for SecurityIdentifier like limits and precision.
     * @param securityIdentifierId security identifier
     * @return metadata
     */
    default SecurityIdentifierMetaData getSecurityIdentifierMetaData(int securityIdentifierId) {
        return getSecurityIdentifierMetaData(getSecurityIdentifierById(securityIdentifierId));
    }

    /**
     * Get metadata for SecurityIdentifier like limits and precision.
     * @param securityIdentifier security identifier
     * @return metadata
     */
    SecurityIdentifierMetaData getSecurityIdentifierMetaData(ISecurityIdentifier securityIdentifier);

    /**
     *
     * @param pId security id
     * @return security identifier
     */
    ISecurityIdentifier getSecurityIdentifierById(int pId);

    /**
     *
     * @param name name
     * @return security identifier
     */
    ISecurityIdentifier getSecurityIdentifierByName(String name);

    /**
     *
     * @param pBroker broker
     * @param pSymbol symbol
     * @param pCurrency currency
     * @return security identifier
     */
    ISecurityIdentifier getSecurityIdentifier(IBroker pBroker, IAsset pSymbol, IAsset pCurrency);

    /**
     *
     * @param pBrokerName broker name
     * @param pSymbolName symbol name
     * @param pCurrencyName currency name
     * @return security identifier
     */
    ISecurityIdentifier getSecurityIdentifier(String pBrokerName, String pSymbolName, String pCurrencyName);

    ISecurityIdentifier getSecurityIdentifier(IBroker pBroker, IExchange exchange, IAsset pSymbol, IAsset pCurrency);

    ISecurityIdentifier getSecurityIdentifier(IExchange exchange, IAsset pSymbol, IAsset pCurrency);

    ISecurityIdentifier getSecurityIdentifier(String pBrokerName, String exchange, String pSymbolName, String pCurrencyName);

    /**
     *
     * @return unmodifiable list of all security itentifiers
     */
    List<ISecurityIdentifier> getAllSecurityIdentifiers();

    /**
     *
     * @return list of all security itentifiers of specific exchange
     */
    List<ISecurityIdentifier> getAllSecurityIdentifiers(IExchange exchange);

    /**
     *
     * @return list of all security itentifiers of specific broker
     */
    List<ISecurityIdentifier> getAllSecurityIdentifiers(IBroker broker);

    /**
     *
     * @param pName broker enum name
     * @return broker enum
     */
    IBroker getBrokerByName(String pName);

    /**
     *
     * @param pName exchange enum name
     * @return exchange enum
     */
    IExchange getExchangeByName(String pName);

    /**
     *
     * @param pName asset name
     * @return asset enum
     */
    IAsset getAssetByName(String pName);

    /**
     *
     * @param pName name
     * @return time in force enum
     */
    ITimeInForce getTimeInForceByName(String pName);

    /**
     *
     * @param pName name
     * @return side
     */
    ISide getSideByName(String pName);

    /**
     *
     * @param pName name
     * @return custom event type
     */
    ICustomEventType getCustomEventTypeByName(String pName);

    /**
     *
     * @param pName name
     * @return event type
     */
    IEventType getEventTypeByName(String pName);

    /**
     *
     * @param pName input type name
     * @return input type
     */
    IInputType getInputTypeByName(String pName);

    /**
     *
     * @param pName order type name
     * @return order type enum
     */
    IOrderType getOrderTypeByName(String pName);

    /**
     *
     * @param pName order type name
     * @return order type enum
     */
    IOptionType getOptionTypeByName(String pName);

    /**
     *
     * @return order type enum
     */
    List<IOptionType> getAllOptionTypes();

    /**
     *
     * @param pName account property name
     * @return account property
     */
    IAccountPropertyType getAccountPropertyByName(String pName);

    /**
     *
     * @param pName data storage name
     * @return data storage type by name
     */
    IDataStorageType getDataStorageTypeByName(String pName);

    /**
     *
     * @param securityTypeName security type name
     * @return security type enum
     */
    ISecurityType getSecurityTypeByName(String securityTypeName);

    /**
     *
     * @param priceTypeName price type name
     * @return price type
     */
    IPriceType getPriceTypeByName(String priceTypeName);

    /**
     *
     * @param analyticsTypeId analytics type id
     * @return analytics type
     */
    IAnalyticsType getAnalyticsTypeById(int analyticsTypeId);

    /**
     *
     * @param analyticsTypeName analytics type name
     * @return analytics type
     */
    IAnalyticsType getAnalyticsTypeByName(String analyticsTypeName);

    /**
     *
     * @param pTimeStamp timestamp
     * @param pEventType event type
     * @param pPriceType price type
     * @param pSecurityId security id
     * @param pVolume volume
     * @param pPrice price
     * @return market data
     */
    IMarketData createMarketData(long pTimeStamp, IEventType pEventType, IPriceType pPriceType, ISecurityIdentifier pSecurityId, BigDecimal pVolume,
                                 BigDecimal pPrice);

    /**
     *
     * @return definition provider that provides access to all the system definitions
     */
    IDefinitionProvider definitionProvider();

    /**
     *
     * @param type pattern type as string
     * @return pattern type definition
     */
    default IPatternSubType getPatternSubTypeByName(String type) {
        return definitionProvider().getDefinition(IPatternSubType.class, String.valueOf(type));
    }

    /**
     *
     * @param type pattern result type
     * @return enum of pattern result type
     */
    default IPatternResultType getPatternResultTypeByName(String type) {
        return definitionProvider().getDefinition(IPatternResultType.class, String.valueOf(type));
    }

    /**
     *
     * @param timeInterval time interval in string
     * @return pattern type definition
     */
    default ITimeInterval getTimeInterval(String timeInterval) {
        return definitionProvider().getDefinition(ITimeInterval.class, String.valueOf(timeInterval));
    }

    /**
     *
     * @param accountPropertyType account property type as string
     * @return account property type as definition
     */
    default IAccountPropertyType getAccountPropertyType(String accountPropertyType) {
        return definitionProvider().getDefinition(IAccountPropertyType.class, accountPropertyType);
    }

    /**
     * If order is marked with position id during the order creation through OrderBuilder, Execution reports
     * that belongs to that position should have the same postion id.
     * @param executionReport execution report
     * @return if present, position id
     */
    default Optional<Integer> getPositionIdOfExecutionReport(IExecutionReport executionReport) {
        if (executionReport == null
                || executionReport.getClOrdId() == null
                || !executionReport.getClOrdId().contains("-")) {
            return Optional.empty();
        }
        String[] uidAndId = executionReport.getClOrdId().split("-", 2);
        try {
            return Optional.ofNullable(new BigDecimal(uidAndId[1]).intValue());
        } catch (Exception ex) {
            return Optional.empty();
        }
    }
}






