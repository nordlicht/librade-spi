package com.librade.spi.open;

import org.mockito.Mockito;

/**
 * @author Tomas Fecko
 */
public class EventMockProvider extends AMockProviderParent {

    /**
     *
     * @return
     */
    public IEvent createDefaultMock() {
        return Mockito.mock(IEvent.class);
    }
}






