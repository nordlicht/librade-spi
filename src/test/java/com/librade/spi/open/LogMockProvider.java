package com.librade.spi.open;

import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Tomas Fecko
 */
public class LogMockProvider extends AMockProviderParent {

    /**
     *
     * @return
     */
    public ILog createDefaultMock() {
        ILog mock = Mockito.mock(ILog.class);
        Answer<Void> answer = new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock pInvocation) throws Throwable {

                System.out.println(appendString(new StringBuilder(), pInvocation.getArguments()).toString());
                return null;
            }

            /**
             *
             * @param pSB builder
             * @param pArray array of elements to print
             * @return StringBuilder
             */
            private StringBuilder appendString(StringBuilder pSB, Object... pArray) {
                if (pArray == null || pArray.length == 0) {
                    return pSB;
                }
                for (Object o : pArray) {
                    if (o != null && o.getClass().isArray()) {
                        return appendString(pSB, o);
                    } else {
                        if (o instanceof Throwable) {
                            ((Throwable) o).printStackTrace();
                        }
                        return pSB.append(o);
                    }
                }
                return pSB;
            }
        };
        doAnswer(answer).when(mock).trace(any());
        doAnswer(answer).when(mock).debug(any());
        doAnswer(answer).when(mock).info(any());
        doAnswer(answer).when(mock).warn(any());
        doAnswer(answer).when(mock).error(any());
        return mock;
    }
}
