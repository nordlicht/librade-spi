package com.librade.spi.open;

import org.mockito.Mockito;

/**
 * @author Tomas Fecko
 */
public class HistoricalDataMockProvider extends AMockProviderParent {


    /**
     *
     * @return
     */
    public IHistoricalData createDefaultMock() {
        return Mockito.mock(IHistoricalData.class);
    }
}






