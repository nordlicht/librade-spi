package com.librade.spi.open;

import org.mockito.Mockito;

/**
 * @author Stanislav Fujdiar
 */
public class DataTransformMockProvider extends AMockProviderParent {

    /**
     *
     * @return
     */
    public IDataTransform createDefaultMock() {
        return Mockito.mock(IDataTransform.class);
    }

}
