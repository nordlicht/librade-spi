package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IAccountPropertyType;
import com.librade.spi.open.data.definitions.IMsgType;
import com.nordlicht.ITimeSeriesData;
import com.nordlicht.commons.base.cep.ICEPEvent;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

/**
 * @author Tomas Fecko
 */
public class DataMockProvider {

    /**
     *
     * @param pCustomAccountId
     * @param pMsgSeqNumber
     * @param pMsgType
     * @param pPropertyValues
     * @param pSendingTime
     * @param pSessionId
     * @param pTimestamp
     * @return
     */
    public static IAccountState createAccountStateMock(Map<IAccountPropertyType, BigDecimal> pPropertyValues, String pCustomAccountId, int pMsgSeqNumber, IMsgType pMsgType,
                                                       long pSendingTime, String pSessionId, long pTimestamp) {
        IAccountState mock = mock(IAccountState.class);
        doAnswer(invocation -> pPropertyValues.get(invocation.getArgument(0, IAccountPropertyType.class))).when(mock).getPropertyValue(any());
        mockBrokerMessage(mock, pCustomAccountId, pMsgSeqNumber, pMsgType, pSendingTime, pSessionId, pTimestamp);
        return mock;
    }


    /**
     *
     * @param pClrOrdId clOrdId
     * @return
     */
    public static IExecutionReport createExecutionReport(String pClrOrdId, String pCustomAccountId, int pMsgSeqNumber, IMsgType pMsgType,
                                                         long pSendingTime, String pSessionId, long pTimestamp, BigDecimal pAvgPrice) {
        IExecutionReport mock = mock(IExecutionReport.class);
        mockBrokerMessage(mock, pCustomAccountId, pMsgSeqNumber, pMsgType, pSendingTime, pSessionId, pTimestamp);

        doAnswer(invocation -> pClrOrdId).when(mock).getClOrdId();
        doAnswer(invocation -> pAvgPrice).when(mock).getAvgPrice();
        return mock;
    }

    /**
     *
     * @param pCurrentTimestamp
     * @param pInputStream
     * @param pRemoveStream
     * @param pNextTimestamp
     * @param pQureyId
     * @return
     */
    public static ICEPEvent createCEPEventMock(long pCurrentTimestamp, List<LinkedHashMap<String, Object>> pInputStream,
                                               List<LinkedHashMap<String, Object>> pRemoveStream, long pNextTimestamp,
                                               String pQureyId) {
        ICEPEvent mock = mock(ICEPEvent.class);
        doAnswer(invocation -> pCurrentTimestamp).when(mock).getCurrentTimestamp();
        doAnswer(invocation -> pInputStream).when(mock).getInputStreamOfObjectsFromCEP();
        doAnswer(invocation -> pRemoveStream).when(mock).getRemoveStreamOfObjectsFromCEP();
        doAnswer(invocation -> pNextTimestamp).when(mock).getNextTimestamp();
        doAnswer(invocation -> pQureyId).when(mock).getQueryId();

        EventWrapper wrapper = new EventWrapper(mock);
        doAnswer(invocation -> pInputStream == null || pInputStream.isEmpty()).when(mock).isInsertStreamEmpty();
        doAnswer(invocation -> pRemoveStream == null || pRemoveStream.isEmpty()).when(mock).isRemoveStreamEmpty();
        return mock;
    }

    /**
     *
     * @param pBrokerMessageMock
     * @param pCustomAccountId
     * @param pMsgSeqNumber
     * @param pMsgType
     * @param pSendingTime
     * @param pSessionId
     * @param pTimestamp
     */
    private static void mockBrokerMessage(IBrokerMessage pBrokerMessageMock, String pCustomAccountId, int pMsgSeqNumber, IMsgType pMsgType,
                                   long pSendingTime, String pSessionId, long pTimestamp) {
        doAnswer(invocation -> pCustomAccountId).when(pBrokerMessageMock).getCustomAccountID();
        doAnswer(invocation -> pMsgSeqNumber).when(pBrokerMessageMock).getMsgSeqNum();
        doAnswer(invocation -> pMsgType.getName()).when(pBrokerMessageMock).getMsgType();
        doAnswer(invocation -> pMsgType).when(pBrokerMessageMock).getMsgTypeEnum();
        doAnswer(invocation -> pMsgType.getId()).when(pBrokerMessageMock).getMsgTypeId();
        doAnswer(invocation -> pSendingTime).when(pBrokerMessageMock).getSendingTime();
        doAnswer(invocation -> pSessionId).when(pBrokerMessageMock).getSessionId();
        mockTimeSeries(pBrokerMessageMock, pTimestamp);
    }

    /**
     *
     * @param pData data timestamp
     * @param pTimestamp
     */
    private static void mockTimeSeries(ITimeSeriesData pData, long pTimestamp) {
        doAnswer(invocation -> pTimestamp).when(pData).getTimestamp();
    }
}






