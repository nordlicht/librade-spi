package com.librade.spi.open.data;

import com.librade.spi.open.data.definitions.IAsset;
import com.librade.spi.open.data.definitions.IBroker;
import com.librade.spi.open.data.definitions.IExchange;
import com.librade.spi.open.data.definitions.IMsgType;
import com.librade.spi.open.data.definitions.IOrderType;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ISide;
import com.librade.spi.open.data.definitions.ITimeInForce;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

/**
 * @author Tomas Fecko
 */
public class DefinitionMocks {

    private static final List<IOrderType> ORDER_TYPES = new ArrayList<>();
    public static final IOrderType ORDER_TYPE_UNSPECIFIED = createOrderType("UNSPECIFIED", 0);
    public static final IOrderType ORDER_TYPE_MARKET = createOrderType("MARKET", 1);
    public static final IOrderType ORDER_TYPE_LIMIT = createOrderType("LIMIT", 2);
    public static final IOrderType ORDER_TYPE_STOP = createOrderType("STOP", 3);
    public static final IOrderType ORDER_TYPE_STOP_LIMIT = createOrderType("STOP_LIMIT", 4);
    public static final IOrderType ORDER_TYPE_MARKET_ON_CLOSE = createOrderType("MARKET_ON_CLOSE", 5);
    public static final IOrderType ORDER_TYPE_WITH_OR_WITHOUT = createOrderType("WITH_OR_WITHOUT", 6);
    public static final IOrderType ORDER_TYPE_LIMIT_OR_BETTER = createOrderType("LIMIT_OR_BETTER", 7);
    public static final IOrderType ORDER_TYPE_LIMIT_WITH_OR_WITHOUT = createOrderType("LIMIT_WITH_OR_WITHOUT", 8);
    public static final IOrderType ORDER_TYPE_ON_BASIS = createOrderType("ON_BASIS", 9);
    public static final IOrderType ORDER_TYPE_ON_CLOSE = createOrderType("ON_CLOSE", 10);
    public static final IOrderType ORDER_TYPE_LIMIT_ON_CLOSE = createOrderType("LIMIT_ON_CLOSE", 11);
    public static final IOrderType ORDER_TYPE_FOREX_MARKET = createOrderType("FOREX_MARKET", 12);
    public static final IOrderType ORDER_TYPE_PREVIOUSLY_QUOTED = createOrderType("PREVIOUSLY_QUOTED", 13);
    public static final IOrderType ORDER_TYPE_PREVIOUSLY_INDICATED = createOrderType("PREVIOUSLY_INDICATED", 14);
    public static final IOrderType ORDER_TYPE_FOREX_LIMIT = createOrderType("FOREX_LIMIT", 15);
    public static final IOrderType ORDER_TYPE_FOREX_SWAP = createOrderType("FOREX_SWAP", 16);
    public static final IOrderType ORDER_TYPE_FOREX_PREVIOUSLY_QUOTED = createOrderType("FOREX_PREVIOUSLY_QUOTED", 17);
    public static final IOrderType ORDER_TYPE_FUNARI = createOrderType("FUNARI", 18);
    public static final IOrderType ORDER_TYPE_MIT = createOrderType("MIT", 19);
    public static final IOrderType ORDER_TYPE_MARKET_WITH_LEFTOVER_AS_LIMIT = createOrderType("MARKET_WITH_LEFTOVER_AS_LIMIT", 20);
    public static final IOrderType ORDER_TYPE_PREVIOUS_FUND_VALUATION_POINT = createOrderType("PREVIOUS_FUND_VALUATION_POINT", 21);
    public static final IOrderType ORDER_TYPE_NEXT_FUND_VALUATION_POINT = createOrderType("NEXT_FUND_VALUATION_POINT", 22);
    public static final IOrderType ORDER_TYPE_PEGGED = createOrderType("PEGGED", 23);
    public static final IOrderType ORDER_TYPE_COUNTER_ORDER_SELECTION = createOrderType("COUNTER_ORDER_SELECTION", 24);

    private static final List<IBroker> BROKERS = new ArrayList<>();
    public static final IBroker BROKER_UNSPECIFIED = createBroker("UNSPECIFIED", 0);
    public static final IBroker BROKER_BSIM = createBroker("BSIM", 1);
    public static final IBroker BROKER_OANDA = createBroker("OANDA", 2);
    public static final IBroker BROKER_HOTSPOT = createBroker("HOTSPOT", 3);
    public static final IBroker BROKER_ICAP = createBroker("ICAP", 4);
    public static final IBroker BROKER_INTERACTIVE_BROKERS = createBroker("INTERACTIVE_BROKERS", 5);
    public static final IBroker BROKER_LMAX = createBroker("LMAX", 6);
    public static final IBroker BROKER_ADVANCED_MARKETS = createBroker("ADVANCED_MARKETS", 7);
    public static final IBroker BROKER_FINOTEC = createBroker("FINOTEC", 8);
    public static final IBroker BROKER_GLOBAL_PRIME = createBroker("GLOBAL_PRIME", 12);

    private static final List<IExchange> EXCHANGES = new ArrayList<>();
    public static final IExchange EXCHANGE_UNSPECIFIED = createExchange("UNSPECIFIED", 0);
    public static final IExchange EXCHANGE_IDEALPRO = createExchange("IDEALPRO", 1);
    public static final IExchange EXCHANGE_IDEAL = createExchange("IDEAL", 2);
    public static final IExchange EXCHANGE_SMART = createExchange("SMART", 3);

    private static final List<ISide> SIDES = new ArrayList<>();
    public static final ISide SIDE_UNSPECIFIED = createSide("UNSPECIFIED", 0);
    public static final ISide SIDE_BUY = createSide("BUY", 1);
    public static final ISide SIDE_SELL = createSide("SELL", 2);

    private static final List<ITimeInForce> TIME_IN_FORCE = new ArrayList<>();
    public static final ITimeInForce TIME_IN_FORCE_UNSPECIFIED = createTimeInForce("UNSPECIFIED", 0);
    public static final ITimeInForce TIME_IN_FORCE_DAY = createTimeInForce("DAY", 1);
    public static final ITimeInForce TIME_IN_FORCE_GTC = createTimeInForce("GTC", 2);
    public static final ITimeInForce TIME_IN_FORCE_OPG = createTimeInForce("OPG", 3);
    public static final ITimeInForce TIME_IN_FORCE_IOC = createTimeInForce("IOC", 4);
    public static final ITimeInForce TIME_IN_FORCE_FOK = createTimeInForce("FOK", 5);
    public static final ITimeInForce TIME_IN_FORCE_GTX = createTimeInForce("GTX", 6);
    public static final ITimeInForce TIME_IN_FORCE_GTD = createTimeInForce("GTD", 7);
    public static final ITimeInForce TIME_IN_FORCE_ATC = createTimeInForce("ATC", 8);

    private static final List<IAsset> ASSETS = new ArrayList<>();
    public static final IAsset ASSET_UNSPECIFIED = createAsset("UNSPECIFIED", 0);
    public static final IAsset ASSET_AED = createAsset("AED", 1);
    public static final IAsset ASSET_AUD = createAsset("AUD", 2);
    public static final IAsset ASSET_BHD = createAsset("BHD", 3);
    public static final IAsset ASSET_BKT = createAsset("BKT", 4);
    public static final IAsset ASSET_CAD = createAsset("CAD", 5);
    public static final IAsset ASSET_CHF = createAsset("CHF", 6);
    public static final IAsset ASSET_CNH = createAsset("CNH", 7);
    public static final IAsset ASSET_CNY = createAsset("CNY", 8);
    public static final IAsset ASSET_CZK = createAsset("CZK", 9);
    public static final IAsset ASSET_DKK = createAsset("DKK", 10);
    public static final IAsset ASSET_DLR = createAsset("DLR", 11);
    public static final IAsset ASSET_EUQ = createAsset("EUQ", 12);
    public static final IAsset ASSET_EUR = createAsset("EUR", 13);
    public static final IAsset ASSET_GBP = createAsset("GBP", 14);
    public static final IAsset ASSET_HKD = createAsset("HKD", 15);
    public static final IAsset ASSET_HUF = createAsset("HUF", 16);
    public static final IAsset ASSET_ILS = createAsset("ILS", 17);
    public static final IAsset ASSET_INR = createAsset("INR", 18);
    public static final IAsset ASSET_JPY = createAsset("JPY", 19);
    public static final IAsset ASSET_KES = createAsset("KES", 20);
    public static final IAsset ASSET_KET = createAsset("KET", 21);
    public static final IAsset ASSET_MXN = createAsset("MXN", 22);
    public static final IAsset ASSET_NOK = createAsset("NOK", 23);
    public static final IAsset ASSET_NZD = createAsset("NZD", 24);
    public static final IAsset ASSET_PLN = createAsset("PLN", 25);
    public static final IAsset ASSET_RON = createAsset("RON", 26);
    public static final IAsset ASSET_RUB = createAsset("RUB", 27);
    public static final IAsset ASSET_SAR = createAsset("SAR", 28);
    public static final IAsset ASSET_SAU = createAsset("SAU", 29);
    public static final IAsset ASSET_SEK = createAsset("SEK", 30);
    public static final IAsset ASSET_SGD = createAsset("SGD", 31);
    public static final IAsset ASSET_THB = createAsset("THB", 32);
    public static final IAsset ASSET_TRY = createAsset("TRY", 33);
    public static final IAsset ASSET_TWD = createAsset("TWD", 34);
    public static final IAsset ASSET_USD = createAsset("USD", 35);
    public static final IAsset ASSET_USQ = createAsset("USQ", 36);
    public static final IAsset ASSET_XAG = createAsset("XAG", 37);
    public static final IAsset ASSET_XAU = createAsset("XAU", 38);
    public static final IAsset ASSET_XPD = createAsset("XPD", 39);
    public static final IAsset ASSET_XPT = createAsset("XPT", 40);
    public static final IAsset ASSET_ZAR = createAsset("ZAR", 41);
    public static final IAsset ASSET_F40 = createAsset("F40", 42);
    public static final IAsset ASSET_DE30 = createAsset("DE30", 43);
    public static final IAsset ASSET_HK40 = createAsset("HK40", 44);
    public static final IAsset ASSET_JP225 = createAsset("JP225", 45);
    public static final IAsset ASSET_UK100 = createAsset("UK100", 46);
    public static final IAsset ASSET_US500 = createAsset("US500", 47);
    public static final IAsset ASSET_USTEC = createAsset("USTEC", 48);
    public static final IAsset ASSET_US30 = createAsset("US30", 49);
    public static final IAsset ASSET_BRENT = createAsset("BRENT", 50);
    public static final IAsset ASSET_WTI = createAsset("WTI", 51);
    public static final IAsset ASSET_AUS200 = createAsset("AUS200", 52);
    public static final IAsset ASSET_STOXX50 = createAsset("STOXX50", 53);
    public static final IAsset ASSET_IT40 = createAsset("IT40", 54);
    public static final IAsset ASSET_ES35 = createAsset("ES35", 55);
    public static final IAsset ASSET_US2000 = createAsset("US2000", 56);
    public static final IAsset ASSET_KRW = createAsset("KRW", 57);
    public static final IAsset ASSET_XVN = createAsset("XVN", 58);
    public static final IAsset ASSET_GDAXI = createAsset("GDAXI", 59);
    public static final IAsset ASSET_GDAXIm = createAsset("GDAXIm", 60);
    public static final IAsset ASSET_BTC = createAsset("BTC", 61);
    public static final IAsset ASSET_LTC = createAsset("LTC", 62);
    public static final IAsset ASSET_NMC = createAsset("NMC", 63);
    public static final IAsset ASSET_NVC = createAsset("NVC", 64);
    public static final IAsset ASSET_PPC = createAsset("PPC", 65);
    public static final IAsset ASSET_XBT = createAsset("XBT", 66);
    public static final IAsset ASSET_STR = createAsset("STR", 67);
    public static final IAsset ASSET_DOG = createAsset("DOG", 68);
    public static final IAsset ASSET_XRP = createAsset("XRP", 69);
    public static final IAsset ASSET_ZCA = createAsset("ZCA", 70);
    public static final IAsset ASSET_ZEU = createAsset("ZEU", 71);
    public static final IAsset ASSET_ZGB = createAsset("ZGB", 72);
    public static final IAsset ASSET_ZJP = createAsset("ZJP", 73);
    public static final IAsset ASSET_ZUS = createAsset("ZUS", 74);
    public static final IAsset ASSET_ETH = createAsset("ETH", 75);
    public static final IAsset ASSET_BRL = createAsset("BRL", 76);
    public static final IAsset ASSET_XBR = createAsset("XBR", 77);
    public static final IAsset ASSET_XTI = createAsset("XTI", 78);
    public static final IAsset ASSET_XNG = createAsset("XNG", 79);
    public static final IAsset ASSET_SPX = createAsset("SPX", 80);
    public static final IAsset ASSET_SPXm = createAsset("SPXm", 81);
    public static final IAsset ASSET_NDX = createAsset("NDX", 82);
    public static final IAsset ASSET_NDXm = createAsset("NDXm", 83);
    public static final IAsset ASSET_WS30 = createAsset("WS30", 84);
    public static final IAsset ASSET_WS30m = createAsset("WS30m", 85);

    private static final List<ISecurityIdentifier> SECURITY_IDENTIFIERS = new ArrayList<>();
    public static final ISecurityIdentifier SECURITY_IDENTIFIER_UNSPECIFIED = createSecurityIdentifier("UNSPECIFIED", 0, BROKER_UNSPECIFIED, ASSET_UNSPECIFIED, ASSET_UNSPECIFIED, EXCHANGE_UNSPECIFIED);
    public static final ISecurityIdentifier SECURITY_IDENTIFIER_EUR_USD_4 = createSecurityIdentifier("EUR_USD_4", 4006, BROKER_LMAX, ASSET_EUR, ASSET_USD, EXCHANGE_UNSPECIFIED);
    public static final ISecurityIdentifier SECURITY_IDENTIFIER_AUD_CAD_4 = createSecurityIdentifier("AUD_CAD_4", 4053, BROKER_LMAX, ASSET_AUD, ASSET_CAD, EXCHANGE_UNSPECIFIED);

    private static final List<IMsgType> MSG_TYPES = new ArrayList<>();
    public static final IMsgType MSG_TYPE_UNSPECIFIED = createMsgType("UNSPECIFIED", 0);
    public static final IMsgType MSG_TYPE_Heartbeat = createMsgType("Heartbeat", 1);
    public static final IMsgType MSG_TYPE_TestRequest = createMsgType("TestRequest", 2);
    public static final IMsgType MSG_TYPE_ResendRequest = createMsgType("ResendRequest", 3);
    public static final IMsgType MSG_TYPE_Reject = createMsgType("Reject", 4);
    public static final IMsgType MSG_TYPE_SequenceReset = createMsgType("SequenceReset", 5);
    public static final IMsgType MSG_TYPE_Logout = createMsgType("Logout", 6);
    public static final IMsgType MSG_TYPE_IndicationOfInterest = createMsgType("IndicationOfInterest", 7);
    public static final IMsgType MSG_TYPE_Advertisement = createMsgType("Advertisement", 8);
    public static final IMsgType MSG_TYPE_ExecutionReport = createMsgType("ExecutionReport", 9);
    public static final IMsgType MSG_TYPE_OrderCancelReject = createMsgType("OrderCancelReject", 10);
    public static final IMsgType MSG_TYPE_QuoteStatusRequest = createMsgType("QuoteStatusRequest", 11);
    public static final IMsgType MSG_TYPE_Logon = createMsgType("Logon", 12);
    public static final IMsgType MSG_TYPE_DerivativeSecurityList = createMsgType("DerivativeSecurityList", 13);
    public static final IMsgType MSG_TYPE_NewOrderMultileg = createMsgType("NewOrderMultileg", 14);
    public static final IMsgType MSG_TYPE_MultilegOrderCancelReplace = createMsgType("MultilegOrderCancelReplace", 15);
    public static final IMsgType MSG_TYPE_TradeCaptureReportRequest = createMsgType("TradeCaptureReportRequest", 16);
    public static final IMsgType MSG_TYPE_TradeCaptureReport = createMsgType("TradeCaptureReport", 17);
    public static final IMsgType MSG_TYPE_OrderMassStatusRequest = createMsgType("OrderMassStatusRequest", 18);
    public static final IMsgType MSG_TYPE_QuoteRequestReject = createMsgType("QuoteRequestReject", 19);
    public static final IMsgType MSG_TYPE_RFQRequest = createMsgType("RFQRequest", 20);
    public static final IMsgType MSG_TYPE_QuoteStatusReport = createMsgType("QuoteStatusReport", 21);
    public static final IMsgType MSG_TYPE_QuoteResponse = createMsgType("QuoteResponse", 22);
    public static final IMsgType MSG_TYPE_Confirmation = createMsgType("Confirmation", 23);
    public static final IMsgType MSG_TYPE_PositionMaintenanceRequest = createMsgType("PositionMaintenanceRequest", 24);
    public static final IMsgType MSG_TYPE_PositionMaintenanceReport = createMsgType("PositionMaintenanceReport", 25);
    public static final IMsgType MSG_TYPE_RequestForPositions = createMsgType("RequestForPositions", 26);
    public static final IMsgType MSG_TYPE_RequestForPositionsAck = createMsgType("RequestForPositionsAck", 27);
    public static final IMsgType MSG_TYPE_PositionReport = createMsgType("PositionReport", 28);
    public static final IMsgType MSG_TYPE_TradeCaptureReportRequestAck = createMsgType("TradeCaptureReportRequestAck", 29);
    public static final IMsgType MSG_TYPE_TradeCaptureReportAck = createMsgType("TradeCaptureReportAck", 30);
    public static final IMsgType MSG_TYPE_AllocationReport = createMsgType("AllocationReport", 31);
    public static final IMsgType MSG_TYPE_AllocationReportAck = createMsgType("AllocationReportAck", 32);
    public static final IMsgType MSG_TYPE_ConfirmationAck = createMsgType("ConfirmationAck", 33);
    public static final IMsgType MSG_TYPE_SettlementInstructionRequest = createMsgType("SettlementInstructionRequest", 34);
    public static final IMsgType MSG_TYPE_AssignmentReport = createMsgType("AssignmentReport", 35);
    public static final IMsgType MSG_TYPE_CollateralRequest = createMsgType("CollateralRequest", 36);
    public static final IMsgType MSG_TYPE_CollateralAssignment = createMsgType("CollateralAssignment", 37);
    public static final IMsgType MSG_TYPE_CollateralResponse = createMsgType("CollateralResponse", 38);
    public static final IMsgType MSG_TYPE_News = createMsgType("News", 39);
    public static final IMsgType MSG_TYPE_MassQuoteAcknowledgement = createMsgType("MassQuoteAcknowledgement", 40);
    public static final IMsgType MSG_TYPE_CollateralReport = createMsgType("CollateralReport", 41);
    public static final IMsgType MSG_TYPE_CollateralInquiry = createMsgType("CollateralInquiry", 42);
    public static final IMsgType MSG_TYPE_NetworkCounterpartySystemStatusRequest = createMsgType("NetworkCounterpartySystemStatusRequest", 43);
    public static final IMsgType MSG_TYPE_NetworkCounterpartySystemStatusResponse = createMsgType("NetworkCounterpartySystemStatusResponse", 44);
    public static final IMsgType MSG_TYPE_UserRequest = createMsgType("UserRequest", 45);
    public static final IMsgType MSG_TYPE_UserResponse = createMsgType("UserResponse", 46);
    public static final IMsgType MSG_TYPE_CollateralInquiryAck = createMsgType("CollateralInquiryAck", 47);
    public static final IMsgType MSG_TYPE_ConfirmationRequest = createMsgType("ConfirmationRequest", 48);
    public static final IMsgType MSG_TYPE_Email = createMsgType("Email", 49);
    public static final IMsgType MSG_TYPE_SecurityDefinitionRequest = createMsgType("SecurityDefinitionRequest", 50);
    public static final IMsgType MSG_TYPE_SecurityDefinition = createMsgType("SecurityDefinition", 51);
    public static final IMsgType MSG_TYPE_NewOrderSingle = createMsgType("NewOrderSingle", 52);
    public static final IMsgType MSG_TYPE_SecurityStatusRequest = createMsgType("SecurityStatusRequest", 53);
    public static final IMsgType MSG_TYPE_NewOrderList = createMsgType("NewOrderList", 54);
    public static final IMsgType MSG_TYPE_OrderCancelRequest = createMsgType("OrderCancelRequest", 55);
    public static final IMsgType MSG_TYPE_SecurityStatus = createMsgType("SecurityStatus", 56);
    public static final IMsgType MSG_TYPE_OrderCancelReplaceRequest = createMsgType("OrderCancelReplaceRequest", 57);
    public static final IMsgType MSG_TYPE_TradingSessionStatusRequest = createMsgType("TradingSessionStatusRequest", 58);
    public static final IMsgType MSG_TYPE_OrderStatusRequest = createMsgType("OrderStatusRequest", 59);
    public static final IMsgType MSG_TYPE_TradingSessionStatus = createMsgType("TradingSessionStatus", 60);
    public static final IMsgType MSG_TYPE_MassQuote = createMsgType("MassQuote", 61);
    public static final IMsgType MSG_TYPE_BusinessMessageReject = createMsgType("BusinessMessageReject", 62);
    public static final IMsgType MSG_TYPE_AllocationInstruction = createMsgType("AllocationInstruction", 63);
    public static final IMsgType MSG_TYPE_BidRequest = createMsgType("BidRequest", 64);
    public static final IMsgType MSG_TYPE_ListCancelRequest = createMsgType("ListCancelRequest", 65);
    public static final IMsgType MSG_TYPE_BidResponse = createMsgType("BidResponse", 66);
    public static final IMsgType MSG_TYPE_ListExecute = createMsgType("ListExecute", 67);
    public static final IMsgType MSG_TYPE_ListStrikePrice = createMsgType("ListStrikePrice", 68);
    public static final IMsgType MSG_TYPE_ListStatusRequest = createMsgType("ListStatusRequest", 69);
    public static final IMsgType MSG_TYPE_XMLmessage = createMsgType("XMLmessage", 70);
    public static final IMsgType MSG_TYPE_ListStatus = createMsgType("ListStatus", 71);
    public static final IMsgType MSG_TYPE_RegistrationInstructions = createMsgType("RegistrationInstructions", 72);
    public static final IMsgType MSG_TYPE_RegistrationInstructionsResponse = createMsgType("RegistrationInstructionsResponse", 73);
    public static final IMsgType MSG_TYPE_AllocationInstructionAck = createMsgType("AllocationInstructionAck", 74);
    public static final IMsgType MSG_TYPE_OrderMassCancelRequest = createMsgType("OrderMassCancelRequest", 75);
    public static final IMsgType MSG_TYPE_DontKnowTrade = createMsgType("DontKnowTrade", 76);
    public static final IMsgType MSG_TYPE_QuoteRequest = createMsgType("QuoteRequest", 77);
    public static final IMsgType MSG_TYPE_OrderMassCancelReport = createMsgType("OrderMassCancelReport", 78);
    public static final IMsgType MSG_TYPE_Quote = createMsgType("Quote", 79);
    public static final IMsgType MSG_TYPE_NewOrderCross = createMsgType("NewOrderCross", 80);
    public static final IMsgType MSG_TYPE_SettlementInstructions = createMsgType("SettlementInstructions", 81);
    public static final IMsgType MSG_TYPE_CrossOrderCancelReplaceRequest = createMsgType("CrossOrderCancelReplaceRequest", 82);
    public static final IMsgType MSG_TYPE_CrossOrderCancelRequest = createMsgType("CrossOrderCancelRequest", 83);
    public static final IMsgType MSG_TYPE_MarketDataRequest = createMsgType("MarketDataRequest", 84);
    public static final IMsgType MSG_TYPE_SecurityTypeRequest = createMsgType("SecurityTypeRequest", 85);
    public static final IMsgType MSG_TYPE_SecurityTypes = createMsgType("SecurityTypes", 86);
    public static final IMsgType MSG_TYPE_MarketDataSnapshotFullRefresh = createMsgType("MarketDataSnapshotFullRefresh", 87);
    public static final IMsgType MSG_TYPE_SecurityListRequest = createMsgType("SecurityListRequest", 88);
    public static final IMsgType MSG_TYPE_MarketDataIncrementalRefresh = createMsgType("MarketDataIncrementalRefresh", 89);
    public static final IMsgType MSG_TYPE_MarketDataRequestReject = createMsgType("MarketDataRequestReject", 90);
    public static final IMsgType MSG_TYPE_SecurityList = createMsgType("SecurityList", 91);
    public static final IMsgType MSG_TYPE_QuoteCancel = createMsgType("QuoteCancel", 92);
    public static final IMsgType MSG_TYPE_DerivativeSecurityListRequest = createMsgType("DerivativeSecurityListRequest", 93);
    public static final IMsgType MSG_TYPE_ContraryIntentionReport = createMsgType("ContraryIntentionReport", 94);
    public static final IMsgType MSG_TYPE_SecurityDefinitionUpdateReport = createMsgType("SecurityDefinitionUpdateReport", 95);
    public static final IMsgType MSG_TYPE_SecurityListUpdateReport = createMsgType("SecurityListUpdateReport", 96);
    public static final IMsgType MSG_TYPE_AdjustedPositionReport = createMsgType("AdjustedPositionReport", 97);
    public static final IMsgType MSG_TYPE_AllocationInstructionAlert = createMsgType("AllocationInstructionAlert", 98);
    public static final IMsgType MSG_TYPE_ExecutionAcknowledgement = createMsgType("ExecutionAcknowledgement", 99);
    public static final IMsgType MSG_TYPE_TradingSessionList = createMsgType("TradingSessionList", 100);
    public static final IMsgType MSG_TYPE_TradingSessionListRequest = createMsgType("TradingSessionListRequest", 101);



    /**
     *
     * @param pName
     * @param pId
     * @return
     */
    private static IMsgType createMsgType(String pName, int pId) {
        IMsgType mock = mock(IMsgType.class);
        doAnswer(invocation -> pId).when(mock).getId();
        doAnswer(invocation -> pName).when(mock).getName();
        MSG_TYPES.add(mock);
        return mock;
    }

    /**
     *
     * @param pName
     * @param pId
     * @return
     */
    private static ISide createSide(String pName, int pId) {
        ISide mock = mock(ISide.class);
        doAnswer(invocation -> pId).when(mock).getId();
        doAnswer(invocation -> pName).when(mock).getName();
        SIDES.add(mock);
        return mock;
    }

    /**
     *
     * @param pName
     * @param pId
     * @return
     */
    private static ITimeInForce createTimeInForce(String pName, int pId) {
        ITimeInForce mock = mock(ITimeInForce.class);
        doAnswer(invocation -> pId).when(mock).getId();
        doAnswer(invocation -> pName).when(mock).getName();
        TIME_IN_FORCE.add(mock);
        return mock;
    }

    /**
     *
     * @param pName
     * @param pId
     * @return
     */
    private static IAsset createAsset(String pName, int pId) {
        IAsset mock = mock(IAsset.class);
        doAnswer(invocation -> pId).when(mock).getId();
        doAnswer(invocation -> pName).when(mock).getName();
        ASSETS.add(mock);
        return mock;
    }

    /**
     *
     * @param pName
     * @param pId
     * @return
     */
    private static IBroker createBroker(String pName, int pId) {
        IBroker mock = mock(IBroker.class);
        doAnswer(invocation -> pId).when(mock).getId();
        doAnswer(invocation -> pName).when(mock).getName();
        BROKERS.add(mock);
        return mock;
    }

    /**
     *
     * @param pName
     * @param pId
     * @return
     */
    private static IExchange createExchange(String pName, int pId) {
        IExchange mock = mock(IExchange.class);
        doAnswer(invocation -> pId).when(mock).getId();
        doAnswer(invocation -> pName).when(mock).getName();
        EXCHANGES.add(mock);
        return mock;
    }

    /**
     *
     * @param pName
     * @param pId
     * @return
     */
    private static IOrderType createOrderType(String pName, int pId) {
        IOrderType mock = mock(IOrderType.class);
        doAnswer(invocation -> pId).when(mock).getId();
        doAnswer(invocation -> pName).when(mock).getName();
        ORDER_TYPES.add(mock);
        return mock;
    }


    /**
     *
     * @param pName
     * @param pId
     * @return
     */
    private static ISecurityIdentifier createSecurityIdentifier(String pName, int pId, IBroker pBroker, IAsset pSymbol, IAsset pCurrency, IExchange pExchange) {
        ISecurityIdentifier mock = mock(ISecurityIdentifier.class);
        doAnswer(invocation -> pId).when(mock).getId();
        doAnswer(invocation -> pName).when(mock).getName();
        doAnswer(invocation -> pBroker).when(mock).getBroker();
        doAnswer(invocation -> pCurrency).when(mock).getCurrency();
        doAnswer(invocation -> pExchange).when(mock).getExchange();
        doAnswer(invocation -> pSymbol).when(mock).getSymbol();
        SECURITY_IDENTIFIERS.add(mock);
        return mock;
    }
}





