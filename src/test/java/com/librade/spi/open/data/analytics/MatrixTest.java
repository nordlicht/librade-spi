package com.librade.spi.open.data.analytics;

import org.junit.Test;

/**
 * Created by StanoFujdiar on 14.09.2016.
 */
public class MatrixTest {
    @Test
    public void getValue() throws Exception {
        Matrix<Float> m = new FloatMatrix(new Float[]{
                0.0f,1.0f,2.0f,
                3.0f,4.0f,5.0f,
                6.0f,7.0f,8.0f,

                9.0f,10.0f,11.0f,
                12.0f,13.0f,14.0f,
                15.0f,16.0f,17.0f,

                18.0f,19.0f,20.0f,
                21.0f,22.0f,23.0f,
                24.0f,25.0f,26.0f}, 3, 3, 3);
        System.out.println(m.getValue(1,1,1));
    }
}








