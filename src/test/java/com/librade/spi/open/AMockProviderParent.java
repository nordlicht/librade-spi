package com.librade.spi.open;

import com.librade.spi.open.listeners.IGenericListener;

/**
 *
 * @author Tomas Fecko
 */
public abstract class AMockProviderParent {

    protected IGenericListener mGenericListener;

    public IGenericListener getGenericListener() {
        return mGenericListener;
    }

    public void setGenericListener(IGenericListener pGenericListener) {
        mGenericListener = pGenericListener;
    }
}





