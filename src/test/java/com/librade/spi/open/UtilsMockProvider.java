package com.librade.spi.open;

import com.librade.spi.open.data.definitions.IAsset;
import com.librade.spi.open.data.definitions.IBroker;
import com.librade.spi.open.data.definitions.IExchange;
import com.librade.spi.open.data.definitions.IOrderType;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ISide;
import com.librade.spi.open.data.definitions.ITimeInForce;
import com.nordlicht.IDefinition;

import org.junit.Test;
import org.mockito.Mockito;

import static com.librade.spi.open.data.definitions.IOrderType.COUNTER_ORDER_SELECTION;
import static com.librade.spi.open.data.definitions.IOrderType.FOREX_LIMIT;
import static com.librade.spi.open.data.definitions.IOrderType.FOREX_MARKET;
import static com.librade.spi.open.data.definitions.IOrderType.FOREX_PREVIOUSLY_QUOTED;
import static com.librade.spi.open.data.definitions.IOrderType.FOREX_SWAP;
import static com.librade.spi.open.data.definitions.IOrderType.FUNARI;
import static com.librade.spi.open.data.definitions.IOrderType.LIMIT;
import static com.librade.spi.open.data.definitions.IOrderType.LIMIT_ON_CLOSE;
import static com.librade.spi.open.data.definitions.IOrderType.LIMIT_OR_BETTER;
import static com.librade.spi.open.data.definitions.IOrderType.LIMIT_WITH_OR_WITHOUT;
import static com.librade.spi.open.data.definitions.IOrderType.MARKET;
import static com.librade.spi.open.data.definitions.IOrderType.MARKET_ON_CLOSE;
import static com.librade.spi.open.data.definitions.IOrderType.MARKET_WITH_LEFTOVER_AS_LIMIT;
import static com.librade.spi.open.data.definitions.IOrderType.MIT;
import static com.librade.spi.open.data.definitions.IOrderType.NEXT_FUND_VALUATION_POINT;
import static com.librade.spi.open.data.definitions.IOrderType.ON_BASIS;
import static com.librade.spi.open.data.definitions.IOrderType.ON_CLOSE;
import static com.librade.spi.open.data.definitions.IOrderType.PEGGED;
import static com.librade.spi.open.data.definitions.IOrderType.PREVIOUSLY_INDICATED;
import static com.librade.spi.open.data.definitions.IOrderType.PREVIOUSLY_QUOTED;
import static com.librade.spi.open.data.definitions.IOrderType.PREVIOUS_FUND_VALUATION_POINT;
import static com.librade.spi.open.data.definitions.IOrderType.STOP;
import static com.librade.spi.open.data.definitions.IOrderType.STOP_LIMIT;
import static com.librade.spi.open.data.definitions.IOrderType.WITH_OR_WITHOUT;
import static com.librade.spi.open.data.definitions.ISide.BUY;
import static com.librade.spi.open.data.definitions.ISide.SELL;
import static com.librade.spi.open.data.definitions.ITimeInForce.ATC;
import static com.librade.spi.open.data.definitions.ITimeInForce.DAY;
import static com.librade.spi.open.data.definitions.ITimeInForce.FOK;
import static com.librade.spi.open.data.definitions.ITimeInForce.GTC;
import static com.librade.spi.open.data.definitions.ITimeInForce.GTD;
import static com.librade.spi.open.data.definitions.ITimeInForce.GTX;
import static com.librade.spi.open.data.definitions.ITimeInForce.IOC;
import static com.librade.spi.open.data.definitions.ITimeInForce.OPG;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * @author Tomas Fecko
 */
public class UtilsMockProvider extends AMockProviderParent {


    /**
     * @throws Exception
     */
    @Test
    public void testName() throws Exception {
        IUtils mock = createDefaultMock();

        assertNotNull(mock.getAssetByName("UNSPECIFIED"));
        assertNotNull(mock.getBrokerByName("UNSPECIFIED"));
        assertNotNull(mock.getExchangeByName("UNSPECIFIED"));
        ISecurityIdentifier securityIdentifierById = mock.getSecurityIdentifierById(4006);
        assertNotNull(securityIdentifierById);
        assertEquals("LMAX", securityIdentifierById.getBroker().getName());
        assertEquals("USD", securityIdentifierById.getCurrency().getName());
        assertEquals("UNSPECIFIED", securityIdentifierById.getExchange().getName());
        assertEquals(4006, securityIdentifierById.getId());
        assertEquals("EUR", securityIdentifierById.getSymbol().getName());
    }

    /**
     *
     * @param pUtilsMock utils mock
     * @param pBroker broker name
     * @param pExchange exchange name
     * @param pSymbol symbol name
     * @param pCurrency currency name
     * @param pId id of security
     * @return utils mock
     */
    public IUtils addValuesToMock(IUtils pUtilsMock, String pBroker, String pExchange, String pSymbol, String pCurrency, int pId) {
        if (pUtilsMock == null) {
            pUtilsMock = Mockito.mock(IUtils.class);
        }
        IBroker broker = createDefinitionMocks(pUtilsMock, IBroker.class, pBroker);
        IExchange exchange = createDefinitionMocks(pUtilsMock, IExchange.class, pExchange);
        IAsset symbol = createDefinitionMocks(pUtilsMock, IAsset.class, pSymbol);
        IAsset currency = createDefinitionMocks(pUtilsMock, IAsset.class, pCurrency);
        createSecurityIdentifierMocks(pUtilsMock, broker, currency, exchange, symbol, pId);
        return pUtilsMock;
    }

    public IUtils createDefaultMock() {
        IUtils mock = addValuesToMock(null, "UNSPECIFIED", "UNSPECIFIED", "UNSPECIFIED", "UNSPECIFIED", 0);
        addValuesToMock(mock, "INTERACTIVE_BROKERS", "IDEALPRO", "EUR", "USD", 7046);
        addValuesToMock(mock, "LMAX", "UNSPECIFIED", "EUR", "USD", 4006);
        createDefinitionMocks(mock, ITimeInForce.class, DAY, GTC, OPG, IOC, FOK, GTX, GTD, ATC);
        createDefinitionMocks(mock, ISide.class, BUY, SELL);
        createDefinitionMocks(mock, IOrderType.class, MARKET, LIMIT, STOP, STOP_LIMIT, MARKET_ON_CLOSE, WITH_OR_WITHOUT, LIMIT_OR_BETTER,
                LIMIT_WITH_OR_WITHOUT, ON_BASIS, ON_CLOSE, LIMIT_ON_CLOSE, FOREX_MARKET, PREVIOUSLY_QUOTED, PREVIOUSLY_INDICATED,
                FOREX_LIMIT, FOREX_SWAP, FOREX_PREVIOUSLY_QUOTED, FUNARI, MIT, MARKET_WITH_LEFTOVER_AS_LIMIT, PREVIOUS_FUND_VALUATION_POINT,
                NEXT_FUND_VALUATION_POINT, PEGGED, COUNTER_ORDER_SELECTION);
        return mock;
    }

    /**
     *
     * @param pName
     * @return
     */
    private ISecurityIdentifier createSecurityIdentifierMocks(IUtils pMock, IBroker pBroker, IAsset pCurrency, IExchange pExchange,
                                                                           IAsset pSymbol, int... pSecurityIdentifierIds) {
        ISecurityIdentifier mock = null;
        for (int id : pSecurityIdentifierIds) {
            mock = Mockito.mock(ISecurityIdentifier.class);
            when(pMock.getSecurityIdentifierById(id)).thenReturn(mock);
            when(mock.getBroker()).thenReturn(pBroker);
            when(mock.getCurrency()).thenReturn(pCurrency);
            when(mock.getExchange()).thenReturn(pExchange);
            when(mock.getId()).thenReturn(id);
            when(mock.getName()).thenReturn("SECURITY-" + id);
            when(mock.getSymbol()).thenReturn(pSymbol);
        }
        return mock;
    }

    /**
     *
     * @param pName
     * @return
     */
    <T extends IDefinition> T createDefinitionMocks(IUtils pMock, Class<T> pEnumClass, String... pName) {
        T mock = null;
        for (String name : pName) {
            mock = Mockito.mock(pEnumClass);
            when(((IDefinition<T>) mock).getName()).thenReturn(name);

            if (pEnumClass.equals(IBroker.class)) {
                when(pMock.getBrokerByName(name)).thenReturn((IBroker) mock);
            } else if (pEnumClass.equals(IExchange.class)) {
                when(pMock.getExchangeByName(name)).thenReturn((IExchange) mock);
            } else if (pEnumClass.equals(IAsset.class)) {
                when(pMock.getAssetByName(name)).thenReturn((IAsset) mock);
            } else if (pEnumClass.equals(ISide.class)) {
                when(pMock.getSideByName(name)).thenReturn((ISide) mock);
            } else if (pEnumClass.equals(ITimeInForce.class)) {
                when(pMock.getTimeInForceByName(name)).thenReturn((ITimeInForce) mock);
            } else if (pEnumClass.equals(IOrderType.class)) {
                when(pMock.getOrderTypeByName(name)).thenReturn((IOrderType) mock);
            }
        }
        return mock;
    }
}





