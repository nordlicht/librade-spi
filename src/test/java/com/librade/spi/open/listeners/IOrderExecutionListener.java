package com.librade.spi.open.listeners;

import com.librade.spi.open.data.IClientToBrokerMessage;

/**
 * @author Tomas Fecko
 */
public interface IOrderExecutionListener {

    /**
     *
     * @param pOrder order
     */
    void onOrder(IClientToBrokerMessage pOrder);
}





