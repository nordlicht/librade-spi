package com.librade.spi.open.listeners;

/**
 * @author Tomas Fecko
 */
@FunctionalInterface
public interface IGenericListener {

    /**
     *
     * @param pMethodName method name
     * @param pMethodParameters method parameters
     */
    void onMethodCall(String pMethodName, Object[] pMethodParameters);

}




