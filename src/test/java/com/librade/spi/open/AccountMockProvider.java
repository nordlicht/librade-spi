package com.librade.spi.open;

import com.librade.spi.open.data.definitions.IBroker;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * @author Stanislav Fujdiar
 */
public class AccountMockProvider extends AMockProviderParent {

    /**
     *
     * @return
     */
    public IAccount createDefaultMock() throws Exception {
        IAccount mock = Mockito.mock(IAccount.class);
        doAnswer(requestAccountState()).when(mock).requestAccountState(anyString());
        when(mock.createVirtualAccount(anyString(), any(IBroker.class), anyList())).thenReturn(true);
        doNothing().when(mock).deleteVirtualAccount(anyString());
        return mock;
    }

    /**
     *
     * @return account state requested
     */
    private Answer<Void> requestAccountState() {
        return new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock pInvocation) throws Throwable {
                return null;
            }
        };
    }
}
