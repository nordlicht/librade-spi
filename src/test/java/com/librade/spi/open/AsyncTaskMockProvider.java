package com.librade.spi.open;

import org.mockito.Mockito;

/**
 * @author Tomas Fecko
 */
public class AsyncTaskMockProvider extends AMockProviderParent {

    /**
     *
     * @return
     */
    public IAsyncTask createDefaultMock() {
        return Mockito.mock(IAsyncTask.class);
    }
}






