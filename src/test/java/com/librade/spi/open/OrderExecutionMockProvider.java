package com.librade.spi.open;

import com.librade.spi.open.data.DefinitionMocks;
import com.librade.spi.open.data.IClientToBrokerMessage;
import com.librade.spi.open.data.INewOrderSingle;
import com.librade.spi.open.data.IOrderCancelRequest;
import com.librade.spi.open.data.IOrderStatusRequest;
import com.librade.spi.open.data.builders.IOrderBuilder;
import com.librade.spi.open.data.definitions.IMsgType;
import com.librade.spi.open.data.definitions.IOrderType;
import com.librade.spi.open.data.definitions.ISecurityIdentifier;
import com.librade.spi.open.data.definitions.ISide;
import com.librade.spi.open.data.definitions.ITimeInForce;
import com.librade.spi.open.listeners.IOrderExecutionListener;
import com.librade.spi.utils.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Tomas Fecko
 */
public class OrderExecutionMockProvider extends AMockProviderParent {

    private final ExecutorService           mExecutorService;
    private static final AtomicInteger      mMsgSequenceNumber = new AtomicInteger(1);
    private IOrderExecutionListener         mListener;
    private ILibradeAPI                     libradeAPI;

    public void setLibradeAPI(ILibradeAPI libradeAPI) {
        this.libradeAPI = libradeAPI;
    }

    /**
     *
     * @param pExecutorService executor
     */
    public OrderExecutionMockProvider(ExecutorService pExecutorService) {
        mExecutorService = pExecutorService;
    }

    public IOrderExecutionListener getListener() {
        return mListener;
    }

    public void setListener(IOrderExecutionListener pListener) {
        mListener = pListener;
    }

    /**
     *
     * @return
     */
    public IOrderExecution createDefaultMock(String pSessionUID) throws Exception {
        List<IClientToBrokerMessage> orders = new ArrayList<>();

        IOrderExecution mock = mock(IOrderExecution.class);
        when(mock.createOrderBuilder()).thenAnswer(invocation -> createOrderBuilderMock(pSessionUID));

        doAnswer(invocation -> {
            IClientToBrokerMessage order = invocation.getArgument(0, IClientToBrokerMessage.class);
            orders.add(order);
            if (mListener != null) {
                mListener.onOrder(order);
            }
            if (getGenericListener() != null) {
                getGenericListener().onMethodCall("sendOrder", invocation.getArguments());
            }
            return order.getClOrdId();
        }).when(mock).sendOrder(any());

        doAnswer(invocation -> {
            List<String> uids = new ArrayList<String>();
            IClientToBrokerMessage[] ordersArray = invocation.getArgument(0, IClientToBrokerMessage[].class);
            for (IClientToBrokerMessage order : ordersArray) {
                uids.add(mock.sendOrder(order));
            }
            return uids;
        }).when(mock).sendOrders(any());

//        doAnswer(invocation -> null).when(mock).getExecutionReports();
        return mock;
    }

    /**
     *
     * @return
     */
    public IOrderBuilder createOrderBuilderMock(String pSessionUID) {
        // build can only be called once
        AtomicBoolean buildCalled = new AtomicBoolean();
        // set by the system
        AtomicLong timestamp = new AtomicLong(System.currentTimeMillis());
        AtomicLong sendingTime = new AtomicLong(timestamp.get());
        AtomicLong transactTime = new AtomicLong(timestamp.get());
        AtomicReference<IMsgType> msgType = new AtomicReference<>();
        AtomicReference<String> clrOrId = new AtomicReference<>(Utils.getUniqueString(18));
        AtomicInteger msgSeqNumber = new AtomicInteger(mMsgSequenceNumber.incrementAndGet());
        // set by the user
        AtomicReference<String> customAccountUID = new AtomicReference<>();
        AtomicReference<Long> expire = new AtomicReference<>();
        AtomicReference<IOrderType> orderType = new AtomicReference<>();
        AtomicReference<String> origClOrdId = new AtomicReference<>();
        AtomicReference<BigDecimal> price = new AtomicReference<>();
        AtomicReference<BigDecimal> quantity = new AtomicReference<>();
        AtomicReference<ISecurityIdentifier> securityIdentifier = new AtomicReference<>();
        AtomicReference<ISide> side = new AtomicReference<>();
        AtomicReference<ITimeInForce> timeInForce = new AtomicReference<>();
        IOrderBuilder orderBuilderMock = mock(IOrderBuilder.class);

        doAnswer(invocation -> {
            customAccountUID.set(invocation.getArgument(0, String.class));
            return orderBuilderMock;
        }).when(orderBuilderMock).customAccountUID(anyString());
        doAnswer(invocation -> {expire.set(invocation.getArgument(0, Long.class)); return orderBuilderMock;}).when(orderBuilderMock).expire(anyLong());
        doAnswer(invocation -> {expire.set(Utils.parseDateTime(invocation.getArgument(0, String.class), invocation.getArgument(1, String.class))); return orderBuilderMock;}).when(orderBuilderMock).expire(anyString(), anyString());
        doAnswer(invocation -> {orderType.set(invocation.getArgument(0, IOrderType.class)); return orderBuilderMock;}).when(orderBuilderMock).orderType(any(IOrderType.class));
        doAnswer(invocation -> {orderType.set(libradeAPI.utils().getOrderTypeByName(invocation.getArgument(0, String.class))); return orderBuilderMock;}).when(orderBuilderMock).orderType(any(String.class));

        doAnswer(invocation -> {origClOrdId.set(invocation.getArgument(0, String.class)); return orderBuilderMock;}).when(orderBuilderMock).origClOrdId(anyString());
        doAnswer(invocation -> {price.set(new BigDecimal(String.valueOf(invocation.getArgument(0, Number.class)))); return orderBuilderMock;}).when(orderBuilderMock).price(any());
        doAnswer(invocation -> {quantity.set(new BigDecimal(String.valueOf(invocation.getArgument(0, Number.class)))); return orderBuilderMock;}).when(orderBuilderMock).quantity(any());
        doAnswer(invocation -> {securityIdentifier.set(invocation.getArgument(0, ISecurityIdentifier.class)); return orderBuilderMock;}).when(orderBuilderMock).securityIdentifier(any(ISecurityIdentifier.class));
        doAnswer(invocation -> {side.set(invocation.getArgument(0, ISide.class)); return orderBuilderMock;}).when(orderBuilderMock).side(any(ISide.class));
        doAnswer(invocation -> {timeInForce.set(invocation.getArgument(0, ITimeInForce.class)); return orderBuilderMock;}).when(orderBuilderMock).timeInForce(any(ITimeInForce.class));

        // BUILD ORDERS
        INewOrderSingle newOrderSingleMock = mock(INewOrderSingle.class);
        doAnswer(invocation -> clrOrId.get()).when(newOrderSingleMock).getClOrdId();
        doAnswer(invocation -> customAccountUID.get()).when(newOrderSingleMock).getCustomAccountID();
        doAnswer(invocation -> expire.get()).when(newOrderSingleMock).getExpire();
        doAnswer(invocation -> msgSeqNumber.get()).when(newOrderSingleMock).getMsgSeqNum();
        doAnswer(invocation -> IMsgType.NewOrderSingle).when(newOrderSingleMock).getMsgType();
        doAnswer(invocation -> DefinitionMocks.MSG_TYPE_NewOrderSingle).when(newOrderSingleMock).getMsgTypeEnum();
        doAnswer(invocation -> DefinitionMocks.MSG_TYPE_NewOrderSingle.getId()).when(newOrderSingleMock).getMsgTypeId();
        doAnswer(invocation -> orderType.get().getName()).when(newOrderSingleMock).getOrderType();
        doAnswer(invocation -> orderType.get()).when(newOrderSingleMock).getOrderTypeEnum();
        doAnswer(invocation -> orderType.get().getName()).when(newOrderSingleMock).getOrderTypeId();
        doAnswer(invocation -> price.get()).when(newOrderSingleMock).getPrice();
        doAnswer(invocation -> quantity.get()).when(newOrderSingleMock).getQuantity();
        doAnswer(invocation -> securityIdentifier.get().getId()).when(newOrderSingleMock).getSecurityId();
        doAnswer(invocation -> sendingTime.get()).when(newOrderSingleMock).getSendingTime();
        doAnswer(invocation -> pSessionUID).when(newOrderSingleMock).getSessionId();
        doAnswer(invocation -> side.get()).when(newOrderSingleMock).getSideEnum();
        doAnswer(invocation -> timeInForce.get().getName()).when(newOrderSingleMock).getTimeInForce();
        doAnswer(invocation -> timeInForce.get()).when(newOrderSingleMock).getTimeInForceEnum();
        doAnswer(invocation -> timeInForce.get().getId()).when(newOrderSingleMock).getTimeInForceId();
        doAnswer(invocation -> timestamp.get()).when(newOrderSingleMock).getTimestamp();
        doAnswer(invocation -> {
            if (!buildCalled.compareAndSet(false, true)) {
                throw new IllegalStateException("buildNewOrderSingle() on OrderBuilder can be called only once. Create new builder for every new created order.");
            }
            return newOrderSingleMock;
        }).when(orderBuilderMock).buildNewOrderSingle();

        IOrderCancelRequest orderCancelRequestMock = mock(IOrderCancelRequest.class);
        doAnswer(invocation -> clrOrId.get()).when(orderCancelRequestMock).getClOrdId();
        doAnswer(invocation -> customAccountUID.get()).when(orderCancelRequestMock).getCustomAccountID();
        doAnswer(invocation -> origClOrdId.get()).when(orderCancelRequestMock).getOrigClOrdId();
        doAnswer(invocation -> msgSeqNumber.get()).when(orderCancelRequestMock).getMsgSeqNum();
        doAnswer(invocation -> IMsgType.OrderCancelRequest).when(orderCancelRequestMock).getMsgType();
        doAnswer(invocation -> DefinitionMocks.MSG_TYPE_OrderCancelRequest).when(orderCancelRequestMock).getMsgTypeEnum();
        doAnswer(invocation -> DefinitionMocks.MSG_TYPE_OrderCancelRequest.getId()).when(orderCancelRequestMock).getMsgTypeId();
        doAnswer(invocation -> securityIdentifier.get().getId()).when(orderCancelRequestMock).getSecurityId();
        doAnswer(invocation -> sendingTime.get()).when(orderCancelRequestMock).getSendingTime();
        doAnswer(invocation -> pSessionUID).when(orderCancelRequestMock).getSessionId();
        doAnswer(invocation -> side.get()).when(orderCancelRequestMock).getSideEnum();
        doAnswer(invocation -> transactTime.get()).when(orderCancelRequestMock).getTransactTime();
        doAnswer(invocation -> timestamp.get()).when(orderCancelRequestMock).getTimestamp();
        doAnswer(invocation -> {
            if (!buildCalled.compareAndSet(false, true)) {
                throw new IllegalStateException("buildOrderCancelRequest() on OrderBuilder can be called only once. Create new builder for every new created order.");
            }
            return orderCancelRequestMock;
        }).when(orderBuilderMock).buildOrderCancelRequest();

        IOrderStatusRequest orderStatusRequestMock = mock(IOrderStatusRequest.class);
        doAnswer(invocation -> clrOrId.get()).when(orderStatusRequestMock).getClOrdId();
        doAnswer(invocation -> customAccountUID.get()).when(orderStatusRequestMock).getCustomAccountID();
        doAnswer(invocation -> msgSeqNumber.get()).when(orderStatusRequestMock).getMsgSeqNum();
        doAnswer(invocation -> IMsgType.OrderStatusRequest).when(orderStatusRequestMock).getMsgType();
        doAnswer(invocation -> DefinitionMocks.MSG_TYPE_OrderStatusRequest).when(orderStatusRequestMock).getMsgTypeEnum();
        doAnswer(invocation -> DefinitionMocks.MSG_TYPE_OrderStatusRequest.getId()).when(orderStatusRequestMock).getMsgTypeId();
        doAnswer(invocation -> securityIdentifier.get().getId()).when(orderStatusRequestMock).getSecurityId();
        doAnswer(invocation -> sendingTime.get()).when(orderStatusRequestMock).getSendingTime();
        doAnswer(invocation -> pSessionUID).when(orderStatusRequestMock).getSessionId();
        doAnswer(invocation -> side.get()).when(orderStatusRequestMock).getSideEnum();
        doAnswer(invocation -> timestamp.get()).when(orderStatusRequestMock).getTimestamp();
        doAnswer(invocation -> {
            if (!buildCalled.compareAndSet(false, true)) {
                throw new IllegalStateException("buildOrderStatusRequest() on OrderBuilder can be called only once. Create new builder for every new created order.");
            }
            return orderStatusRequestMock;
        }).when(orderBuilderMock).buildOrderStatusRequest();

        doAnswer(pInvocation1 -> {
            if (!buildCalled.compareAndSet(false, true)) {
                throw new IllegalStateException("buildNewOrderSingle() on OrderBuilder can be called only once. Create new builder for every new created order.");
            }
            INewOrderSingle order = pInvocation1.getArgument(0, INewOrderSingle.class);
            customAccountUID.set(order.getCustomAccountID());
            expire.set(order.getExpire());
            orderType.set(order.getOrderTypeEnum());
            origClOrdId.set(order.getClOrdId());
            price.set(order.getPrice());
            quantity.set(order.getQuantity());
            side.set(order.getSideEnum());
            timeInForce.set(order.getTimeInForceEnum());
            INewOrderSingle orderToCreateCancelFrom = pInvocation1.getArgument(0, INewOrderSingle.class);
            buildCalled.set(false); // as we are calling another method from same builder
            return orderBuilderMock.buildOrderCancelRequest();
        }).when(orderBuilderMock).buildOrderCancelRequest(any());
        return orderBuilderMock;
    }
}
