package com.librade.spi.open;

import org.mockito.Mockito;

/**
 * @author Tomas Fecko
 */
public class SessionMockProvider extends AMockProviderParent {

    /**
     *
     * @return
     */
    public ISession createDefaultMock() {
        return Mockito.mock(ISession.class);
    }
}






