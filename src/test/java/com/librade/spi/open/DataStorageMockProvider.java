package com.librade.spi.open;

import org.mockito.Mockito;

/**
 * @author Tomas Fecko
 */
public class DataStorageMockProvider extends AMockProviderParent {

    /**
     *
     * @return
     */
    public IDataStorage createDefaultMock() {
        return Mockito.mock(IDataStorage.class);
    }
}






