package com.librade.spi.open;

import org.mockito.Mockito;

/**
 * @author Tomas Fecko
 */
public class ComputerHumanInteractionMockProvider extends AMockProviderParent {


    /**
     *
     * @return
     */
    public IComputerHumanInteraction createDefaultMock() {
        return Mockito.mock(IComputerHumanInteraction.class);
    }
}





