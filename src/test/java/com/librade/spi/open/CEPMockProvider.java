package com.librade.spi.open;

import com.librade.spi.open.exceptions.APIException;
import java.util.LinkedHashMap;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doAnswer;

/**
 *
 * @author Tomas Fecko
 */
public class CEPMockProvider extends AMockProviderParent {

    /**
     *
     * @return
     */
    public ICEP createDefaultMock() throws APIException {
        ICEP mock = Mockito.mock(ICEP.class);

        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQueriesOrPatterns", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQueriesOrPatterns(anyString(), isA(boolean[].class), any());
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQueriesOrPatterns", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQueriesOrPatterns(anyString(), anyString(), anyString(), anyString(), isA(int[].class), isA(boolean[].class), any());
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQueriesOrPatterns", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQueriesOrPatterns(anyString(), anyLong(), anyLong(), isA(int[].class), isA(boolean[].class), any());
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQueriesOrPatterns", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQueriesOrPatterns(anyString(), anyLong(), isA(int[].class), isA(boolean[].class), any());
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQueriesOrPatterns", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQueriesOrPatterns(anyString(), anyString(), anyString(), isA(int[].class), isA(boolean[].class), any());

        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQuery", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQuery(anyString(), anyString());
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQuery", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQuery(anyString(), anyString(), anyString(), anyString(), anyString(), isA(int[].class));
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQuery", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQuery(anyString(), anyString(), anyLong(), anyLong(), isA(int[].class));
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQuery", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQuery(anyString(), anyString(), anyLong(), isA(int[].class));
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQuery", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQuery(anyString(), anyString(), anyString(), anyString(), anyLong(), isA(int[].class));
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQuery", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQuery(isA(LinkedHashMap.class), isA(LinkedHashMap.class), anyLong(), isA(int[].class));
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartEPLQuery", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartEPLQuery(anyString(), anyString(), anyString(), anyString(), isA(int[].class));

        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("createAndStartPattern", invocation.getArguments());
            }
            return null;
        }).when(mock).createAndStartPattern(anyString(), anyString());
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("destroyStatement", invocation.getArguments());
            }
            return null;
        }).when(mock).destroyStatement(anyString());
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("startStatement", invocation.getArguments());
            }
            return null;
        }).when(mock).startStatement(anyString());
        doAnswer(invocation -> {
            if (mGenericListener != null) {
                mGenericListener.onMethodCall("stopStatement", invocation.getArguments());
            }
            return null;
        }).when(mock).stopStatement(anyString());
        return mock;
    }
}






