package com.librade.spi.open;

import com.librade.spi.open.listeners.IOrderExecutionListener;

import org.mockito.Mockito;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.mockito.Mockito.when;

/**
 * @author Tomas Fecko
 */
public class LibradeAPIMockProvider {

    private final ExecutorService                   mExecutorService = Executors.newCachedThreadPool();

    private TimeMockProvider                        mTimeMockProvider = new TimeMockProvider();
    private AsyncTaskMockProvider                   mAsyncTaskMockProvider = new AsyncTaskMockProvider();
    private UtilsMockProvider                       mUtilsMockProvider = new UtilsMockProvider();
    private CEPMockProvider                         mCEPMockProvider = new CEPMockProvider();
    private EventMockProvider                       mEventMockProvider = new EventMockProvider();
    private LogMockProvider                         mLogMockProvider = new LogMockProvider();
    private SessionMockProvider                     mSessionMockProvider = new SessionMockProvider();
    private DataStorageMockProvider                 mDataStorageMockProvider = new DataStorageMockProvider();
    private OrderExecutionMockProvider              mOrderExecutionMockProvider = new OrderExecutionMockProvider(mExecutorService);
    private HistoricalDataMockProvider              mHistoricalDataMockProvider = new HistoricalDataMockProvider();
    private ComputerHumanInteractionMockProvider    mComputerHumanInteractionMockProvider = new ComputerHumanInteractionMockProvider();
    private AccountMockProvider                     mAccountMockProvider = new AccountMockProvider();
    private DataTransformMockProvider               mDataTransformMockProvider = new DataTransformMockProvider();

    /**
     *
     * @param pSessionUID session UID
     * @return librade api mock
     * @throws Exception
     */
    public final ILibradeAPI createMock(String pSessionUID) throws Exception {
        ILibradeAPI mock = Mockito.mock(ILibradeAPI.class);
        when(mock.async()).thenAnswer(invocation -> mAsyncTaskMockProvider.createDefaultMock());
        when(mock.cep()).thenAnswer(invocation -> mCEPMockProvider.createDefaultMock());
        when(mock.computerHumanInteraction()).thenAnswer(invocation -> mComputerHumanInteractionMockProvider.createDefaultMock());
        when(mock.event()).thenAnswer(invocation -> mEventMockProvider.createDefaultMock());
        when(mock.historicalData()).thenAnswer(invocation -> mHistoricalDataMockProvider.createDefaultMock());
        when(mock.log()).thenAnswer(invocation -> mLogMockProvider.createDefaultMock());
        when(mock.orderExecution()).thenAnswer(invocation -> mOrderExecutionMockProvider.createDefaultMock(pSessionUID));
        when(mock.dataStorage()).thenAnswer(invocation -> mDataStorageMockProvider.createDefaultMock());
        when(mock.session()).thenAnswer(invocation -> mSessionMockProvider.createDefaultMock());
        when(mock.time()).thenAnswer(invocation -> mTimeMockProvider.createDefaultMock());
        when(mock.utils()).thenAnswer(invocation -> mUtilsMockProvider.createDefaultMock());
        when(mock.account()).thenAnswer(invocation -> mAccountMockProvider.createDefaultMock());
        when(mock.dataTransform()).thenAnswer(invocation -> mDataTransformMockProvider.createDefaultMock());

        mOrderExecutionMockProvider.setLibradeAPI(mock);
        return mock;
    }

    public TimeMockProvider getTimeMockProvider() {
        return mTimeMockProvider;
    }

    public AsyncTaskMockProvider getAsyncTaskMockProvider() {
        return mAsyncTaskMockProvider;
    }

    public UtilsMockProvider getUtilsMockProvider() {
        return mUtilsMockProvider;
    }

    public CEPMockProvider getCEPMockProvider() {
        return mCEPMockProvider;
    }

    public EventMockProvider getEventMockProvider() {
        return mEventMockProvider;
    }

    public LogMockProvider getLogMockProvider() {
        return mLogMockProvider;
    }

    public SessionMockProvider getSessionMockProvider() {
        return mSessionMockProvider;
    }

    public DataStorageMockProvider getDataStorageMockProvider() {
        return mDataStorageMockProvider;
    }

    public OrderExecutionMockProvider getOrderExecutionMockProvider() {
        return mOrderExecutionMockProvider;
    }

    public HistoricalDataMockProvider getHistoricalDataMockProvider() {
        return mHistoricalDataMockProvider;
    }

    public ComputerHumanInteractionMockProvider getComputerHumanInteractionMockProvider() {
        return mComputerHumanInteractionMockProvider;
    }

    public AccountMockProvider getAccountMockProvider() {
        return mAccountMockProvider;
    }

    public DataTransformMockProvider getDataTransformMockProvider() {
        return mDataTransformMockProvider;
    }

    /**
     *
     * @param pListener listener
     */
    public void setListener(IOrderExecutionListener pListener) {
        mOrderExecutionMockProvider.setListener(pListener);
    }
}





