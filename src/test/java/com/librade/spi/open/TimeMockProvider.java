package com.librade.spi.open;

import java.util.concurrent.atomic.AtomicLong;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * @author Tomas Fecko
 */
public class TimeMockProvider extends AMockProviderParent {

    private final AtomicLong mCurrentTime = new AtomicLong();

    /**
     *
     * @return mock of ITime
     */
    public ITime createDefaultMock() throws Exception {
        ITime mock = Mockito.mock(ITime.class);
        when(mock.currentTime()).thenReturn(0L);
        doNothing().when(mock).createCustomTimeEvent(anyString(), anyString(), anyString(), anyLong());
        when(mock.destroyCustomTimeEvent(anyString())).thenReturn(true);

        return mock;
    }
}
