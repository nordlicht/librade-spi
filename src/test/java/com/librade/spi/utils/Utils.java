package com.librade.spi.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

/**
 * Created by TomasLocal on 04.05.2016.
 */
public final class Utils {

    private static final Random RANDOM = new Random(System.nanoTime());

    /**
     *
     */
    public static final String                  DATE_TIME_FORMAT_PATTERN = "dd.MM.yyyy HH:mm:ss.SSS";

    /**
     *
     */
    private Utils() {
    }

    /**
     * @param pTimeZone time zone in string
     * @return timeZone object
     * @throws AException if wrong timezone is sent
     */
    public static TimeZone getTimeZone(String pTimeZone) {
        TimeZone tz = TimeZone.getTimeZone(pTimeZone);
        if (tz.getID().equals("GMT") && !tz.getID().equals(pTimeZone)) {
            throw new IllegalArgumentException("Unparseable TimeZone: " + pTimeZone);
        }
        return tz;
    }

    /**
     * @param pDateTime date time to parse
     * @param pTimeZone timezone
     * @return time in long
     * @throws AException if timezone is wrong, or dateTime didn't match the dateTime pattern
     */
    public static long parseDateTime(String pDateTime, String pTimeZone) {
        TimeZone timeZone = getTimeZone(pTimeZone);
        Calendar calendar = Calendar.getInstance(timeZone);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.setCalendar(calendar);
        simpleDateFormat.setTimeZone(timeZone);
        simpleDateFormat.setLenient(false);
        simpleDateFormat.applyPattern(DATE_TIME_FORMAT_PATTERN);
        try {
            Date dateTime = simpleDateFormat.parse(pDateTime);
            return dateTime.getTime();
        } catch (ParseException e) {
            throw new IllegalArgumentException("Unparseable dateTime!!! Pattern should match:" + DATE_TIME_FORMAT_PATTERN, e);
        }
    }

    /**
     *
     * @param pSize size of less than 16 is questionable unique (if the size is bigger than 1024, 1024 is used)
     * @return unique string based on currentTimeInMillis and nanoTime (resulting string is a positive number)
     */
    public static String getUniqueString(int pSize) {
        if (pSize < 0) {
            pSize = Math.abs(pSize);
        }
        if (pSize > 1024) {
            pSize = 1024;
        }
        String unique = String.valueOf(System.currentTimeMillis()) + new StringBuilder(String.valueOf(Math.abs(System.nanoTime()))).reverse().toString();
        while (unique.length() < pSize) {
            unique += Math.abs(RANDOM.nextLong());
        }
        return unique.substring(0, pSize);
    }
}






