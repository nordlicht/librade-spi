package com.librade;

import com.librade.spi.open.ILibradeAPI;
import com.librade.spi.open.LibradeAPIMockProvider;
import com.librade.spi.open.data.DefinitionMocks;
import com.librade.spi.open.data.INewOrderSingle;
import com.librade.spi.open.data.IOrderCancelRequest;
import com.librade.spi.open.data.IOrderStatusRequest;
import com.librade.spi.open.data.definitions.IMsgType;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.UUID;

import static com.librade.spi.open.data.DefinitionMocks.ORDER_TYPE_MARKET;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Tomas Fecko
 */
//@RunWith(PowerMockRunner.class)
public class SampleTest {

    @Test
    public void testOrderExecution() throws Exception {
        String sessionUID = UUID.randomUUID().toString();
        String customAccountId = "account1";
        long expire = System.currentTimeMillis();
        BigDecimal price = new BigDecimal("1.15456");
        BigDecimal quantity = new BigDecimal("100000");

        ILibradeAPI mock = new LibradeAPIMockProvider().createMock(sessionUID);
        INewOrderSingle orderSingle = mock.orderExecution().createOrderBuilder()
                .orderType(ORDER_TYPE_MARKET)
                .customAccountUID(customAccountId)
                .expire(expire)
                .price(price)
                .quantity(quantity)
                .side(DefinitionMocks.SIDE_BUY)
                .securityIdentifier(DefinitionMocks.SECURITY_IDENTIFIER_EUR_USD_4)
                .timeInForce(DefinitionMocks.TIME_IN_FORCE_IOC)
                .buildNewOrderSingle();

        assertNotNull(orderSingle.getClOrdId());
        assertEquals(customAccountId, orderSingle.getCustomAccountID());
        assertEquals(expire, orderSingle.getExpire());
        assertTrue(orderSingle.getMsgSeqNum() > 0);
        assertEquals(IMsgType.NewOrderSingle, orderSingle.getMsgType());
        assertEquals(DefinitionMocks.MSG_TYPE_NewOrderSingle, orderSingle.getMsgTypeEnum());
        assertEquals(DefinitionMocks.MSG_TYPE_NewOrderSingle.getId(), orderSingle.getMsgTypeId());
        assertEquals(ORDER_TYPE_MARKET, orderSingle.getOrderTypeEnum());
        assertEquals(price, orderSingle.getPrice());
        assertEquals(quantity, orderSingle.getQuantity());
        assertEquals(DefinitionMocks.SECURITY_IDENTIFIER_EUR_USD_4.getId(), orderSingle.getSecurityId());
        assertTrue(orderSingle.getSendingTime() > 0);
        assertEquals(sessionUID, orderSingle.getSessionId());
        assertEquals(DefinitionMocks.SIDE_BUY, orderSingle.getSideEnum());
        assertEquals(DefinitionMocks.TIME_IN_FORCE_IOC, orderSingle.getTimeInForceEnum());
        assertTrue(orderSingle.getTimestamp() > 0);

        IOrderCancelRequest cancelRequest = mock.orderExecution().createOrderBuilder()
                .origClOrdId(orderSingle.getClOrdId())
                .customAccountUID(customAccountId)
                .orderType(ORDER_TYPE_MARKET)
                .expire(expire)
                .price(price)
                .quantity(quantity)
                .side(DefinitionMocks.SIDE_BUY)
                .securityIdentifier(DefinitionMocks.SECURITY_IDENTIFIER_EUR_USD_4)
                .timeInForce(DefinitionMocks.TIME_IN_FORCE_IOC)
                .buildOrderCancelRequest();

        assertNotNull(cancelRequest.getClOrdId());
        assertEquals(customAccountId, cancelRequest.getCustomAccountID());
        assertEquals(orderSingle.getClOrdId(), cancelRequest.getOrigClOrdId());
        assertTrue(cancelRequest.getMsgSeqNum() > 0);
        assertEquals(IMsgType.OrderCancelRequest, cancelRequest.getMsgType());
        assertEquals(DefinitionMocks.MSG_TYPE_OrderCancelRequest, cancelRequest.getMsgTypeEnum());
        assertEquals(DefinitionMocks.MSG_TYPE_OrderCancelRequest.getId(), cancelRequest.getMsgTypeId());
        assertTrue(cancelRequest.getTransactTime() > 0);
        assertEquals(DefinitionMocks.SECURITY_IDENTIFIER_EUR_USD_4.getId(), cancelRequest.getSecurityId());
        assertTrue(cancelRequest.getSendingTime() > 0);
        assertEquals(sessionUID, cancelRequest.getSessionId());
        assertEquals(DefinitionMocks.SIDE_BUY, cancelRequest.getSideEnum());
        assertTrue(cancelRequest.getTimestamp() > 0);

        IOrderStatusRequest statusRequest = mock.orderExecution().createOrderBuilder()
                .origClOrdId(orderSingle.getClOrdId())
                .customAccountUID(customAccountId)
                .orderType(ORDER_TYPE_MARKET)
                .expire(expire)
                .price(price)
                .quantity(quantity)
                .side(DefinitionMocks.SIDE_BUY)
                .securityIdentifier(DefinitionMocks.SECURITY_IDENTIFIER_EUR_USD_4)
                .timeInForce(DefinitionMocks.TIME_IN_FORCE_IOC)
                .buildOrderStatusRequest();

        assertNotNull(statusRequest.getClOrdId());
        assertEquals(customAccountId, statusRequest.getCustomAccountID());
        assertTrue(statusRequest.getMsgSeqNum() > 0);
        assertEquals(IMsgType.OrderStatusRequest, statusRequest.getMsgType());
        assertEquals(DefinitionMocks.MSG_TYPE_OrderStatusRequest, statusRequest.getMsgTypeEnum());
        assertEquals(DefinitionMocks.MSG_TYPE_OrderStatusRequest.getId(), statusRequest.getMsgTypeId());
        assertEquals(DefinitionMocks.SECURITY_IDENTIFIER_EUR_USD_4.getId(), statusRequest.getSecurityId());
        assertTrue(statusRequest.getSendingTime() > 0);
        assertEquals(sessionUID, statusRequest.getSessionId());
        assertEquals(DefinitionMocks.SIDE_BUY, statusRequest.getSideEnum());
        assertTrue(statusRequest.getTimestamp() > 0);

    }
}










