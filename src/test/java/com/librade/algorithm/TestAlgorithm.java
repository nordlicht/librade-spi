package com.librade.algorithm;

import com.librade.spi.open.IExecutable;
import com.librade.spi.open.ILibradeAPI;
import com.librade.spi.open.data.EventWrapper;
import com.librade.spi.open.data.IControlMessage;
import com.librade.spi.open.exceptions.APIException;
import com.nordlicht.commons.base.cep.ICEPEvent;

import java.util.List;

/**
 * Test algorithm.
 */
public class TestAlgorithm implements IExecutable {

    @Override
    public void execute(ILibradeAPI api, String inputParameters) throws APIException {
        // base events
        api.cep().createAndStartEPLQuery("ALL_MonitoringEvents", "SELECT * FROM MonitoringEvent");
        api.cep().createAndStartEPLQuery("ALL_ControlMessage", "SELECT * FROM ControlMessage");

        // market data events
        api.cep().createAndStartEPLQuery("ALL_Patterns", "SELECT * FROM Pattern");
        api.cep().createAndStartEPLQuery("ALL_MarketData", "SELECT * FROM MarketData");
        api.cep().createAndStartEPLQuery("ALL_MarketDataAnalytics", "SELECT * FROM MarketDataAnalytics");

        // trading data events
        api.cep().createAndStartEPLQuery("ALL_ExecutionReports", "SELECT * FROM ExecutionReport");
        api.cep().createAndStartEPLQuery("ALL_AccountStates", "SELECT * FROM AccountState");


        EventWrapper event = new EventWrapper(null);
        while (!api.session().isStopped()) {
            ICEPEvent cepEvent = api.event().poll(9);
            if (cepEvent == null) {
                continue;
            }
            event.wrap(cepEvent);
            if (event.isNa()) {
                continue;
            }
            if (event.isStop()) {
                api.log().debug("TestAlgorithm.execute()->Stop event arrived, stopping algo...");
                break;
            } else if (api.session().isStopped()) {
                api.log().debug("TestAlgorithm.execute()->Session is stopped, stopping algo...");
                break;
            } else if (event.isFail()) {
                // if error arrived from platform
                List<IControlMessage> controlMessages = event.getControlMessages();
                String error = "TestAlgorithm.execute()->Failure detected, finishing. Error:" + controlMessages;
                api.log().warn(error);
                break;
            }
            event.stripTrailingZeros();

            if (event.containsMarketData()) {
                // TODO handle market data
            } else if (event.containsExecutionReports()) {
                // TODO handle execution reports
            }
        }
    }
}





