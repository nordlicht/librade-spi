#!/bin/bash

set -v
################################################################################
#                         Additional script file                               #
#                             for task file                                    #
#                        Created by Marek Urban                                #
#                                                                              #
# This is only partial file of pipeline, for full understanding please look    #
# into specified pipeline.yml file.                                            #
################################################################################
set +v
# set system enviroments
# -e = Exit immediately if a command exits with a non-zero exit status.
# -u = Treat unset variables as an error when substituting.
# -v = Print shell input lines as they are read.
set -e -u

# Uncomment for debug
# set -v -x

# Perform mvn clean install for every individual project and copy dependencies
# -B = batch mode
# -q = quiet, print out only when error occurs

echo "Copying nordlicht-public/ to sonarqube-analysis-input/nordlicht-public"
cp -rf ./nordlicht-public/ ./sonarqube-analysis-input/
echo "Done."

echo "Copying librade-spi/ to sonarqube-analysis-input/librade-spi"
cp -rf ./librade-spi/ ./sonarqube-analysis-input/
echo "Done."

cd sonarqube-analysis-input/nordlicht-public/
echo "[NORDLICHT-PUBLIC] executing command: 'mvn clean install -B -q'"
mvn clean install -B -q
echo "[NORDLICHT-PUBLIC] executing command: 'mvn dependency:copy-dependencies -B -q'"
mvn dependency:copy-dependencies -B -q
# Wait 3 second to calm down everything...
echo "Waiting to make the job done..."
echo "3 sec"
sleep 1
echo "2 sec"
sleep 1
echo "1 sec"
sleep 1
echo "Done."

echo "[NORDLICHT-PUBLIC] Generating jacoco and surefire test reports..."
# Generate jacoco and surefire reports
mvn -DXmx1024m org.jacoco:jacoco-maven-plugin:prepare-agent install -B -q
mvn -DXmx1024m org.jacoco:jacoco-maven-plugin:prepare-agent-integration install -B -q
mvn -DXmx1024m surefire-report:report -B -q
echo "[NORDLICHT-PUBLIC] done."

# Wait 3 second to calm down everything...
echo "Waiting to make the job done..."
echo "3 sec"
sleep 1
echo "2 sec"
sleep 1
echo "1 sec"
sleep 1
echo "Done."

cd ../librade-spi
echo "[LIBRADE-SPI] executing command: 'mvn clean install -B -q'"
mvn clean install -B -q
echo "[LIBRADE-SPI] executing command: 'mvn dependency:copy-dependencies -B -q'"
mvn dependency:copy-dependencies -B -q
# Wait 3 second to calm down everything...
echo "Waiting to make the job done..."
echo "3 sec"
sleep 1
echo "2 sec"
sleep 1
echo "1 sec"
sleep 1
echo "Done."

echo "[LIBRADE-SPI] Generating jacoco and surefire test reports..."
# Generate jacoco and surefire reports
mvn -DXmx1024m org.jacoco:jacoco-maven-plugin:prepare-agent install -B -q
mvn -DXmx1024m org.jacoco:jacoco-maven-plugin:prepare-agent-integration install -B -q
mvn -DXmx1024m surefire-report:report -B -q
echo "[LIBRADE-SPI] done."

# Wait 3 second to calm down everything...
echo "Waiting to make the job done..."
echo "3 sec"
sleep 1
echo "2 sec"
sleep 1
echo "1 sec"
sleep 1
echo "Done."

echo "Copying .m2 to sonarqube-analysis-input/"
cp -rf /root/.m2/ ../../sonarqube-analysis-input/
echo "Waiting to make the job done..."
echo "3 sec"
sleep 1
echo "2 sec"
sleep 1
echo "1 sec"
sleep 1
echo "Copy done."
