#!/bin/bash

set -v
################################################################################
#                         Additional script file                               #
#                             for task file                                    #
#                        Created by Marek Urban                                #
#                                                                              #
# This is only partial file of pipeline, for full understanding please look    #
# into specified pipeline.yml file.                                            #
################################################################################
set +v
# set system enviroments
# -e = Exit immediately if a command exits with a non-zero exit status.
# -u = Treat unset variables as an error when substituting.
# -v = Print shell input lines as they are read.
set -e -u

# Uncomment for debug
# set -v -x

# print out Maven version
mvn --version

# Perform mvn clean install for every individual project
# -B = batch mode
# -q = quiet, print out only when error occurs

cd organized-resources/nordlicht-public/
echo "[NORDLICHT-PUBLIC] executing command: 'mvn clean install -B -q'"
mvn clean install -B -q
echo "[NORDLICHT-PUBLIC] done."

cd ../librade-spi
echo "[LIBRADE-SPI] executing command: 'mvn clean install -B -q'"
mvn clean install -B -q
echo "[LIBRADE-SPI] done."

cd ../../
echo "Copying organized-resources/ to prepared-resources/"
cp -rf ./organized-resources/* ./prepared-resources/
echo "Done."

# Wait 3 second to calm down everything...
echo "Waiting to make the job done..."
echo "3 sec"
sleep 1
echo "2 sec"
sleep 1
echo "1 sec"
sleep 1
echo "Done."
